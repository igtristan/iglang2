/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass2;

import pass1.*;

import java.util.Vector;

public class Processor extends IGConsts
{
	private  Token []   s_token_list = null;
	private   int             s_token_index = 0;
	private   int             s_token_end = 0;
	private   Token           m_eof;
	
	
	
	public  void error(Token stok, String text)
	{

		stok.printError(text);
		System.exit(1);

	}
	
	public Token expect(int token_type) {
		return expect(token_type, "no message defined");
	}
	
	public  Token expect(int token_type, String text)
	{
		Token stok = read();
		if (stok.m_type != token_type)
		{
			stok.printError(text);
			System.exit(1);
		}
		return stok;
	}
	
	protected int getTokenIndex() {
		return s_token_index;
	}
	
	protected void setTokenIndex(int ti) {
		s_token_index = ti;
	}
	
	
	protected  void setSource(Token [] list, int start_index, int end_index)
	{
		s_token_list = list;
		s_token_index = start_index;
		s_token_end = end_index;
		m_eof = list[list.length - 1];
	}
	
	protected  Token read()
	{
		if (s_token_index >= s_token_end) return m_eof;
		
		Token t = s_token_list[s_token_index];
		s_token_index ++;
		
		if (t.m_discarded) {
			t = read();
		}
		
		return t;
	}
	
	protected  int peekTokenType()
	{
		return peek().m_type;
	}
	
	protected  Token prev()
	{
		s_token_index --;
		Token t = s_token_list[s_token_index];
		return t;
	}
	
	protected Token peekAt(int absolute_position)
	{
		if (absolute_position >= s_token_end) return m_eof;
		
		Token t = s_token_list[absolute_position];
		return t;
	}
	
	protected Token findPair(int absolute_position)
	{
		Token tok = peekAt(absolute_position);
		if (tok.m_type != TOK_LBRACE) {
			error(tok, "findPair only works on { at the moment");
		}
		
		int height = 1;
		while (height > 0 && tok.m_type != TOK_EOF) 
		{
			if (tok.m_type == TOK_LBRACE) {
				height ++;
			}
			if (tok.m_type == TOK_RBRACE) {
				height --;
				
				if (height == 0) {
					break;
				}
			}
			
			absolute_position++;
			tok = peekAt(absolute_position);
		}
		
		return tok;
	}

	protected  Token peek()
	{
		if (s_token_index >= s_token_end) return m_eof;
		
		Token t = s_token_list[s_token_index];
		
		if (t.m_discarded) {
			read();
			return peek();
		}
		return t;
	}

	public static boolean hasNode(Token t, int node_type)
	{
		return t.m_node != null && t.m_node.m_node_type == node_type;
	}
}