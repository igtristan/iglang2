/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass2;

import pass1.Token;
import pass1.IGNode;

import java.io.*;
import java.util.*;


public class TokenWriter extends PrintWriter
{
	private Hashtable<Integer, String> m_to_append = null;
	private Hashtable<Integer, String> m_to_prepend = null;
	private Hashtable<Integer, String>  m_disabled   = null;
	
	public TokenWriter(File file) throws FileNotFoundException, IOException
	{
		super(new BufferedWriter(new FileWriter(file)));
		
		clearAppendBuffer();
	}
	
	public void clearAppendBuffer() {
		m_to_append = new Hashtable<Integer, String>();
		m_to_prepend = new Hashtable<Integer, String>();
		m_disabled = new Hashtable<Integer, String>();
	}
	
	@Override public void print(String s) {
		super.print(s);
	}
	
	@Override public void println(String s) {
		super.println(s);
	}
	
	public void disableOutput(IGNode node)
	{

			//System.err.println("disableOutput " + node.m_start_index + " to " + node.m_end_index);
		for (int i = node.m_start_index; i < node.m_end_index; i++) {

			m_disabled.put(i, "");
		}
	}
	
	
	public void replace(IGNode node, String text)
	{
		for (int i = node.m_start_index; i < node.m_end_index; i++) {

			m_disabled.put(i, (i == node.m_start_index) ? text : "");
		}
	}
	
	
	public void disableOutputTokens(int token_index, int count) {
		for (int i = 0; i < count; i++) {
			m_disabled.put(token_index + i, "");
		}
	}	
	
	public void disableOutput(int token_index)
	{
		m_disabled.put(token_index, "");
	}
	
	public void disableOutput(Token token)
	{
		m_disabled.put(token.m_token_index, "");
	}
	
	
	public void replace(Token token, String text)
	{
		m_disabled.put(token.m_token_index, text);	
		//prepend(token.m_token_index, text);
	}
	
	
	
	public void replaceFirstToken(IGNode node, String text) {
		m_disabled.put(node.m_start_index, text);
	}
	
	public void replaceLastToken(IGNode node, String text) {
		m_disabled.put(node.m_end_index - 1, text);
	}
	
	public void replaceAtOffset(Token token, int off, String text)
	{
		m_disabled.put(token.m_token_index + off, text);	
	}
	
	public void append(Token index, String text) {
		append(index.m_token_index, text);
	}
	
	public void prepend(Token index, String text) {
		prepend(index.m_token_index, text);		
	}
	
	public void prepend(IGNode node , String text) {
		prepend(node.m_start_index, text);
	}

	public void append(IGNode node , String text) {
		append(node.m_end_index - 1, text);
	}
	
	public void append(int index, String text) {
		String existing_text = "";
		if (m_to_append.containsKey(index)) {
			existing_text = m_to_append.get(index);
		}
		m_to_append.put(index, existing_text + text);
	}
	
	public void prepend(int index, String text) {
		String existing_text = "";
		if (m_to_prepend.containsKey(index)) {
			existing_text = m_to_prepend.get(index);
		}
		m_to_prepend.put(index, text + existing_text);
		
	}

	public void print(Token t) throws IOException {
		print(t.m_whitespace);
		print(t.m_value);
	}
	
	public void print(Token t, StringBuilder sb) {
		sb.append(t.m_whitespace);
		sb.append(t.m_value);
	}
	
	public void print(String t, StringBuilder sb) {
		sb.append(t);
	}
	
	
	public void printWhitespace(Token t) throws IOException{
		print(t.m_whitespace);
	}
	
	public void printWhitespace(Token t, StringBuilder sb) {
		sb.append(t.m_whitespace);
	}
	
	public void printValue(Token t) throws IOException {
		print(t.m_value);
	}
	
	
	// WHITESPACE PREPEND-n ... PREPEND-0 TOKEN-VALUE APPEND-0 ... APPEND-n
	
	public void printRange(Token [] tokens, int start_index, int end_index) throws IOException
	{
		if (end_index > tokens.length) {
			throw new RuntimeException("TokenWriter.printRange exceeded token list");
		}
		
		for (int i = start_index; i < end_index; i++) {
			Token t = tokens[i];
			
			if (t.m_discarded) {
				printWhitespace(t);		
				continue;
			}
			
			printWhitespace(t);			
			
			if (m_to_prepend.containsKey(i)) {
				print(m_to_prepend.get(i));
			}
			
			if (t.m_output && !m_disabled.containsKey(i)) {
				if (t.m_value != null) {
					print(t.m_value);
				}
			}
			
			if (m_disabled.containsKey(i)) {
				print(m_disabled.get(i));
			}
			
			if (m_to_append.containsKey(i)) {
				print(m_to_append.get(i));
			}
		}	
	}
	
	public void printRangeNoWhitespace(Token [] tokens, int start_index, int end_index, StringBuilder sb)
	{
		if (end_index > tokens.length) {
			throw new RuntimeException("TokenWriter.printRange exceeded token list");
		}
		
		for (int i = start_index; i < end_index; i++) {
			Token t = tokens[i];
			
			if (t.m_discarded) {
				//printWhitespace(t,sb);		
				continue;
			}
			
			//printWhitespace(t,sb);			
			
			if (m_to_prepend.containsKey(i)) {
				print(m_to_prepend.get(i),sb);
			}
			
			if (t.m_output && !m_disabled.containsKey(i)) {
				if (t.m_value != null) {
					print(t.m_value,sb);
				}
			}
			
			if (m_disabled.containsKey(i)) {
				print(m_disabled.get(i),sb);
			}
			
			if (m_to_append.containsKey(i)) {
				print(m_to_append.get(i),sb);
			}
		}	
	}
	
	public void printIndex(Vector<Token> tokens,int i) throws IOException
	{
		Token t = tokens.get(i);
		
		if (t.m_discarded) {
			printWhitespace(t);		
			return;
		}
		
		printWhitespace(t);			
		
		if (m_to_prepend.containsKey(i)) {
			print(m_to_prepend.get(i));
		}
		
		if (t.m_output && !m_disabled.containsKey(i)) {
			if (t.m_value != null) {
			print(t.m_value);
			}
		}
		
		if (m_disabled.containsKey(i)) {
			print(m_disabled.get(i));
		}
		
		if (m_to_append.containsKey(i)) {
			print(m_to_append.get(i));
		}	
	}
}