/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass2.asm;


/*

	array[index] += 5

	push_l	 array
	push_i32 index
	dup2x
	array_get
	push_i32   5
	add
	array_set



	WHY THE HELL DOESN'T THIS COMPLAIN ABOUT DUPLICATE LABELS?
*/

import java.io.*;
import java.util.*;
import util.ByteArray;
import util.*;
import javax.xml.bind.DatatypeConverter;
import java.util.regex.Pattern;


/*
INPUT FORMAT:

	class 		Object 
	extends 	OtherObject 
	implements 	SomeInterface

	var 	   x : int
	static-var j
	
	#function that takes 3..4 arguments, and uses a grand total of 8 local variables (includes parameters)
	static-function simpleAdd 1..2 3
		def 2   33		# this has a value of 33 for the default parameter 2
		push_local 0
		push_local 1
		add_i
		ret1
	END
		 
 

OUTPUT FORMAT:
struct ModuleHeader
{
	ig_i32 m_header;
	ig_i32 m_version;
	
	ig_i32 m_full_path
	ig_i32 m_extends;
	ig_i32 m_implements_count;
	ig_i32 m_implements[16]
		
	ig_i32 m_static_variable_count;
	ig_i32 m_member_variable_count;
	ig_i32 m_static_function_count;
	ig_i32 m_member_function_count;
	
	ig_i32 m_string_pool_size;
	ig_i32 m_string_entries;			// # of entries
	ig_i32 m_pair_pool_entries;			// # of entries
}

struct Variable
{
	ig_i32  m_name;
	ig_i32  m_type;
	ig_i32  m_flags;
}	
	
struct Method
{
	ig_i32  m_min_args;
	ig_i32  m_max_args;
	ig_i32  m_local_count;
	
	// need to point to code
	// need to point to defaults
}	
	


	

VM
	The vm will be organized that the stack contains 64 bit registers

		struct IGVMFunctionPointer
		{
			ig_i32 object;
			ig_i32 function;	// also has a tag for if an objet is expected
		}
		union
		{
			ig_i32 i32;	  // for ints and handles
			double f64;	  // for doubles
			struct IGVMFunctionPointer fp;
		} IGVMStackEntry;

		each object contains a vtable with the following entries
		
		Object
		{
			vtable => VTable
		
		}
		
		VTable
		{
			length
			idx0
			idx1
			idx2
			key id0
			key id1
			key id2
		}	
*/



public  class IGAssembler
{
	/*
	public static void process(TextReader br, String f, File dst) throws IOException
	{
		IGAsmUnit unit = new IGAsmUnit();
		
		String [] toks = new String[16];

//		boolean has_sorted_member_vars = false;
		
		try
		{
			
			while (br.readASMLineToks(toks) > 0) 
			{
				String tok0 = toks[0];
			
				if (tok0.equals("class")) {
					unit.setClassName(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("enum")) {
					unit.setEnumName(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("interface")) {
					unit.setInterfaceName(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("extends")) {
					unit.setExtends(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("implements")) {
					unit.addImplements(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("full-extends"))
				{
				}
				else if (tok0.equals("full-implements"))
				{
				}
				else if (tok0.equals("var") || tok0.equals("static-var")) 
				{
				
					IGAsmUnitVariable var = new IGAsmUnitVariable(unit);
					var.setStatic(tok0.equals("static-var"));
					
					var.setProtection(toks[1]);
					var.setName   (unit._stringPool(toks[2]));
					var.setExactType(unit._stringPool(toks[3]));
					var.setStorageType(unit._stringPool(toks[4]));
				
					var.setStorageClass(Integer.parseInt(toks[5]));
				
					if (var.m_static) {	unit.m_static_variables.add(var); }
					else              {	unit.m_member_variables.add(var); }
				}
				else if (tok0.equals("function") || tok0.equals("static-function")) {
				
					//if (!has_sorted_member_vars) {
						//
					//	//
//
//						unit.sortMemberVariables();
//						has_sorted_member_vars = true;
//					}
				
					IGAsmUnitMethod method = new IGAsmUnitMethod(unit);
					method.setStatic(tok0.equals("static-function"));
					method.setProtection(toks[1]);
					method.setName(unit.stringPool(toks[2]));
					
				
					if (method.m_static) { unit.m_static_functions.add(method);   }
					else                 { unit.m_member_functions.add(method);   }
				
					String arg_count   = toks[3];
				
					int arg_min = -1;
					int arg_max = -1;
					int local_count = 0;
					
					int range_index = arg_count.indexOf("..");
					if (range_index == -1) {
						arg_min = arg_max = Integer.parseInt(arg_count);
					}
					else
					{
						//String [] arg_toks = IGAssembler.RANGE_SPLIT.split(arg_count);
						arg_min = Integer.parseInt(arg_count.substring(0, range_index));
						arg_max = Integer.parseInt(arg_count.substring(range_index + 2));
					}
					
					local_count = Integer.parseInt(toks[4]);
					method.setReturns(Integer.parseInt(toks[5]));
					
					method.setCounts(arg_min, arg_max, local_count + arg_max);
				
					//line = br.readASMLine();
					while (br.readASMLineToks(toks) > 0) 
					{
						// we're done for the time being
						if (toks[0].equals("END")) {
							break;
						}
					
						method.addCodeLine(toks);
						//line = br.readASMLine();
					}
					method.resolveLabels();
				}
				
				else
				{
					throw new RuntimeException("Invalid ig assembly code");
				}
			
		
				//line = br.readASMLine();
			}	
			
			System.out.println("You should be sorting!!!");
			unit.sortMemberVariables();
		
		}
		catch (Exception ex) {
			System.out.println("" + dst);
			util.Log.logErrorEcho(f, "Assembler failure. " + br.getLineNumber() + "> " + br.getCurrentLine());
			ex.printStackTrace();
			System.exit(1);
		}	
	
		try
		{
			LEDataOutputStream dos = new LEDataOutputStream(new BufferedOutputStream(new FileOutputStream(dst)));
			unit.output(dos);
			dos.close();
		
		}
		catch (IOException ex) 
		{
			util.Log.logErrorEcho("" + dst, "writing failed");
			ex.printStackTrace();
			System.exit(1);
		}
	
		unit.clear();
	}
	
	*/
	
	public static void process(IGAsmDocument br, String f, File dst) throws IOException
	{
		IGAsmUnit unit = new IGAsmUnit();
		

		try
		{
			for (String [] s : br.m_meta)
			{
				String [] toks = s;
				String tok0 = toks[0];
			
				if (tok0.equals("class")) {
					unit.setClassName(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("enum")) {
					unit.setEnumName(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("interface")) {
					unit.setInterfaceName(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("extends")) {
					unit.setExtends(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("implements")) {
					unit.addImplements(unit.stringPool(toks[1]));
				}
				else if (tok0.equals("full-extends"))
				{
				}
				else if (tok0.equals("full-implements"))
				{
				}
				else
				{
					throw new RuntimeException("Invalid ig assembly code");
				}
		
			}

			for (IGAsmHeader header : br. m_headers)
			{
				String [] toks = header.m_header;
				String tok0 = toks[0];
		
				if (tok0.equals("function") || tok0.equals("static-function")) 
				{
					IGAsmUnitMethod method = new IGAsmUnitMethod(unit);
					method.setStatic(tok0.equals("static-function"));
					method.setProtection(toks[1]);
					method.setName(unit.stringPool(toks[2]));
				
			
					if (method.m_static) { unit.m_static_functions.add(method);   }
					else                 { unit.m_member_functions.add(method);   }
			
					String arg_count   = toks[3];
			
					int arg_min = -1;
					int arg_max = -1;
					int local_count = 0;
				
					int range_index = arg_count.indexOf("..");
					if (range_index == -1) {
						arg_min = arg_max = Integer.parseInt(arg_count);
					}
					else
					{
						//String [] arg_toks = IGAssembler.RANGE_SPLIT.split(arg_count);
						arg_min = Integer.parseInt(arg_count.substring(0, range_index));
						arg_max = Integer.parseInt(arg_count.substring(range_index + 2));
					}
				
					local_count = Integer.parseInt(toks[4]);
					method.setReturns(Integer.parseInt(toks[5]));
					method.setCounts(arg_min, arg_max, local_count + arg_max);
			
					for (IGAsmOp op : header.m_ops)
					{
						if (op.m_op != -1) {
							if (op.m_op == IGAsm.END) {
								break;
							}	
							method.addOp(op);	
						}			
					}
				
					method.resolveLabels();
				}
				else if (tok0.equals("var") || tok0.equals("static-var")) 
				{
			
					IGAsmUnitVariable var = new IGAsmUnitVariable(unit);
					var.setStatic(tok0.equals("static-var"));
				
					var.setProtection(toks[1]);
					var.setName   (unit._stringPool(toks[2]));
					var.setExactType(unit._stringPool(toks[3]));
					var.setStorageType(unit._stringPool(toks[4]));
			
					var.setStorageClass(Integer.parseInt(toks[5]));
			
					if (var.m_static) {	unit.m_static_variables.add(var); }
					else              {	unit.m_member_variables.add(var); }
				}
				else
				{
					throw new RuntimeException("Invalid ig assembly code");
				}
			}
			
			
			unit.sortMemberVariables();
		
		}
		catch (Exception ex) {
		
		
			System.out.println("" + dst);
			util.Log.logErrorEcho(f, "Assembler failure. ");
			ex.printStackTrace();
			System.exit(1);
		}	
	
		try
		{
			LEDataOutputStream dos = new LEDataOutputStream(new BufferedOutputStream(new FileOutputStream(dst)));
			unit.output(dos);
			dos.close();
		
		}
		catch (IOException ex) 
		{
			util.Log.logErrorEcho("" + dst, "writing failed");
			ex.printStackTrace();
			System.exit(1);
		}
	
		unit.clear();
	}
}
