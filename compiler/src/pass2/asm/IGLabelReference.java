package pass2.asm;



import java.io.*;
import java.util.*;
import util.ByteArray;
import util.*;

public final class IGLabelReference
{
	// where the reference should be written
	public ByteArray m_dst;
	public int m_offset;
	
	public String m_label;
	
	public IGLabelReference(int o, String l, ByteArray dst) {
		m_offset = o;
		m_label = l;
		m_dst = dst;
	}
}	
