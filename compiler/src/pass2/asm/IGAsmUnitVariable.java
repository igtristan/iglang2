package pass2.asm;

import java.io.*;
import java.util.*;
import util.ByteArray;
import util.*;
import javax.xml.bind.DatatypeConverter;
import java.util.regex.Pattern;


public final class IGAsmUnitVariable implements Comparable
{
	public IGAsmUnit m_unit;
	public boolean 	m_static = true;

	public int  	m_name;
	public int      m_type          = 0;
	public int      m_exact_type    = 0;
	public int      m_protection    = 0;	
	
	public int      m_storage_class = 8;	// highest possible storage class
	// 16 = object, 17 = function pointer, 18 = ref count
	
	
	@Override
    public int compareTo(Object o) {  
    	IGAsmUnitVariable other = (IGAsmUnitVariable)o;
  
    	int this_class  = (m_storage_class < 16) ? 0 : 1;
    	int other_class = (other.m_storage_class < 16) ? 0 : 1;
    	
    	return  other_class - this_class;
    }
	
	
	public IGAsmUnitVariable(IGAsmUnit u) {
		m_unit = u;
	}
	
	public void setName(int n) {
		m_name = n;
	}
	
	public void setStatic(boolean s) {
		m_static = s;
	}	
	
	public void setProtection(String p) {
		p = p.toLowerCase();
		if (p.equals("public")) {
			m_protection = 0;
		}
		else
		{
			m_protection = 1;
		}
	}
	public void output(LEDataOutputStream dos) throws IOException
	{
		dos.writeInt(m_name);			// string key for the name
		dos.writeInt(m_type);			// string key for the contents type.        iglang.Vector<iglang.Object>
		dos.writeInt(m_exact_type);		// string key for the exact contents type.  iglang.Vector<iglang.String>
		dos.writeShort(m_protection);	// int for the protection, and other such flags
		dos.writeShort(m_storage_class);
	}
	
	// ie iglang.ArrayList<iglang.Object>
	public void setStorageType(int type_name) {
		m_type = type_name;
	}

	// ie iglang.ArrayList<iglang.ArrayList<iglang.String>>>	
	public void setExactType(int type_name) {
		m_exact_type = type_name;
	}
	
	public void setStorageClass(int sc) {
		m_storage_class = sc;
	}
}
