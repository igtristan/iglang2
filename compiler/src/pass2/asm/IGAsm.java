/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass2.asm;

// table a direct encoding of the s_simple map
// these are all the meta ops.  Ie used to communicate to the assembler, but
// not as output to the vm
public class IGAsm
{
		public static final int nop = IGAsmUnitMethod. NOP;
		public static final int nop_term = IGAsmUnitMethod. NOP_TERM;
		public static final int pop = IGAsmUnitMethod. POP;
		//public static final int ll0 = IGAsmUnitMethod. LL0;
		//public static final int ll1 = IGAsmUnitMethod. LL1;
		//public static final int ll2 = IGAsmUnitMethod. LL2;
		//public static final int ll3 = IGAsmUnitMethod. LL3;	
		//public static final int ll4 = IGAsmUnitMethod. LL4;	
				
		public static final int dup = IGAsmUnitMethod. DUP;
		public static final int dup2 = IGAsmUnitMethod. DUP2;
		public static final int dup1x = IGAsmUnitMethod. DUP1X;
		public static final int dup2x = IGAsmUnitMethod. DUP2X;
		public static final int ret0 = IGAsmUnitMethod.  RET0;
		public static final int ret1 = IGAsmUnitMethod.  RET1;
		public static final int ret_obj = IGAsmUnitMethod. RET_OBJ;
		public static final int ret_funcptr = IGAsmUnitMethod. RET_FUNCPTR;
		public static final int add_i32 = IGAsmUnitMethod. ADD_I32;
		public static final int sub_i32 = IGAsmUnitMethod. SUB_I32;
		public static final int mul_i32 = IGAsmUnitMethod. MUL_I32;
		public static final int div_i32 = IGAsmUnitMethod. DIV_I32;
		public static final int mod_i32 = IGAsmUnitMethod. MOD_I32;
		


		public static final int add_f64_vec = IGAsmUnitMethod. ADD_F64_VEC;
		public static final int sub_f64_vec = IGAsmUnitMethod. SUB_F64_VEC;
		public static final int mul_f64_vec = IGAsmUnitMethod. MUL_F64_VEC;
		public static final int mul_f64_vec_val = IGAsmUnitMethod. MUL_F64_VEC_VAL;

		public static final int add_f64 = IGAsmUnitMethod. ADD_F64;
		public static final int sub_f64 = IGAsmUnitMethod. SUB_F64;
		public static final int mul_f64 = IGAsmUnitMethod. MUL_F64;
		public static final int div_f64 = IGAsmUnitMethod. DIV_F64;
		public static final int mod_f64 = IGAsmUnitMethod. MOD_F64;
		
		public static final int bit_and_i32 = IGAsmUnitMethod. AND_I32;
		public static final int bit_or_i32 = IGAsmUnitMethod.  OR_I32;
		public static final int bit_xor_i32 = IGAsmUnitMethod. XOR_I32;
		public static final int bit_not_i32 = IGAsmUnitMethod. BIT_NOT_I32;
		
		public static final int not = IGAsmUnitMethod.         NOT;
		public static final int lsl_i32 = IGAsmUnitMethod. LSL_I32;
		public static final int lsr_i32 = IGAsmUnitMethod. LSR_I32;
		public static final int asr_i32 = IGAsmUnitMethod. ASR_I32;
		
		public static final int cmp_eq_i32 = IGAsmUnitMethod. CMP_EQ_I32;
		public static final int cmp_lt_i32 = IGAsmUnitMethod. CMP_LT_I32;
		public static final int cmp_le_i32 = IGAsmUnitMethod. CMP_LE_I32;
		
		public static final int cmp_eq_f64 = IGAsmUnitMethod. CMP_EQ_F64;
		public static final int cmp_lt_f64 = IGAsmUnitMethod. CMP_LT_F64;
		public static final int cmp_le_f64 = IGAsmUnitMethod. CMP_LE_F64;
	
		public static final int cmp_eq_obj = IGAsmUnitMethod. CMP_EQ_OBJ;	// NOTE THIS WEIRD MAPPING
		public static final int cmp_eq_fp = IGAsmUnitMethod. CMP_EQ_I64;	// ALSO NOTE THIS WEIRD MAPPING
		public static final int i32_to_f64 = IGAsmUnitMethod. I32_TO_F64;
		public static final int f64_to_i32 = IGAsmUnitMethod. F64_TO_I32;
			
		public static final int catch_entry = IGAsmUnitMethod. CATCH_ENTRY;	
		public static final int array_length = IGAsmUnitMethod. ARRAY_LENGTH;
		
		
		public static final int param_undef = IGAsmUnitMethod. META_param_undef;
		public static final int param_i32 = IGAsmUnitMethod. META_param_i32;
		public static final int param_i64 = IGAsmUnitMethod. META_param_i64;
		public static final int param_f64 = IGAsmUnitMethod. META_param_f64;
		public static final int param_object = IGAsmUnitMethod.META_param_object;
		public static final int param_func_ptr = IGAsmUnitMethod.META_param_func_ptr;
		
		public static final int push_f64 = IGAsmUnitMethod. META_push_f64;
		public static final int push_i64 = IGAsmUnitMethod. META_push_i64;
		public static final int push_i32 = IGAsmUnitMethod. META_push_i32;
		

		public static final int push_h32 = IGAsmUnitMethod. META_push_h32;
		public static final int push_string = IGAsmUnitMethod. META_push_string;
		public static final int local_store = IGAsmUnitMethod. META_local_store;
		public static final int local_load = IGAsmUnitMethod. META_local_load;
		public static final int local_store_keep = IGAsmUnitMethod. META_local_store_keep;

		//public static final int local_load_2 = IGAsmUnitMethod. META_local_load_2;
		public static final int local_clear = IGAsmUnitMethod. META_local_clear;
//		public static final int create_it = IGAsmUnitMethod. META_create_it;
		public static final int member_store = IGAsmUnitMethod. META_member_store;
		public static final int member_load = IGAsmUnitMethod. META_member_load;
		public static final int static_store = IGAsmUnitMethod. META_static_store;
		public static final int static_load = IGAsmUnitMethod. META_static_load;
		public static final int this_store = IGAsmUnitMethod. META_this_store;
		public static final int this_load = IGAsmUnitMethod. META_this_load;
		public static final int cast_to = IGAsmUnitMethod. META_cast_to;
		
		public static final int call_interface = IGAsmUnitMethod. META_call_interface;
		public static final int call_vtable = IGAsmUnitMethod. META_call_vtable;
		public static final int call_static = IGAsmUnitMethod. META_call_static;
		public static final int call_static_vtable = IGAsmUnitMethod. META_call_static_vtable;
		public static final int call_dynamic = IGAsmUnitMethod. META_call_dynamic;
		public static final int call_super = IGAsmUnitMethod. META_call_super;
		
		public static final int call_ret1_interface = IGAsmUnitMethod.		META_call_ret1_interface;
		public static final int call_ret1_vtable = IGAsmUnitMethod. 		META_call_ret1_vtable;
		public static final int call_ret1_static = IGAsmUnitMethod. 		META_call_ret1_static;
		public static final int call_ret1_static_vtable = IGAsmUnitMethod. META_call_ret1_static_vtable;
		public static final int call_ret1_dynamic = IGAsmUnitMethod. 		META_call_ret1_dynamic;
		public static final int call_ret1_super = IGAsmUnitMethod. 		META_call_ret1_super;	

		
		public static final int funcptr_member = IGAsmUnitMethod. META_funcptr_member;
		public static final int funcptr_static = IGAsmUnitMethod. META_funcptr_static;
		public static final int try_push = IGAsmUnitMethod. META_try_push;
		public static final int instance_of = IGAsmUnitMethod. META_instance_of;
		public static final int __new = IGAsmUnitMethod. META_new;
		public static final int __throw = IGAsmUnitMethod. META_throw;
		public static final int jmp = IGAsmUnitMethod. META_jmp;
		public static final int j0_i32 = IGAsmUnitMethod. META_j0_i32;
		public static final int jn0_i32 = IGAsmUnitMethod. META_jn0_i32;
		public static final int jeq_i32 = IGAsmUnitMethod. META_jeq_i32;
		public static final int and = IGAsmUnitMethod. META_and;
		public static final int or = IGAsmUnitMethod. META_or;
		public static final int next_it = IGAsmUnitMethod. META_next_it;
//		public static final int release = IGAsmUnitMethod. META_release;
		public static final int array_load = IGAsmUnitMethod. META_array_load;
		public static final int array_store = IGAsmUnitMethod. META_array_store;
		public static final int array_new = IGAsmUnitMethod. META_array_new;	
		public static final int array_resize = IGAsmUnitMethod. META_array_resize;	
		public static final int local_inc_i8 = IGAsmUnitMethod. META_local_inc_i8;
		public static final int stack_inc_i8 = IGAsmUnitMethod. META_stack_inc_i8;
		public static final int this_inc_i32 = IGAsmUnitMethod. META_this_inc_i32;
		
		public static final int array_fill = IGAsmUnitMethod.   META_array_fill;
		public static final int array_copy = IGAsmUnitMethod.   META_array_copy;
		
		public static final int asr_i32_const = IGAsmUnitMethod. META_ASR_I32_CONST;
		public static final int lsr_i32_const = IGAsmUnitMethod. META_LSR_I32_CONST;
		public static final int lsl_i32_const = IGAsmUnitMethod. META_LSL_I32_CONST;
		
		public static final int mla_i32       = IGAsmUnitMethod.MLA_I32;
		public static final int mla_f64       = IGAsmUnitMethod.MLA_F64;		
		public static final int lsl_asr_i32_const = IGAsmUnitMethod.META_lsl_asr_i32_const;
		public static final int switch_i32_jmp    = IGAsmUnitMethod.META_switch_i32_jmp;
		
		
		// these can only be created by the optimizer
		public static final int push_i32_0 = IGAsmUnitMethod.PUSH_I32_0;
		public static final int push_i32_1 = IGAsmUnitMethod.PUSH_I32_1;
		public static final int push_f64_0 =  IGAsmUnitMethod.PUSH_F64_0;
		public static final int push_f64_1 =  IGAsmUnitMethod.PUSH_F64_1;
		public static final int push_i8 = IGAsmUnitMethod. META_push_i8;
		public static final int push_i16 = IGAsmUnitMethod. META_push_i16;		
			
		public static final int jr_i32 = IGAsmUnitMethod. META_jr_i32;
		
		public static final int debug_block_start = IGAsmUnitMethod.DEBUG_BLOCK_START;
		public static final int debug_block_end = IGAsmUnitMethod.DEBUG_BLOCK_END;
		
		//public static final int local_store_lo = IGAsmUnitMethod. META_local_store_lo;
		//public static final int local_load_lo = IGAsmUnitMethod. META_local_load_lo;
		
		
		
		
		public static final int load_obj_field = IGAsmUnitMethod.META_load_obj_field;//       = 135;
				
		public static final int push_obj_null = IGAsmUnitMethod.META_push_obj_null;
		public static final int push_fp_null  = IGAsmUnitMethod.META_push_fp_null;			
		public static final int END = IGAsmUnitMethod.META_end;
}