package pass2.asm;

import java.util.*;

public class IGSet
{
	public ArrayList<String> m_keys = new ArrayList<String>();

	public List<String> getKeys() {
		return m_keys;
	}

	public void add(String s) {
		if (-1 == m_keys.indexOf(s)) {
			m_keys.add(s);
		}
	}

	public void add(IGSet s) {
		for (int i = 0; i < s.m_keys.size(); i++) {
			add(s.m_keys.get(i));
		}	
	}

}