package pass2.asm;

import java.io.*;
import java.util.*;
import util.ByteArray;
import util.*;
import javax.xml.bind.DatatypeConverter;
import java.util.regex.Pattern;

public final class IGAsmUnit
{
	public static final boolean SORT_MEMBER_VARIABLES = true;
/*

OUTPUT FORMAT:
struct ModuleHeader
{
	ig_i32 m_header;
	ig_i32 m_version;
	
	
	ig_i32 m_full_path
	ig_i32 m_type;
	ig_i32 m_extends;
	ig_i32 m_implements_count;
	
	ig_i32 m_implements[16]
	
	
	ig_u8 m_storage_class;
	ig_u8 padding0;
	ig_u8 padding1;
	ig_u8 padding2;
		
	ig_i32 m_static_variable_count;
	ig_i32 m_member_variable_count;
	ig_i32 m_static_function_count;
	ig_i32 m_member_function_count;
	
	ig_i32 m_pair_pool_entries;			// # of entries
	ig_i32 m_string_entries;			// # of entries
	ig_i32 m_string_pool_size;
	ig_i32 m_data_pool_size;
	
	// static variables
	// member variables
	// static methods
	// member methods
	
	// pair pool
	// string lengths
	// string data
	
};

struct MethodHeader
{
	ig_i32 m_name;
	ig_i32 m_arg_min;
	ig_i32 m_arg_max;
	ig_i32 m_local_count;
	ig_i32 m_data_offset;
	ig_i32 m_spacer;
};

struct VariableHeader
{
	ig_i32 m_name;
	ig_i32 m_type;
	ig_i32 m_parameters;
}

*/

	// 0.1 = optimizations
	// 0.2 = adding support for tracing gc
	// 0.3 = stack height tracing, w/ jump table
	// 0.4 = additional opcodes for optimization purposes
	// 0.5 = support for ref counted 32 bit handles
	// 0.6 = added support for mid execution gc, switches, mla's, bit extraction
	public static final int VERSION = 0x0006;

	public void output(LEDataOutputStream dos) throws IOException {
		dos.writeByte((byte)'I');
		dos.writeByte((byte)'G');
		dos.writeByte((byte)'V');
		dos.writeByte((byte)'M');		
		
		dos.writeInt(VERSION);
		
		dos.writeInt(m_name);
		dos.writeInt(m_type);
		dos.writeInt(m_extends);
		dos.writeInt(m_implements.size());
		
		for (int i = 0; i < 16; i++) {
			if (i < m_implements.size()) {
				dos.writeInt(m_implements.get(i));
			}
			else
			{
				dos.writeInt(0);
			}
		}	
		
		dos.writeByte(0);
		dos.writeByte(0);
		dos.writeByte(0);
		dos.writeByte(0);
		
		dos.writeInt(m_static_variables.size());
		dos.writeInt(m_member_variables.size());
		dos.writeInt(m_static_functions.size());
		dos.writeInt(m_member_functions.size());	
		dos.writeInt(m_pair_pool.size());
		dos.writeInt(m_string_pool.size());
		
		int string_pool_length = 0;
		{
			//m_string_pool_lengths = new int[m_string_pool.size()];
			{
				int sp_sz = m_string_pool_bytes.size();
				for (int i = 0; i < sp_sz; i++) 
				{
					byte [] b = (byte []) m_string_pool_bytes.get(i);
					int l =  b.length;
					
					//m_string_pool_lengths[i] = l;
					string_pool_length += l;
				}
			}
		}
		dos.writeInt(string_pool_length);
		
		int data_pool_length = 0;
		{
			int member_functions = m_member_functions.size();
			int static_functions = m_static_functions.size();

			m_data_pool_lengths = new int[member_functions + static_functions];
			
			int k = 0;
			
			for (int i = 0; i < static_functions; i++) {
				m_data_pool_lengths[k] = m_static_functions.get(i).getLength();
				data_pool_length += m_data_pool_lengths[k];
				k++;
			}		
			
			
			for (int i = 0; i < member_functions; i++) {
				m_data_pool_lengths[k] = m_member_functions.get(i).getLength();
				data_pool_length += m_data_pool_lengths[k];
				k++;
			}		
		}
		
		dos.writeInt(data_pool_length);
		
		
		
		for (int i = 0; i < m_static_variables.size(); i++) {
			m_static_variables.get(i).output(dos);
		}		

		for (int i = 0; i < m_member_variables.size(); i++) {
			m_member_variables.get(i).output(dos);
		}		
		
		int data_offset = 0;
		int k = 0;
		{
			for (int i = 0; i < m_static_functions.size(); i++) {
				m_static_functions.get(i).output(dos, data_offset);
				data_offset += m_data_pool_lengths[k]; k++;
			}		

			for (int i = 0; i < m_member_functions.size(); i++) {
				m_member_functions.get(i).output(dos, data_offset);
				data_offset += m_data_pool_lengths[k]; k++;
			}	
		}
		

		int sp_sz = m_string_pool_bytes.size();
		//for (int i = 0; i < sp_sz; i++) {
		//	dos.writeInt(m_string_pool_lengths[i]);
		//}
		
		
		for (int i = 0; i < sp_sz; i++) {
			byte [] b = (byte []) m_string_pool_bytes.get(i);
			dos.writeInt(b.length);
		}
		
		for (int i = 0; i < sp_sz; i++) {
			byte [] b = (byte []) m_string_pool_bytes.get(i);
			dos.write(b);
		}

		for (int i = 0; i < m_pair_pool.size(); i++) {
			dos.writeLong(m_pair_pool.get(i));
		}

		
		{
			for (int i = 0; i < m_static_functions.size(); i++) {
				m_static_functions.get(i).outputData(dos);
			}		

			for (int i = 0; i < m_member_functions.size(); i++) {
				m_member_functions.get(i).outputData(dos);
			}	
		}
		
	}



	public static final int TYPE_CLASS     = 1;
	public static final int TYPE_ENUM      = 2;
	public static final int TYPE_INTERFACE = 3;

	int    m_type = 0;
	int    m_name = 0;
	int    m_extends = 0;
	ArrayList<Integer> m_implements = new ArrayList<Integer>();
	
	//public Hashlist m_labels = new Hashlist();
	public ArrayList<IGAsmUnitVariable> m_static_variables = new ArrayList<IGAsmUnitVariable>();
	public ArrayList<IGAsmUnitVariable> m_member_variables = new ArrayList<IGAsmUnitVariable>();	
	public ArrayList<IGAsmUnitMethod>   m_static_functions = new ArrayList<IGAsmUnitMethod>();
	public ArrayList<IGAsmUnitMethod>   m_member_functions = new ArrayList<IGAsmUnitMethod>();
	
	
	public void sortMemberVariables()
	{
		if (SORT_MEMBER_VARIABLES) {
			Collections.sort(m_member_variables);
		}
	
		/*
		System.out.println("sortMemberVariables");
		
		for (IGAsmUnitVariable v : m_member_variables) {
			System.out.println("\t" + v.m_storage_class);
		}
		*/
	
	}
	
	
	// is this the right approach? (yeah I think it'll do)
	public ArrayList<String> m_string_pool       = new ArrayList<String>();
	public ArrayList<Object> m_string_pool_bytes = new ArrayList<Object>();
	//private StringIntHashtable m_string_pool_membership = new StringIntHashtable();
	
	public int [] m_string_pool_lengths;
	public int [] m_data_pool_lengths;
	public LongVector    m_pair_pool   = new LongVector();
	//ArrayList<Integer> m_i32_pool   = new ArrayList<Integer>();
	//LongVector    m_i64_pool   = new LongVector();
	
	public void clear() {
		//m_string_pool_membership.clear();
	}
			
	public void setClassName(String n) {
		//System.err.println("class: " + n);
		m_name = _stringPool(n);
		m_type = TYPE_CLASS;
	}
	
	public void setEnumName(String n) {
		m_name = _stringPool(n);
		m_type = TYPE_ENUM;
	}
	
	public void setInterfaceName(String n) {
		m_name = _stringPool(n);
		m_type = TYPE_INTERFACE;
	}
	
	public void setExtends(String n) {
		//System.err.println("extends: " + n);
		if (m_type != TYPE_CLASS) {
			throw new RuntimeException("Only classes may extend other classes");
		}
		
		m_extends = _stringPool(n);
	}
	
	public void addImplements(String n) {
		if (m_type != TYPE_CLASS) {
			throw new RuntimeException("Only classes may implement other classes");
		}
		
		m_implements.add(_stringPool(n));
	}
	

	
	
	
	public String stringPool(String s) {
		_stringPool(s);
		return s;
	}
	
	// hrm... wont th
	public int _stringPool(String s) 
	{
		int idx = m_string_pool.indexOf(s);
		if (idx != -1) {
			return idx;
		}
		
		try
		{
			m_string_pool.add(s);
			m_string_pool_bytes.add(s.getBytes("UTF-8"));
		}
		catch (UnsupportedEncodingException ex) {
			throw new RuntimeException("barf..");
		}
		
		return m_string_pool.size() - 1;
		
	}
	
	
	public int pairPool(String a, String b) {
		return pairPool(_stringPool(a), _stringPool(b));
	}
	
	public int pairPool(int a, int b) {
		long la = a;
		long lb = b;
		long s = ((la & 0x00000000FFFFFFFFL) | ((lb & 0x00000000FFFFFFFFL) << 32));
		
		int idx = m_pair_pool.indexOf(s);
		if (idx >= 0) {
			return idx;
		}
		//for (int i = 0; i < m_pair_pool.size(); i++) {
		//	if (m_pair_pool.get(i) == s) {
		//		return i;
		//	}
		//}
		m_pair_pool.add(s);
		return m_pair_pool.size() - 1;		
	}
}