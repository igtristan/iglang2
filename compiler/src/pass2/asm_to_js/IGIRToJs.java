package pass2.asm_to_js;

import pass1.*;

/**
This doesn't correctly handle the following cases

class A
{
	public static var X :int = 0;
	public static function a():void {
	
	}
}

class B extends A {
	public static function b():void {
		a();
		trace ("A": " + X);
	}
}
*/

import java.util.*;
import pass2.asm.*;

import java.io.*;
public class IGIRToJs
{
	public static void touch() {

	}

	int m_sp = 0;
	int m_arg_count = 0;
	StringBuilder m_out = new StringBuilder();
	int m_indent = 0;
	HashMap<String,Label> m_labels;
	int m_label_count;

	static class Variable
	{
		String m_name;
		boolean m_is_param;

		public Variable(String n, boolean p) {
			m_name = n;
			m_is_param = p;
		}
	}

	private ArrayList<Variable> m_active_variables_list = new ArrayList<Variable>();
	private HashMap<String,Variable> m_active_variables = new HashMap<String,Variable>();

	public void clearVariables() {
		m_active_variables = new HashMap<String,Variable>();
		m_active_variables_list = new ArrayList<Variable>();
	}

	public void registerVariable(String name, boolean is_param) {
		if (!m_active_variables.containsKey(name)) {
			Variable v = new Variable(name,is_param);
			m_active_variables.put(name, v);
			m_active_variables_list.add(v);
		}
	}
	public void push(int idx)
	{
		m_sp ++;
		writeS0(rvar(idx));
	}

	public void indent(int amount) {
		m_indent+= amount;
	}


	private String m_s0_pending_left = null;
	private String m_s0_pending_right = null;



	public void writeFlush() {
		if (m_s0_pending_left != null) {
			String left = m_s0_pending_left;
			String right =m_s0_pending_right;

			m_s0_pending_left = null;
			m_s0_pending_right = null;

			writes(left + "=" + right);
			
		}	
	}

	public void writeS0(String right) {
		//writes(stack(0) + "=" + right);

		
		if (m_s0_pending_left != null) {
			writeFlush();
		}
		
		m_s0_pending_left  = stack(0);
		m_s0_pending_right = right;	
		
	}


	public void write(String s) {
		writeFlush();

		for (int i = m_indent; i > 0; i--) { m_out.append("\t"); }
		m_out.append(s);
		m_out.append("\n"); //" sp:" + m_sp + 
	}

	public void writes(String s) {
		writeFlush();

		for (int i = m_indent; i > 0; i--) { m_out.append("\t"); }
		m_out.append(s);
		m_out.append(";\n");  //" sp:" + m_sp +

	//	System.out.println("OUTPUT: " + s + "; ## sp=" + m_sp);
	}

	


	public String S(int offset) {
		return stack(offset);
	}
	public String stack(int offset) {

		int o =  (m_sp - 1 + offset);
		if (o < 0) 
		{ 
			System.out.println("CURRENT DATA");
			System.out.println(m_out);
			System.out.println("SADNESS");
			throw new RuntimeException("Invalid Stack Offset: " + m_current_module + " " + m_current_member + " " + m_op); 
		}
		registerVariable("s" + o, false);
		return "s" + o;
	}

	public String lstack(int offset) {
		return stack(offset) + "=";
	}

	public String rstack(int offset) {
		String rname = stack(offset);

		if (rname.equals(m_s0_pending_left)) {
			String ret = m_s0_pending_right;
			m_s0_pending_left = null;
			m_s0_pending_right = null;

			return "("+ret+")";
		}

		return rname;
	}



	public String lvar(String offset) {
		return rvar(Integer.parseInt(offset)) + "=";
	}

	public String lvar(int offset) {
		return rvar(offset) + "=";
	}

	public String rvar(int offset) {
		//m_active_variables[offset] = true;
		if (offset < 0) { throw new RuntimeException("Invalid Variable Offset"); }
		registerVariable("v" + offset, false);
		return "v" +  offset;

	}

	public String pvar(int offset) {
		//m_active_variables[offset] = true;
		if (offset < 0) { throw new RuntimeException("Invalid Variable Offset"); }
		registerVariable("v" + offset, true);
		return "v" +  offset;

	}


	public String var(int offset) {
		return rvar(offset);
	}

//	public String var(String offset) {
//		return rvar(Integer.parseInt(offset));
//	}




	public void binary(String fn) {
		// WRITES 0, CONSUMES 0 and 1
		m_sp--;
		String s = rstack(0) + fn + rstack(1);
		writeS0(s);
	}

	public void compare(String fn) {


		// WRITES 0, CONSUMES 0 and 1
		m_sp--;
		String s = "+(" + rstack(0) + fn + rstack(1) + ")";
		writeS0(s);
	}

	public void ibinary(String fn) {

		m_sp--;
		String s = "(" + rstack(0) + fn + rstack(1) + ")|0";
		writeS0(s);
	}

	public void iunary(String fn) {
		String s =  fn + rstack(0);
		writeS0(s);
	}

	public String binary_vec(String fn, int count) {

		
		StringBuilder sb = new StringBuilder();

		//System.out.println("sp: " + m_sp + " count: " + count);
		m_sp -= count;
		for (int i = 0; i < count; i++)
		{
			int dst_slot = i - count + 1;
			sb.append(stack(dst_slot) + "=(" + stack(dst_slot) + fn + stack(dst_slot + count) + ")"+"; ");
		}

		return sb.toString();
	}

	public String binary_vec_val(String fn, int count) {

		
		StringBuilder sb = new StringBuilder();

		//System.out.println("sp: " + m_sp + " count: " + count);
		m_sp -= 1;
		for (int i = 0; i < count; i++)
		{
			int dst_slot = i - count + 1;
			sb.append(stack(dst_slot) + "=(" + stack(dst_slot) + fn + stack(1) + ")"+"; ");
		}

		return sb.toString();
	}

	

	static final class Label {
		public int m_id;
		public int m_sp;

		public Label(int i,int p) { m_id = i; m_sp = p; }

	}


	public Label generateLabel(String label, int sp)
	{
		Label existance = m_labels.get(label);
		if (existance == null) {
			m_label_count++;
			existance = new Label(m_label_count, sp);
			m_labels.put(label, existance);
		}
		return existance;
	}

	public String GOTO(String label)
	{
		Label existance = generateLabel(label, m_sp);
		return "pc=" + existance.m_id + ";continue;";
	}

	public String LABEL(String label) {
		Label existance = generateLabel(label, -1);
		//System.out.println("LABEL: " + label + " sp: " + m_sp + " spec'd: " + existance.m_sp);
		return "case " + existance.m_id + ":";
	}	

	public int getSPForLabel(String label)
	{
		Label lbl = m_labels.get(label);
		if (lbl == null) {
			System.out.println("WARNING This label was completely inaccessible: " + label);
			return 0;
		}
		return lbl.m_sp;
	}

	public int getIndexForLabel(String label)
	{
		Label lbl = m_labels.get(label);
		if (lbl == null) {
			System.out.println("SPMissing '" + label + "'");
		}
		return lbl.m_id;
	}


	/**

		Okay anyways ,... we dont properly handle all the remaining array ops that
		were never directly coded into ops
	*/
	public void dumpParameters(IGIROp op, int uniform_op, int ret_count)
	{

		//writeFlush();

		StringBuilder tmp = new StringBuilder();

		int argc = op.getParameterCount();	//getArgCount();

		// include the function pointer into the argument count
		// for code gen
		if (uniform_op == IGIR.CALL_DYNAMIC) {
			argc++;
		}


		m_sp -= argc;


		boolean disable_first_escape = false;


		//if (ret_count != 0) {
		//	tmp.append(lstack(1));
		//}

		// all the ret1 varients have been changed into their NON ret1 versions
		switch(uniform_op) 
		{
			case IGIR.CALL_INTERFACE     :
			{ 
				disable_first_escape = true;
				tmp.append(stack(1) + ".$vtable." + getSafeMemberName(op.name)); 
				break;  
			}
			case IGIR.CALL_VTABLE        :
			{

				disable_first_escape = true;
				tmp.append(stack(1) + ".$vtable." + getSafeMemberName(op.name)); 
				break;  
			}
			case IGIR.CALL_STATIC:
			{
				IGIRModule mod = getModuleByType(op.module);
				if (null == mod) {
					throw new RuntimeException("Failed to resolve module: " + op.module);
				}
				Type module = mod.resolve(op.name).m_module.m_type; 
				String module_name = getSafeModuleName(module);
				String member_name = getSafeMemberName(op.name);

				// an early out case
				//if (module_name.equals("iglang.Math") && member_name.equals("square")) {
				//	tmp.append(stack(1) + "*" + stack(1));
				//	m_sp += ret_count;	
				//	return tmp.toString();
				//}

				tmp.append("IGVM.MODULE." + module_name + "." + member_name); 
				break;  
			}
			case IGIR.CALL_DYNAMIC       :{ 
				tmp.append("IGVM._invokeDynamic");
				break; 
			}
			case IGIR.CALL_SUPER         :
			{ 
				tmp.append("IGVM.MODULE." + getSafeModuleName(op.module) + 
					 ".$vtable.$parent."  + getSafeMemberName(op.name)); ; 
				break; 
			}
				//		tmp.append(stack(1) + ".$vtable.$parent." + getSafeMemberName(op.m_p2)); ; break; }
	
		}


			
		

		tmp.append('(');
		for (int i = 0; i < argc; i++) {
			if (i != 0) tmp.append(',');

			// never replace out the first parameter
			if (i == 0 && disable_first_escape) {
				tmp.append(stack(i + 1));
			}
			else {
				tmp.append(rstack(i + 1));
			}
		}
		tmp.append(')');

		m_sp += ret_count;	

		if (ret_count != 0) {
			writeS0(tmp.toString());
		}
		else {
			writes(tmp.toString());
		}
	}

	// this is an optimization opportunity
	//public HashMap<String,String> m_safe_names = new HashMap<String,String>();

	public static String getSafeMemberName(String member_name) {
		// TODO
		if ("<constructor>".equals(member_name)) {
			return "$constructor";
		}
		else if ("<static>".equals(member_name)) {
			return "$static";
		}
		else if ("<get[]>".equals(member_name)) {
			return "$aget";
		}
		else if ("<set[]>".equals(member_name)) {
			return "$aset";
		}
		return member_name;
	}


	public static String getSafeModulePath(IGIRModule module) {
		return getSafeModulePath(module.m_type.toString());
	}

	public static String getSafeModulePath(Type module) {
		return getSafeModulePath(module.toString());
	}

	public static String getSafeModulePath(String module_name) {
		return "IGVM.MODULE." + getSafeModuleName(module_name);
	}

	public static String getSafeModuleName(IGIRModule module) {
		return getSafeModuleName(module.m_type.toString());
	}

	public static String getSafeModuleName(Type module) {
		return getSafeModuleName(module.toString());
	}

	public static String getSafeModuleName(String module_name) {
		if (module_name == null) {
			throw new RuntimeException("Null module_name");
		}
		// TODO
		StringBuilder tmp = new StringBuilder();
		int len = module_name.length();
		for (int i = 0; i < len; i++) {
			final char c = module_name.charAt(i);
			switch(c) {
				case '.': { tmp.append("$$"); break; }
				case '<': { tmp.append("$B"); break; }
				case '>': { tmp.append("$E"); break; }
				case ',': { tmp.append("$_"); break; }
				
				default:
					tmp.append(c);
			}
		}
		return tmp.toString();
	}

	private String lstatic(Type module, String field) 
	{
		IGIRModule.Variable v= (IGIRModule.Variable)getModuleByType(module).resolve(field);

		module = v.m_module.m_type; 

		//if (m_active_vtable.pathMatches(module)) {
		//	JsVtable mod = m_active_vtable;
		//	while (!mod.containsStaticVariable(field)) {
		//		mod = mod.m_parent;
		//	}
		//	module = mod.m_path;
		//}
		return getSafeModulePath(module) + "." + getSafeMemberName(field) + "=";
	}



	private String toStringValue(IGIRModule.Variable v) 
	{
		if (!v.m_initial_value.isConcrete()) {
			throw new RuntimeException("ValueRange is not concrete: " + v);
		}
		int storage_class = v.m_type.getStorageClass();

		if (v.m_type == Type.BOOL) {
			return v.m_initial_value.boolValue() ? "1" : "0";
		}
		else if (storage_class == Type.STORAGE_8 ||
			storage_class == Type.STORAGE_16 ||
			storage_class == Type.STORAGE_32) {
			return "" + v.m_initial_value.intValue();
		} 
		else if (storage_class == Type.STORAGE_F64 ||
				 storage_class == Type.STORAGE_F32) {
			return "" + v.m_initial_value.doubleValue();
		}
		else {
			// TODO add more error checks around this
			return "null";
		}
	}

	private String rstatic(Type module, String field) 
	{
		IGIRModule.Variable v= (IGIRModule.Variable)getModuleByType(module).resolve(field);
		if (v.isFinal() && 
			v.m_initial_value != null && 
			v.m_initial_value.isConcrete()) 
		{
			return toStringValue(v);
		}
		
		module = v.m_module.m_type; 

		//if (m_active_vtable.pathMatches(module)) {
		//	JsVtable mod = m_active_vtable;
		//	while (!mod.containsStaticVariable(field)) {
		//		mod = mod.m_parent;
		//	}
		//	module = mod.m_path;
		//}/
		return getSafeModulePath(module) + "." + getSafeMemberName(field);// + " " + v.m_initial_value;
	}



	private static Map<Type,IGIRModule> s_modules = new HashMap<Type,IGIRModule>();

	private static IGIRModule getModuleByType(Type t) {
		return s_modules.get(t);
	}

	public static void run(String arg_dst_directory, Vector<IGIRModule> to_run, int optimization_level)
	{
		for (IGIRModule target : to_run) {
			s_modules.put(target.m_type, target);
		//	targets.put(target.m_type.toString(), target);
		}

		// remove excessive labels
		//for (IGIRModule target : to_run) {
		//	target.optimize(targets);
		//s}

		boolean skipped = true;
		boolean added   = false;


		//HashMap<String,JsVtable> vtables = new HashMap<String,JsVtable>();
		ArrayList<IGIRModule> ordered_targets = new ArrayList<IGIRModule>();
		Set<IGIRModule> loaded = new HashSet<IGIRModule>();

		while (skipped) {
			skipped = false;
			added   = false;
			// everything that is extended is now in the correct order
			for (IGIRModule target : to_run) 
			{
				if (loaded.contains(target)) {
					continue;
				}

				IGIRModule extends_module = target.getExtends();
				if (extends_module != null && !loaded.contains(extends_module)) {
					skipped = true;
					continue;
				}

				
				added = true;
				ordered_targets.add(target);

				try
				{
					StringBuilder data = new StringBuilder();
					File dst = new File(arg_dst_directory + typeToPath(target.m_type) + ".js");
					PrintWriter pw = new PrintWriter(dst);

					new IGIRToJs().processTarget(target, data);
				
					pw.print(data.toString());
					pw.close();
				}
				catch (IOException ex) {
					ex.printStackTrace();
					System.out.println("Failed in thread");
				}


				

				loaded.add(target);
			}

			if (!added) {
				throw new RuntimeException("Recursive includes :(");
			}
		}


		try
		{
			// node js suitable output	
			{
				PrintWriter pw = new PrintWriter(arg_dst_directory + "index.js");
				pw.println("var fs = require(\"fs\");");
				pw.println("var vm = require(\"vm\");");
				pw.println("");
				pw.println("function include(path) {");
				//pw.println("    console.log('loading: ' + path );");
				pw.println("    var code = fs.readFileSync(__dirname + '/' + path, 'utf-8');");
				pw.println("    vm.runInThisContext(code);");
				pw.println("}");
				pw.println("");
				pw.println("include(\"igvm_core.js\");");
				pw.println("module.exports = IGVM;");

				for (IGIRModule target : ordered_targets) {
					pw.println("include(\"" + typeToPath(target.m_type) + ".js\");");
				}

				pw.println("console.log('Initializing...');");
				pw.println("IGVM.init();");
				pw.println();

				// output the static initializer order
				///////////////////////////////////////
				for (IGIRModule target : ordered_targets) 
				{
					pw.println(getSafeModulePath(target.m_type) + ".$static();");
				}


				pw.close();
			}

			// see https://www.html5rocks.com/en/tutorials/speed/script-loading/
			// standard js suitable output
			// we'll also add a mode that outputs the whole codebase in a single
			// file
			{

				PrintWriter pw2 = new PrintWriter(
					arg_dst_directory + "igvm_static_init.js");


				//pw.println("function igvm_static_init(){");
				for (IGIRModule target : ordered_targets) 
				{
				//	IGAsmDocument w = target.getWriter();
				//	String module_extends = w.m_name_fully_qualified;

					//pw2.println("console.log('" + module_extends + "');");
					pw2.println(getSafeModulePath(target.m_type) + ".$static();");
				}

				pw2.close();

				PrintWriter pw = new PrintWriter(
					arg_dst_directory + "igvm.js");


			///	pw.println("}");


				/**
					PHILOSOPHY
					1) include igvm.js - this should by default load everything
					2) have any native call actually trigger the static initialization process
				**/


				pw.println("var igvm = (function(){");
				{
					pw.println("'use strict';");

					// scripts that are loaded dynamically are considered to be async
					// this is clearly not our case.  Order does verymuch matter
					// for initializinng parent classes
					pw.println("function include(url) {");
					pw.println("\tvar script = document.createElement( 'script' );");
					pw.println("\tscript.type = 'text/javascript';");
					pw.println("\tscript.async = false;");
					pw.println("\tscript.src = url;");
					pw.println("\tdocument.body.appendChild( script );");
					pw.println("\treturn script;");
					pw.println("}");


					pw.println("return {");
					pw.println("m_callback:null,");
					pw.println("load:function(path_to_scripts, cbk){");
					pw.println("\tthis.m_callback = cbk; ");
					pw.println("\tvar last = include(path_to_scripts+\"igvm_core.js\");");
					for (IGIRModule target : ordered_targets) {
						pw.println("\tlast = include(path_to_scripts+\"" + typeToPath(target.m_type) + ".js\");");
					}

					// user libraries also need to know when the code base has been loaded
					// but do I want to do this reactive the way the IGVM code is set up
					pw.println("\tlast.onload = function() { IGVM.init(); };");
					pw.println();

					pw.println("\tlast = include(path_to_scripts+\"igvm_static_init.js\");");
					//StringBuilder instructions = new StringBuilder();
					pw.println("\tlast.onload = function() { cbk(); };");

					//pw.println("\tincludeText(\"igvm.m_callback();\");");
					pw.println("}}");
				}
				pw.println("})();");

				pw.close();
			}
		}
		catch (IOException ex) {
			ex.printStackTrace();
			System.out.println("messed up");
		}

	}

	private static String typeToPath(Type t) {
		return t.toString().replace("<", "{").replace(">","}");
	}


	private String   m_current_member = null;
	private IGIRModule   m_current_module = null;
	//private JsVtable m_active_vtable = null;
	// this is neet
	public void processTarget(IGIRModule module, StringBuilder _out) {

		m_out = _out;
		m_current_module = module;;

		write("(function(IGVM){ \"use strict\"; ");
		// gather all the static variables and static functions for the module
		// should this also pull in functions from extended scopes (LIKELY)
		write(getSafeModulePath(module) + " = {");
		{
			indent(1);


			for (IGIRModule.Function h : module.m_static_functions) 
			{	
				m_current_member = h.getName();
				write("\"" + getSafeMemberName(h.getName()) + "\" :");

				// dump out the function bodies
				processTargetMember(module, h );
				write(",");
			}

			for (IGIRModule.Function h : module.m_member_functions) 
			{
				m_current_member = h.getName();
				write("\"" + getSafeMemberName(h.getName()) + "\" :");

				// dump out the function bodies
				processTargetMember(module,  h );
				write(",");
			}


			// what about if the value is already known?
			for (IGIRModule.Variable v : module.m_static_variables) 
			{
				m_current_member = v.getName();
				write(getSafeMemberName(v.getName()) + " : " + getDefaultForStorageClass(v.m_type.getStorageClass()) + ",");
			}

			write("'$vtable': null,");
			write("'$name': '" + escapeString(module.m_type.toString()) + "',");

			
			{
				// this is only technically valid for a class
				// will need to do more legwork for interfaces
				write("'$instanceof': function (other) {");
				indent(1);
				write("if (null == other) { return true; }");
				writes("var other_vtable = other.$vtable");
				writes("var my_vtable    = " + getSafeModulePath(module) + ".$vtable");
				write("while(null != other_vtable) {" );
				indent(1);
					write("if(other_vtable == my_vtable){return true;}");
					write("other_vtable = other_vtable.$parent;");
				indent(-1);
				write("}");
				writes("return false");
				write("}");
				indent(-1);
			}
			indent(-1);
		}
		write("};");

		if (module.isClass())
		{
			///////////////////////////////
			// OUTPUT THE VTABLE
			/////////////////////////////
			write("IGVM.MODULE." + getSafeModuleName(module) + ".$vtable = {");
			{
				indent(1);
				if (module.getExtends() == null) {
					write("'$parent':\tnull");
				}
				else {
					write("'$parent':\t" + getSafeModulePath(module.getExtends()) + ".$vtable");
				}


				write(",'$name': '" + escapeString(module.m_type.toString()) + "'");

				for (IGIRModule.Function fn : module.getVTable()) {
					String fn_name = getSafeMemberName(fn.m_name);
	    			write("," + fn_name + ":\t" + getSafeModulePath(fn.m_module) + "." + fn_name);
				}


				indent(-1);
			}
			write("};");

			/////////////////////////////////
			// PROTOTYPE
			////////////////////////////////

			String parent_defaults = "{}";
			if (module.getExtends() != null) {
				parent_defaults = "IGVM.OBJECT." + getSafeModuleName(module.getExtends());
			}
			
			// gather all the initial state for an instantiated module
			write("IGVM.OBJECT." + getSafeModuleName(module) + "= Object.assign({}," +parent_defaults + ", {");
			{
				indent(1);
				write("'$vtable' : " + getSafeModulePath(module) + ".$vtable");


				List<IGIRModule.Variable> fns = module.m_member_variables;
				for (IGIRModule.Variable v : fns) {
					write(", '" + getSafeMemberName(v.getName()) + "': " + getDefaultForStorageClass(v.m_type.getStorageClass()));	
				}

				indent(-1);
			}
			write("});");
		}



		write("})(IGVM);");

		// SHOULD THROW PARENT IF IT EXISTS INTO THE PROTOTYPE CHAIN?
		// or inlude all its members in the fn

		// should all of the previous just be in the prototype?
		// probably
		//writes("IGVM.OBJECT." + getSafeModuleName(module) + ".prototype.$vtable = " +
		//		getSafeModulePath(module) + ".$vtable");

		

		// are prototypes mutable?
		// should it 

	}



	public String createArray(int storage_class) {
		// sp[0] contains the number of elements
		if (Type.STORAGE_8 == storage_class) {
			return "IGVM.newInt8Array(" + rstack(0) + "|0)";
		}
		else if (Type.STORAGE_16 == storage_class) {
			return "IGVM.newInt16Array(" + rstack(0) + "|0)";
		} 
		else if (Type.STORAGE_32 == storage_class) {
			return "IGVM.newInt32Array(" + rstack(0) + "|0)";
		} 
		else if (Type.STORAGE_F64 == storage_class) {
			return "IGVM.newFloat64Array(" + rstack(0) + "|0)";
		}
		else if (Type.STORAGE_F32 == storage_class) {
			return "IGVM.newFloat32Array(" + rstack(0) + "|0)";
		}
		else if (Type.STORAGE_OBJ == storage_class) {
			return "IGVM.newObjectArray(" + rstack(0) +"|0)"; // ES2015
		}
		else if (Type.STORAGE_FUNC_PTR == storage_class) {
			return "IGVM.newFuncPtrArray(" + rstack(0) +"|0)"; // ES2015
		}
		else {
			throw new RuntimeException("Unsupported storage_class");
		}
	}

	public String copyArray(String dst, String src, int storage_class) 
	{		

		StringBuilder sb = new StringBuilder();
		return "IGVM.copyArray(" + dst + "," + src + ")";

		/*
		//sb.append("if (dst.length < src.length)")

		//STORAGE_32F
		// sp[0] contains an existing array of the appropriate type
		if (Type.STORAGE_8 == storage_class) {
			return "IGVM.dupInt8Array(" + stack(0) + ")";
		}
		else if (Type.STORAGE_16 == storage_class) {
			return "IGVM.dupInt16Array(" + stack(0) + ")";
		} 
		else if (Type.STORAGE_32 == storage_class) {
			return "IGVM.dupInt32Array(" + stack(0) + ")";
		} 
		else if (Type.STORAGE_F64 == storage_class) {
			return "IGVM.dupFloat64Array(" + stack(0) + ")";
		}
		else if (Type.STORAGE_F32 == storage_class) {
			return "IGVM.dupFloat64Array(" + stack(0) + ")";
		}
		else if (Type.STORAGE_OBJ == storage_class) {
			return "IGVM.dupObjectArray(" + stack(0) + ")"; // slice 
		}
		else if (Type.STORAGE_FUNC_PTR == storage_class) {
			return "IGVM.dupFuncPtrArray(" + stack(0) + ")"; // slice}
		}
		else {
			throw new RuntimeException("Unsupported storage_class");
		}
		*/
	}

	public String resizeArray(int storage_class) 
	{
		String def = "0";

// sp[0] contains an existing array of the appropriate type
		if (Type.STORAGE_8 == storage_class) {
			return lstack(0) + "IGVM._resizzle(IGVM.newInt8Array(" + stack(1)    + "|0), " + stack(0) + ");";
		}
		else if (Type.STORAGE_16 == storage_class) {
			return lstack(0) + "IGVM._resizzle(IGVM.newInt16Array(" + stack(1)   + "|0), " + stack(0) + ");";
		} 
		else if (Type.STORAGE_32 == storage_class) {
			return lstack(0) + "IGVM._resizzle(IGVM.newInt32Array(" + stack(1)   + "|0), " + stack(0) + ");";
		} 
		else if (Type.STORAGE_F64 == storage_class) {
			return lstack(0) + "IGVM._resizzle(IGVM.newFloat64Array(" + stack(1) + "|0), " + stack(0) + ");";
		}
			else if (Type.STORAGE_F32 == storage_class) {
			return lstack(0) + "IGVM._resizzle(IGVM.newFloat32Array(" + stack(1) + "|0), " + stack(0) + ");";
		}
		else if (Type.STORAGE_OBJ == storage_class) {
			def = "null";
		}
		else if (Type.STORAGE_FUNC_PTR == storage_class) {
			def = "null";
		}
		else {
			throw new RuntimeException("Unsupported storage_class");
		}


		return 
			"while (" + stack(0)+".length<" + stack(1) + "){" + stack(0) + ".push("+def+"); }" +
			stack(0) + ".length = " + stack(1)  + ";";

		
	}


	public String escapeString(String s) {
		StringBuilder sb = new StringBuilder();
		final int len = s.length();
		for (int i = 0; i < len; i++) {
			char c = s.charAt(i);
			if (c == '"') {
				sb.append("\\");
				sb.append("\"");
			}
			else if (c == '\\') {
				sb.append("\\");
				sb.append("\\");
			}
			else if (c == '\n') {
				sb.append("\\");
				sb.append("n");
			}
			else if (c == '\t') {
				sb.append("\\");
				sb.append("t");
			}
			else {
				sb.append(c);
			}
		}

		return sb.toString();
	}

	public String getDefaultForStorageClass(int storage_class) {
		if (Type.STORAGE_8 == storage_class ||
			Type.STORAGE_16 == storage_class ||
			Type.STORAGE_32 == storage_class) {
			return "0";
		} 
		else if (Type.STORAGE_F64 == storage_class ||
				 Type.STORAGE_F32 == storage_class) {
			return "0.0";
		}
		else if (Type.STORAGE_OBJ == storage_class ||
			Type.STORAGE_FUNC_PTR == storage_class) {
			return "null";
		}
		else {
			throw new RuntimeException("Unsupported storage_class");
		}
	}

	public void createStaticPointer(IGIROp op) 
	{
		m_sp++; 
		IGIRModule module = getModuleByType(op.module).resolve(op.name).m_module;
		writeS0("{'o':null," +
			    "'m':'" + getSafeModuleName(module) + "'," +
			    "'f':'" + getSafeMemberName(op.name) + "'}");
	}

	public void createMemberPointer(IGIROp op){
		writeS0("{'o':"  + stack(0) + "," +
			    "'m':'" + getSafeModuleName(op.module) + "'," +
			    "'f':'" + getSafeMemberName(op.name) + "'}");
	}

	/**
	 * Process a function within a member
	 */

	IGIROp m_op = null;
	public void processTargetMember(IGIRModule target, IGIRModule.Function header)
	{

		StringBuilder primary = m_out;

		clearVariables();

		//
		m_sp = 0;

		m_out          = new StringBuilder();
		m_labels       = new HashMap<String,Label>();
		m_label_count  = 0;
	//	m_active_variables = new boolean[256];

		//String fn_name = header.getName();
		int argc = header.m_max_parameter_count;


		

		final List<IGIROp> ops = header.m_ops;
		final int op_count = ops.size();


		int prev_op = -1;
		
		// check if there are any labels defined
		boolean has_labels = false;
		for (int i = op_count - 1; i >= 0; i--) {
			if (ops.get(i).label != null) {
				has_labels = true;
				break;
			}
		}

		// mark the variables that are parameters
		for (int i = 0; i < argc; i++) {
			registerVariable("v" + i, true);
		}


		for (int i = 0; i < header.m_parameters.size(); i++) 
		{
			IGIRModule.Variable v = header.m_parameters.get(i);
			if (v.m_initial_value != null) {
				write("\tif (typeof " + var(i) + " === 'undefined') {");
				write("\t" + lvar(i) + " " + toStringValue(v) + ";}");
			}

/*
			if (v.m_type.isIntishOrIntishEnum()) {
				write("\t" + lvar(i)  + var(i) + "|0;");
			}
			else if (v.m_type == Type.DOUBLE) {
				write("\t" + lvar(i)  + "+" + var(i) + ";");	
			}
*/
		}


		indent(1);

		//System.out.println("################################ " + target.m_full_path + " " + header.getName());
		
		if (has_labels)
		{
			write("var pc = 0; while(true) { switch(pc) {");
			write("case 0: ");
		}
/*
	
		for (int i = 0; i < op_count; i++)
		{
			System.out.println(ops.get(i));
		}
*/
		try
		{
		for (int i = 0; i < op_count; i++)
		{
			final IGIROp op = ops.get(i);
			m_op = op;


			if (prev_op == IGIR.JMP    || 
				prev_op == IGIR._THROW || 
				prev_op == IGIR.RET1   ||
				prev_op == IGIR.RET1_I32 ||
				prev_op == IGIR.RET0)
			{
				if (op.op != IGIR.END)
				{
					// need to pull pc
					if (op.label != null) {
						
						int sp = getSPForLabel(op.label);
						if (-1 != sp) {
							m_sp = sp;
						}

					}
					else {
						System.err.println("Unacessible code. " + target + " "
							+ header + " OP: " + op + " " + op.op);
					//throw new RuntimeException("Unacessible code: " + op + " " + op.m_op);
					}
				}
			}
			

			op.stack = m_sp;	
			prev_op = op.op;


			if (op.label != null) {
				indent(-1);
				write(LABEL(op.label));
				indent(1);
			}

			// should track the state in the stack

			switch (op.op)
			{
				case IGIR.NOP      :{ break; }
				case IGIR.POP      :{ m_sp -= op.count; break; }  // ..X => ..
				case IGIR.DUP  : { 
					m_sp+= op.count;
					for (int k = 0; k < op.count; k++) {
						writes(lstack(0 - k) + stack(-k - op.count));  
					}
					break; } // ..A => ..AA
				case IGIR.DUP2 : { 
					m_sp += 2;
					writes(lstack(-1) + rstack(-3));
					writes(lstack(-0) + stack(-2));  
					break; } // ..AB => ..ABAB
				case IGIR.DUP1X :{ // AB=>BAB
					m_sp ++;
					writes(lstack(0)  + rstack(-1)); //AB?* => ABB*;
					writes(lstack(-1) + stack(-2)); //     => AAB*
					writes(lstack(-2) + stack(0));  //     => BAB*
					break; } 
				case IGIR.DUP2X :{ 
					m_sp ++;
					writes(lstack(0)  + rstack(-1)); //ABC?* => ABCC*;
					writes(lstack(-1) + stack(-2)); //      => ABBC*
					writes(lstack(-2) + stack(-3));  //     => AABC*
					writes(lstack(-3) + stack(0));  //      => CABC*
					break; } 
				case IGIR.RET0         :{ writes("return");	break; }
				case IGIR.RET1_I32     :{ writes("return " +  (int)op.value + "|0");	break; }
				case IGIR.RET1         :{ writes("return " + rstack(0));	break; }
				case IGIR.ADD_I32      :{ ibinary("+");	   break; }
				case IGIR.SUB_I32      :{ ibinary("-");	   break; }
				case IGIR.MUL_I32      :{ 
					m_sp--; writeS0("Math.imul(" + rstack(0) + "," + rstack(1) + ")"); 
					//ibinary("*"));
						   break; }
				
				case IGIR.DIV_I32      :{ ibinary("/");	   break; }
				case IGIR.MOD_I32      :{ ibinary("%");	   break; }
				case IGIR.ADD_F64      :{ binary("+");	   break; }
				case IGIR.SUB_F64      :{ binary("-");	   break; }
				case IGIR.MUL_F64      :{ binary("*");	   break; }
				
				case IGIR.POW_I32      :{ 
					m_sp--; 
					writeS0("Math.pow(" + rstack(0) + "," + rstack(1) + ")|0"); 
				  	break; }

				case IGIR.POW_F64      :{ 
					m_sp--; 
					writeS0("Math.pow(" + rstack(0) + "," + rstack(1) + ")"); 
				  	break; }

				case IGIR.POW_I32_CONST      :{ 
					if (op.value == 2.0) {
						// THIS CANNOT USE CACHED VALUES (eg rstack)
						writeS0("("+stack(0) + "*" + stack(0) + ")|0"); 
					}
					else
					{
						writeS0("Math.pow(" + rstack(0) + "," + op.value + ")|0"); 
				  	}
				  	break; 
				 }
				  	
				case IGIR.POW_F64_CONST      :{ 
					if (op.value == 2.0) {
						// THIS CANNOT USE CACHED VALUES (eg rstack)
						writeS0(stack(0) + "*" + stack(0) + ""); 
					}
					else 
					{
						writeS0("Math.pow(" + rstack(0) + "," + rstack(1) + ")"); 
				  	}
				  	break; 
				 }
				
				case IGIR.DIV_F64         :{ binary("/");	   break; }
				case IGIR.MOD_F64         :{ binary("%");	   break; }
				case IGIR.ADD_F64_VEC     :{ write(binary_vec("+", op.count)); break; }
				case IGIR.SUB_F64_VEC     :{ write(binary_vec("-", op.count)); break; }
				case IGIR.MUL_F64_VEC     :{ write(binary_vec("*", op.count)); break; }
				case IGIR.DIV_F64_VEC     :{ write(binary_vec("/", op.count)); break; }
				case IGIR.MUL_F64_VEC_VAL :{ write(binary_vec_val("*", op.count)); break; }
				case IGIR.CROSS_F64_VEC   :{


					int tmp_head = 1;
					int a        = 1 - 6;
					int b        = 1 - 3;
					
					// calculate and write the results ahead in memory
					writes(lstack(tmp_head+0) + 
							S(a+1) + "*" + S(b+2) + "-" + S(a+2) + "*" + S(b+1));
					writes(lstack(tmp_head+1) + 
							S(a+2) + "*" + S(b+0) + "-" + S(a+0) + "*" + S(b+2));
					writes(lstack(tmp_head+2) + 
							S(a+0) + "*" + S(b+1) + "-" + S(a+1) + "*" + S(b+0));
					
					// move the values back
					writes(lstack(a+0) + S(tmp_head+0));
					writes(lstack(a+1) + S(tmp_head+1));
					writes(lstack(a+2) + S(tmp_head+2));
					// a b c 1 2 3
					m_sp -= 3;

					// this will require termporary space




					break;
				}
				case IGIR.DOT_F64_VEC: 
				{
					// TODO verif stack
					m_sp -= op.count * 2;
					StringBuilder sb = new StringBuilder();
					for (int k = 1; k <= op.count; k++) {
						if (k != 1) {
							sb.append("+");
						}
						sb.append(S(k) + "*" + S(k + op.count));
					}
					m_sp++;
					writes(lstack(0) + sb);
					break;
				}
				case IGIR.LENGTH_F64_VEC: 
				{
					// TODO verif stack
					m_sp -= op.count;
					StringBuilder sb = new StringBuilder();
					for (int k = 1; k <= op.count; k++) {
						if (k != 1) {
							sb.append("+");
						}
						sb.append(S(k) + "*" + S(k));
					}
					m_sp++;
					writes(lstack(0) + "Math.sqrt(" + sb + ")");
					break;
				}
				case IGIR.ABS_F64_VEC: {
					// todo veify stack
					for (int k = 0; k < op.count; k++) {
						writes(lstack(-k) + "Math.abs("+S(-k)+")"); 
					}
					break;
				}
				case IGIR.DUP_VEC: {  
					for (int k = 0; k < op.count; k++) {
						m_sp++;
						writes(lstack(0) + stack(-op.count)); 
					}
					break; 
				}
				
				case IGIR.LOCAL_STORE_VEC: {  
					m_sp -= op.count;
					for (int k = 0; k < op.count; k++) {
						writes(lvar(op.index + k) + stack(k + 1)); 
					}
					break; 
				}
				

				case IGIR.LOCAL_LOAD_VEC: {  
					
					for (int k = 0; k < op.count; k++) {
						m_sp++;
						writes(stack(0) + "=" + rvar(op.index + k)); 	
					}
					break; 
				}

				case IGIR.AND_I32  :{ ibinary("&");	   break; }
				case IGIR.OR_I32   :{ ibinary("|");	   break; }
				case IGIR.XOR_I32  :{ ibinary("^");	   break; }
				case IGIR.NOT_I32  :{ iunary("~");	   break; }
				case IGIR.NOT          :{ iunary("1^");    break; } 
				case IGIR.LSL_I32      :{ ibinary("<<");   break; }
				case IGIR.LSR_I32      :{ ibinary(">>>");  break; }
				case IGIR.ASR_I32      :{ ibinary(">>");   break; }
				case IGIR.CMP_EQ_I32   :{ compare("===");  break; }
				case IGIR.CMP_NE_I32   :{ compare("!==");  break; }
				case IGIR.CMP_LT_I32   :{ compare("<");   break; }
				case IGIR.CMP_LE_I32   :{ compare("<=");  break; }
				case IGIR.CMP_EQ_F64   :{ compare("===");  break; }
				case IGIR.CMP_LT_F64   :{ compare("<");   break; }
				case IGIR.CMP_LE_F64   :{ compare("<=");  break; }
				case IGIR.CMP_EQ_OBJ   :{ compare("==="); break; }
				// TODO this should really be more complicated
				// we should be checking the internal values as well
				case IGIR.CMP_EQ_FP    :{ compare("==="); break; }
				case IGIR.I32_TO_F64   :{ writeS0("1.0*" + rstack(0) ); break; }
				case IGIR.F64_TO_I32   :{ writeS0(rstack(0) + "|0"); break; }
				case IGIR.CATCH_ENTRY  :{
					m_sp++; 
					writes("pc=" + getIndexForLabel(op.label));
					write("continue; // terminate try catch");
					indent(-1);
					write("}}}");
					write("catch(_tmp) { " + lstack(0) + "_tmp; }");	
					break; }
				case IGIR.ARRAY_LENGTH :{ writeS0(rstack(0) + ".length"); break; }
				
				case IGIR.PUSH_F64 :{ m_sp++; writes(lstack(0) + op.value); break; }

				// these need to be differentiated
				//case IGIR.PUSH_I64 :{ m_sp++; writes(lstack(0) + op.value); break; }
				// these need to be differentiated
				case IGIR.PUSH_FP_NULL :{ m_sp++; writeS0("null"); break; }
				case IGIR.PUSH_OBJ_NULL :{ m_sp++; writeS0("null"); break; }
				case IGIR.PUSH_I32 :{ m_sp++; writeS0((int)op.value + "|0"); break; }
				case IGIR.PUSH_STRING      :{ m_sp++; 
					writeS0("IGVM.cachedNativeToString(\"" + escapeString(op.name) + "\")"); break; } // TODO
				
				case IGIR.LOCAL_LOAD       :{ push(op.index); break; }	
				case IGIR.LOCAL_STORE      :{ writes(lvar(op.index) + rstack(0)); m_sp--; break; }

				

				// this needs to be disambiguated
				case IGIR.LOCAL_CLEAR  :{ 
					int storage_class = op.type.getStorageClass();	
					if (storage_class == Type.STORAGE_8 ||
						storage_class == Type.STORAGE_16 ||
						storage_class == Type.STORAGE_32) {
						writes(lvar(op.index) + " 0");
					} 
					else if (storage_class == Type.STORAGE_F64 ||
							 storage_class == Type.STORAGE_F32) {
						writes(lvar(op.index) + " 0.0");
					}
					else {
						writes(lvar(op.index) + " null");
					}
					break; 
				}	

				case IGIR.IIF: {
					m_sp -= op.count * 2;
	
					write ("if (" + stack(0) + ") {");
					for (int k = 0; k < op.count; k++) {
						writes(lstack(k) + stack(1 + k)); 
					}
					write ("} else {");
					for (int k = 0; k < op.count; k++) {
						writes(lstack(k) + stack(1 + k + op.count)); 
					}
					write ("}");

					m_sp += op.count - 1;

					break;
				}
				case IGIR.IIF_PIECEWISE: {

					m_sp -= op.count * 3;
	
					for (int k = 0; k < op.count; k++) 
					{
						m_sp++;
						writes( lstack(0) +  "(" + stack(0) + ") ? " + stack(op.count) + ":"  + stack(op.count *2) );
						write("//iif"); 
					}
	

					break;
				}
//				case IGIR.CREATE_IT    :{ break; }
				case IGIR.MEMBER_STORE :{ m_sp -= 2; writes(rstack(1) + "." + getSafeMemberName(op.name) + "=" + rstack(2)); break; }
				case IGIR.LOCAL_MEMBER_LOAD  :{ m_sp++; writeS0(rvar(op.index) + "." + getSafeMemberName(op.name));  break; }
				
				case IGIR.MEMBER_LOAD  :{ writeS0( rstack(0) + "." + getSafeMemberName(op.name));  break; }
				case IGIR.STATIC_STORE :{ m_sp --; writes(lstatic(op.module, op.name) + rstack(1)); break; }
				case IGIR.STATIC_LOAD  :{ m_sp ++; writeS0(rstatic(op.module,op.name)); break; }
				case IGIR.THIS_STORE   :{ m_sp --; writes(var(0) + "." + op.name + "=" + rstack(1)); break; }
				case IGIR.THIS_LOAD    :{ m_sp ++; writeS0(var(0) + "." + op.name); break; }
				
				case IGIR.CALL_INTERFACE     :{  dumpParameters(op, op.op, 0); break; }
				case IGIR.CALL_VTABLE        :{  dumpParameters(op, op.op, 0); break;  }
				case IGIR.CALL_STATIC        :{  dumpParameters(op, op.op, 0); break;  }
				//case IGIR.CALL_STATIC_VTABLE :{  writes(dumpParameters(op, op.op, 0)); break;  }
				case IGIR.CALL_DYNAMIC       :{  dumpParameters(op, op.op, 0); break; }
				case IGIR.CALL_SUPER         :{  dumpParameters(op, op.op, 0); break; }
				
				case IGIR.CALL_RET1_INTERFACE     :{ dumpParameters(op, IGIR.CALL_INTERFACE, 1); break; }
				case IGIR.CALL_RET1_VTABLE        :{ dumpParameters(op, IGIR.CALL_VTABLE,    1); break; }
				case IGIR.CALL_RET1_STATIC        :{ dumpParameters(op, IGIR.CALL_STATIC,   1); break; }
				//case IGIR.CALL_RET1_STATIC_VTABLE :{ writes(dumpParameters(op, IGIR.CALL_STATIC_VTABLE, 1)); break; }
				case IGIR.CALL_RET1_DYNAMIC       :{ dumpParameters(op, IGIR.CALL_DYNAMIC, 1); break; }
				case IGIR.CALL_RET1_SUPER         :{ dumpParameters(op, IGIR.CALL_SUPER,   1); break; }

				
				case IGIR.FUNCPTR_MEMBER :{ createMemberPointer(op); break; }
				case IGIR.FUNCPTR_STATIC :{ createStaticPointer(op); break; }
				
				// start of a try block.  The label on this denotes where the catch block begins
				// TODO
				case IGIR.TRY_PUSH       :{ 
					Label l =generateLabel(op.target_label, m_sp);
					write("pc = 0; try { "); // reset the pc for the try catch entry);
					write("while(pc !== " + l.m_id +") { switch(pc) { case 0: //" + op.target_label);	

					indent(1);
					break; }


				case IGIR.INSTANCE_OF    :{ 
					writeS0("+(" + getSafeModulePath(op.module) + ".$instanceof(" + rstack(0) + "))");     
					break; }
				
				case IGIR.CAST_TO      :{ 
					// this value is a pass through
					write("if(!" + getSafeModulePath(op.module) + ".$instanceof(" + stack(0) + ")) {");
					write("throw 'Cast Failed';");		// should generte a built in exception?
					// also our standard catch
					write("}");	// this needs to fail if the underlying object is of the wrong type
					break;
				 }

				case IGIR._NEW          :{ 
					m_sp++; 
					writeS0("Object.create(IGVM.OBJECT."+getSafeModuleName(op.module) + ")");
					break;
				}

				case IGIR._THROW         :{ m_sp--; writes("throw " + rstack(1)); break; }
				case IGIR.JMP            :{  write(GOTO(op.target_label));	break; }
				case IGIR.J0_I32         :{ m_sp--; write("if(0===" + rstack(1) + "){" + GOTO(op.target_label) + "}"); break; }
				case IGIR.JN0_I32        :{ m_sp--; write("if(0!==" + rstack(1) + "){" + GOTO(op.target_label) + "}"); break;}

				case IGIR.AND: {
					//stack(0) should be preserved (thus no rstack)
					write("if(0===" + stack(0) + "){" + GOTO(op.target_label) + "}");
					m_sp--;
					break; }
				

				case IGIR.OR: {
					//stack(0) should be preserved (thus no rstack) 	
					write("if(0!==" + stack(0) + "){" + GOTO(op.target_label) + "}");
					m_sp--;
					break; }

				case IGIR.ARRAY_LOAD     :{ 
					m_sp --;  
					writeS0(rstack(0)+"["+rstack(1)+"]");  
					break; }
				
				case IGIR.ARRAY_STORE    :{ 
					m_sp -= 3;
					writes(rstack(1) + "["+rstack(2)+"] = " + rstack(3)); 
					break; }
				
				// array_new ACTUAL-CLASS STORAGE-CLASS # length => new-array
				case IGIR.ARRAY_NEW      :{ 
					writeS0(createArray(op.type.getStorageClass()));	
					break; 
				}
				case IGIR.ARRAY_RESIZE   :{ 
					m_sp --;
					write(resizeArray(op.type.getStorageClass()));
					break; 
				}

				//  array_fill storge-class # array, item => nil # check that fill exists
				case IGIR.ARRAY_FILL     :{ 
					m_sp -= 2;	writes(rstack(1) + ".fill("+rstack(2)+")"); break; }
				case IGIR.ARRAY_COPY     :{ 
					m_sp--;
					writes(copyArray(rstack(0), rstack(1), op.type.getStorageClass()));
					break; }	
					
				case IGIR.LOCAL_INC_I32   :{ 
					if (op.value == 1.0) {
						writes(var(op.index) + "++");
					}
					else {
						writes(var(op.index) + "+=" + (int)op.value); 
					}
					break; 
				}
				case IGIR.STACK_INC_I32   :{ writeS0("("+rstack(0) + "+" + (int)op.value+")|0"); break; }
				case IGIR.STACK_INC_F64   :{ writeS0("("+rstack(0) + "+" +      op.value+")");   break; }

				case IGIR.MLA_I32           :{ m_sp -=2; writeS0("(" + rstack(0)+"+Math.imul("+rstack(1)+"*"+rstack(2) + "))|0"); break; }
				case IGIR.MLA_F64           :{ m_sp -=2; writeS0(rstack(0)+"+"+rstack(1)+"*"+rstack(2)); break; }
				
				case IGIR.END :{ break; }
				case -1 : {
					break;
				}
				default: {
					write("// unknown op " + op.op);
					throw new RuntimeException("Unhandled: " + op);
				}
			}
		}
		}
		catch (RuntimeException ex) 
		{
			for (int i = 0; i < op_count; i++)
			{
				try
				{
					final IGIROp op = ops.get(i);
					System.err.println("" + op);
				}
				catch (Exception ex2) {
					System.err.println("Failed to render op");
				}
			}

			throw ex;
		}

		if (has_labels) {
			write("}}");
		}

		indent(-1);

		// teminte the switch and the function
		

		writeFlush();

		StringBuilder body = m_out;
		m_out = new StringBuilder();

		StringBuilder fn_decl = new StringBuilder();
		fn_decl.append("function (");
		for (int i = 0; i < argc; i++) {
			if (i != 0) {
				fn_decl.append(",");
			}
			fn_decl.append(pvar(i));
		}
		fn_decl.append("){");

		write(fn_decl.toString());


		indent(1);
		for(int i = 0; i < m_active_variables_list.size(); i++) {
			Variable v = m_active_variables_list.get(i);
			if (v.m_is_param) {
				continue;
			}

			writes ("var " + v.m_name);
		}
		indent(-1);
		m_out.append(body);

		write("}");

		// add to the primary StringBuilder for the whole project
		primary.append(m_out);
		m_out = primary;
	}
}