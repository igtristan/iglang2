/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package util;

import java.util.Vector;
import java.io.PrintStream;

class LogError
{
	String m_source;
	String m_message;
	int    m_line;
	int    m_column;
	String    m_line_text  = "";
	String    m_under_text = "";
	
	public LogError(String source, String message, int line, int column, String line_text, String under_text)
	{
		m_source  = source;
		m_message = message.replace("\"", "&quot;");
		m_line    = line;
		m_column = column;
		m_line_text = line_text.replace("\"", "&quot;");
		m_under_text = under_text.replace("\"", "&quot;");
	}
	
	public void output(PrintStream out) {
		out.println("{");
		out.println("\"source\": \"" + m_source + "\",");
		out.println("\"message\": \"" + m_message + "\",");
		out.println("\"line\": \"" + m_line + "\",");
		out.println("\"column\": \"" + m_column + "\",");	
		out.println("\"line_text\": \"" + m_line_text + "\",");
		out.println("\"under_text\": \"" + m_under_text + "\"");			
		out.println("}");
	}
}

public class Log implements Runnable
{
	public static String s_status = "OK";
	public static String s_sentinel = null;
	public static boolean s_enabled = false;
	public static boolean s_print_timers = false;
	
	public static void log(String s) {
		System.out.println("log: " + s);
	}
	
	public static void setPrintTimers(boolean v) {
		s_print_timers = v;
	}
	
	private static Vector<LogError> s_errors = new Vector<LogError>();
	
	public static void logError(String source, String message, int line, int column, String line_text, String under_text)
	{
		s_errors.add(new LogError(source, message, line, column, line_text, under_text));
	}
	
	public static void logError(String source, String message, int line, int column)
	{
		s_errors.add(new LogError(source, message, line, column, "", ""));
	}
	
	public static void logErrorEcho(String source, String message)
	{
		s_errors.add(new LogError(source, message, 0, 0, "", ""));
		
		System.err.println(source + "> " + message);
	}
	
	private static StringBuilder s_timer_buffer = new StringBuilder();
	private static long s_timer_start = 0;
	private static long s_timer_total = 0;
	
	public static void startTimer() {
		s_timer_start = System.currentTimeMillis();
	}
	
	public static void stopTimer(String tag)
	{
		long diff = System.currentTimeMillis() - s_timer_start;
		s_timer_buffer.append("\ntimer: " + tag + " " + diff);
		s_timer_total += diff;
	}
	
	public static void printTimers() {
		if (s_print_timers) {
			System.out.println(s_timer_buffer.toString());
			System.out.println("total: " + s_timer_total);
		}
	}
	
	public static void setSentinel(String s) {
		s_sentinel = s;
	}
	
	public static void setEnabled(boolean b) {
		s_enabled = b;
	}
	
	public void run()
	{
		if (!s_enabled ){
			return;
		}
		
		try
		{
			PrintStream out = System.out;
			if (s_sentinel != null) {
				out.println(s_sentinel);
			}
			out.println("{");
			
			if (s_errors.size() > 0) {
				out.println("\"status\": \"Error\",");
			}
			else {
				out.println("\"status\": \"" + s_status + "\",");
			}
			out.println("\"errors\": [");
			
			boolean first = true;
			for (LogError err : s_errors) {
				if (!first) {
					out.println(",");
				}
				first = false;
				err.output(out);
			}
			
			out.println("]");
			out.println("}");
			
		}
		catch (Exception ex) {
			System.err.println("Log Print Failed");
		}
	}
	
	public static void addShutdownHook()
	{
		Thread hook = new Thread(new Log());
		Runtime.getRuntime().addShutdownHook(hook);
	}
}