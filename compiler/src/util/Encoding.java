package util;

public class Encoding
{
	public static double parseDouble(String token_value)
	{
		boolean neg = token_value.startsWith("-");
		if (neg) {
			token_value = token_value.substring(1);
		}
		
		double value = 0;
		if (token_value.startsWith("0d"))
		{
			long val = Long.parseLong(token_value.substring(2), 16);
			value = Double.longBitsToDouble(val);
		}
		else
		{
			value = Double.parseDouble(token_value);
		}
		return neg ? -value : value;
	}
}