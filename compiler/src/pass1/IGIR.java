package pass1;
import java.util.*;


public class IGIR {
	public static final int NOP = 0;
	public static final int POP = 1;
	public static final int DUP = 2;
	public static final int DUP2 = 3;
	public static final int DUP1X = 4;
	public static final int DUP2X = 5;
	public static final int RET0 = 6;
	public static final int RET1 = 7;
	public static final int RET1_I32 = 8;
	public static final int ADD_F64_VEC = 9;
	public static final int SUB_F64_VEC = 10;
	public static final int MUL_F64_VEC = 11;
	public static final int DIV_F64_VEC = 12;
	public static final int MUL_F64_VEC_VAL = 13;
	public static final int CROSS_F64_VEC = 14;
	public static final int DOT_F64_VEC = 15;
	public static final int LENGTH_F64_VEC = 16;
	public static final int ABS_F64_VEC = 17;
	public static final int IIF = 18;
	public static final int IIF_PIECEWISE = 19;
	public static final int ADD_I32 = 20;
	public static final int SUB_I32 = 21;
	public static final int MUL_I32 = 22;
	public static final int DIV_I32 = 23;
	public static final int POW_I32 = 24;
	public static final int POW_I32_CONST = 25;
	public static final int MOD_I32 = 26;
	public static final int MLA_I32 = 27;
	public static final int AND_I32 = 28;
	public static final int OR_I32 = 29;
	public static final int XOR_I32 = 30;
	public static final int ASR_I32 = 31;
	public static final int LSR_I32 = 32;
	public static final int LSL_I32 = 33;
	public static final int NOT_I32 = 34;
	public static final int ADD_F64 = 35;
	public static final int SUB_F64 = 36;
	public static final int MUL_F64 = 37;
	public static final int POW_F64 = 38;
	public static final int POW_F64_CONST = 39;
	public static final int DIV_F64 = 40;
	public static final int MOD_F64 = 41;
	public static final int MLA_F64 = 42;
	public static final int CMP_EQ_I32 = 43;
	public static final int CMP_NE_I32 = 44;
	public static final int CMP_LT_I32 = 45;
	public static final int CMP_LE_I32 = 46;
	public static final int CMP_EQ_F64 = 47;
	public static final int CMP_LT_F64 = 48;
	public static final int CMP_LE_F64 = 49;
	public static final int CMP_EQ_I64 = 50;
	public static final int CMP_EQ_FP = 51;
	public static final int CMP_EQ_OBJ = 52;
	public static final int I32_TO_F64 = 53;
	public static final int F64_TO_I32 = 54;
	public static final int PUSH_F64 = 55;
	public static final int PUSH_I32 = 56;
	public static final int PUSH_STRING = 57;
	public static final int PUSH_OBJ_NULL = 58;
	public static final int PUSH_FP_NULL = 59;
	public static final int LOCAL_LOAD = 60;
	public static final int LOCAL_STORE = 61;
	public static final int LOCAL_CLEAR = 62;
	public static final int LOCAL_LOAD_VEC = 63;
	public static final int LOCAL_STORE_VEC = 64;
	public static final int DUP_VEC = 65;
	public static final int THIS_LOAD = 66;
	public static final int THIS_STORE = 67;
	public static final int LOCAL_MEMBER_LOAD = 68;
	public static final int MEMBER_LOAD = 69;
	public static final int MEMBER_STORE = 70;
	public static final int STATIC_LOAD = 71;
	public static final int STATIC_STORE = 72;
	public static final int FUNCPTR_MEMBER = 73;
	public static final int FUNCPTR_STATIC = 74;
	public static final int _NEW = 75;
	public static final int TRY_PUSH = 76;
	public static final int CATCH_ENTRY = 77;
	public static final int _THROW = 78;
	public static final int JMP = 79;
	public static final int J0_I32 = 80;
	public static final int JN0_I32 = 81;
	public static final int NOT = 82;
	public static final int AND = 83;
	public static final int OR = 84;
	public static final int CAST_TO = 85;
	public static final int INSTANCE_OF = 86;
	public static final int ARRAY_LOAD = 87;
	public static final int ARRAY_STORE = 88;
	public static final int ARRAY_NEW = 89;
	public static final int ARRAY_LENGTH = 90;
	public static final int ARRAY_RESIZE = 91;
	public static final int ARRAY_FILL = 92;
	public static final int ARRAY_COPY = 93;
	public static final int LOCAL_INC_I32 = 94;
	public static final int STACK_INC_I32 = 95;
	public static final int STACK_INC_F64 = 96;
	public static final int CALL_VTABLE = 97;
	public static final int CALL_SUPER = 98;
	public static final int CALL_STATIC = 99;
	public static final int CALL_INTERFACE = 100;
	public static final int CALL_DYNAMIC = 101;
	public static final int CALL_RET1_VTABLE = 102;
	public static final int CALL_RET1_SUPER = 103;
	public static final int CALL_RET1_STATIC = 104;
	public static final int CALL_RET1_INTERFACE = 105;
	public static final int CALL_RET1_DYNAMIC = 106;
	public static final int END = 107;
	public static final Map<Integer,String> OP_TO_NAME = new HashMap<Integer,String>();
	static {
		OP_TO_NAME.put(NOP,"NOP");
		OP_TO_NAME.put(POP,"POP");
		OP_TO_NAME.put(DUP,"DUP");
		OP_TO_NAME.put(DUP2,"DUP2");
		OP_TO_NAME.put(DUP1X,"DUP1X");
		OP_TO_NAME.put(DUP2X,"DUP2X");
		OP_TO_NAME.put(RET0,"RET0");
		OP_TO_NAME.put(RET1,"RET1");
		OP_TO_NAME.put(RET1_I32,"RET1_I32");
		OP_TO_NAME.put(ADD_F64_VEC,"ADD_F64_VEC");
		OP_TO_NAME.put(SUB_F64_VEC,"SUB_F64_VEC");
		OP_TO_NAME.put(MUL_F64_VEC,"MUL_F64_VEC");
		OP_TO_NAME.put(DIV_F64_VEC,"DIV_F64_VEC");
		OP_TO_NAME.put(MUL_F64_VEC_VAL,"MUL_F64_VEC_VAL");
		OP_TO_NAME.put(CROSS_F64_VEC,"CROSS_F64_VEC");
		OP_TO_NAME.put(DOT_F64_VEC,"DOT_F64_VEC");
		OP_TO_NAME.put(LENGTH_F64_VEC,"LENGTH_F64_VEC");
		OP_TO_NAME.put(ABS_F64_VEC,"ABS_F64_VEC");
		OP_TO_NAME.put(IIF,"IIF");
		OP_TO_NAME.put(IIF_PIECEWISE,"IIF_PIECEWISE");
		OP_TO_NAME.put(ADD_I32,"ADD_I32");
		OP_TO_NAME.put(SUB_I32,"SUB_I32");
		OP_TO_NAME.put(MUL_I32,"MUL_I32");
		OP_TO_NAME.put(DIV_I32,"DIV_I32");
		OP_TO_NAME.put(POW_I32,"POW_I32");
		OP_TO_NAME.put(POW_I32_CONST,"POW_I32_CONST");
		OP_TO_NAME.put(MOD_I32,"MOD_I32");
		OP_TO_NAME.put(MLA_I32,"MLA_I32");
		OP_TO_NAME.put(AND_I32,"AND_I32");
		OP_TO_NAME.put(OR_I32,"OR_I32");
		OP_TO_NAME.put(XOR_I32,"XOR_I32");
		OP_TO_NAME.put(ASR_I32,"ASR_I32");
		OP_TO_NAME.put(LSR_I32,"LSR_I32");
		OP_TO_NAME.put(LSL_I32,"LSL_I32");
		OP_TO_NAME.put(NOT_I32,"NOT_I32");
		OP_TO_NAME.put(ADD_F64,"ADD_F64");
		OP_TO_NAME.put(SUB_F64,"SUB_F64");
		OP_TO_NAME.put(MUL_F64,"MUL_F64");
		OP_TO_NAME.put(POW_F64,"POW_F64");
		OP_TO_NAME.put(POW_F64_CONST,"POW_F64_CONST");
		OP_TO_NAME.put(DIV_F64,"DIV_F64");
		OP_TO_NAME.put(MOD_F64,"MOD_F64");
		OP_TO_NAME.put(MLA_F64,"MLA_F64");
		OP_TO_NAME.put(CMP_EQ_I32,"CMP_EQ_I32");
		OP_TO_NAME.put(CMP_NE_I32,"CMP_NE_I32");
		OP_TO_NAME.put(CMP_LT_I32,"CMP_LT_I32");
		OP_TO_NAME.put(CMP_LE_I32,"CMP_LE_I32");
		OP_TO_NAME.put(CMP_EQ_F64,"CMP_EQ_F64");
		OP_TO_NAME.put(CMP_LT_F64,"CMP_LT_F64");
		OP_TO_NAME.put(CMP_LE_F64,"CMP_LE_F64");
		OP_TO_NAME.put(CMP_EQ_I64,"CMP_EQ_I64");
		OP_TO_NAME.put(CMP_EQ_FP,"CMP_EQ_FP");
		OP_TO_NAME.put(CMP_EQ_OBJ,"CMP_EQ_OBJ");
		OP_TO_NAME.put(I32_TO_F64,"I32_TO_F64");
		OP_TO_NAME.put(F64_TO_I32,"F64_TO_I32");
		OP_TO_NAME.put(PUSH_F64,"PUSH_F64");
		OP_TO_NAME.put(PUSH_I32,"PUSH_I32");
		OP_TO_NAME.put(PUSH_STRING,"PUSH_STRING");
		OP_TO_NAME.put(PUSH_OBJ_NULL,"PUSH_OBJ_NULL");
		OP_TO_NAME.put(PUSH_FP_NULL,"PUSH_FP_NULL");
		OP_TO_NAME.put(LOCAL_LOAD,"LOCAL_LOAD");
		OP_TO_NAME.put(LOCAL_STORE,"LOCAL_STORE");
		OP_TO_NAME.put(LOCAL_CLEAR,"LOCAL_CLEAR");
		OP_TO_NAME.put(LOCAL_LOAD_VEC,"LOCAL_LOAD_VEC");
		OP_TO_NAME.put(LOCAL_STORE_VEC,"LOCAL_STORE_VEC");
		OP_TO_NAME.put(DUP_VEC,"DUP_VEC");
		OP_TO_NAME.put(THIS_LOAD,"THIS_LOAD");
		OP_TO_NAME.put(THIS_STORE,"THIS_STORE");
		OP_TO_NAME.put(LOCAL_MEMBER_LOAD,"LOCAL_MEMBER_LOAD");
		OP_TO_NAME.put(MEMBER_LOAD,"MEMBER_LOAD");
		OP_TO_NAME.put(MEMBER_STORE,"MEMBER_STORE");
		OP_TO_NAME.put(STATIC_LOAD,"STATIC_LOAD");
		OP_TO_NAME.put(STATIC_STORE,"STATIC_STORE");
		OP_TO_NAME.put(FUNCPTR_MEMBER,"FUNCPTR_MEMBER");
		OP_TO_NAME.put(FUNCPTR_STATIC,"FUNCPTR_STATIC");
		OP_TO_NAME.put(_NEW,"_NEW");
		OP_TO_NAME.put(TRY_PUSH,"TRY_PUSH");
		OP_TO_NAME.put(CATCH_ENTRY,"CATCH_ENTRY");
		OP_TO_NAME.put(_THROW,"_THROW");
		OP_TO_NAME.put(JMP,"JMP");
		OP_TO_NAME.put(J0_I32,"J0_I32");
		OP_TO_NAME.put(JN0_I32,"JN0_I32");
		OP_TO_NAME.put(NOT,"NOT");
		OP_TO_NAME.put(AND,"AND");
		OP_TO_NAME.put(OR,"OR");
		OP_TO_NAME.put(CAST_TO,"CAST_TO");
		OP_TO_NAME.put(INSTANCE_OF,"INSTANCE_OF");
		OP_TO_NAME.put(ARRAY_LOAD,"ARRAY_LOAD");
		OP_TO_NAME.put(ARRAY_STORE,"ARRAY_STORE");
		OP_TO_NAME.put(ARRAY_NEW,"ARRAY_NEW");
		OP_TO_NAME.put(ARRAY_LENGTH,"ARRAY_LENGTH");
		OP_TO_NAME.put(ARRAY_RESIZE,"ARRAY_RESIZE");
		OP_TO_NAME.put(ARRAY_FILL,"ARRAY_FILL");
		OP_TO_NAME.put(ARRAY_COPY,"ARRAY_COPY");
		OP_TO_NAME.put(LOCAL_INC_I32,"LOCAL_INC_I32");
		OP_TO_NAME.put(STACK_INC_I32,"STACK_INC_I32");
		OP_TO_NAME.put(STACK_INC_F64,"STACK_INC_F64");
		OP_TO_NAME.put(CALL_VTABLE,"CALL_VTABLE");
		OP_TO_NAME.put(CALL_SUPER,"CALL_SUPER");
		OP_TO_NAME.put(CALL_STATIC,"CALL_STATIC");
		OP_TO_NAME.put(CALL_INTERFACE,"CALL_INTERFACE");
		OP_TO_NAME.put(CALL_DYNAMIC,"CALL_DYNAMIC");
		OP_TO_NAME.put(CALL_RET1_VTABLE,"CALL_RET1_VTABLE");
		OP_TO_NAME.put(CALL_RET1_SUPER,"CALL_RET1_SUPER");
		OP_TO_NAME.put(CALL_RET1_STATIC,"CALL_RET1_STATIC");
		OP_TO_NAME.put(CALL_RET1_INTERFACE,"CALL_RET1_INTERFACE");
		OP_TO_NAME.put(CALL_RET1_DYNAMIC,"CALL_RET1_DYNAMIC");
		OP_TO_NAME.put(END,"END");
	}
	public static IGIROp nop(
) { 
		IGIROp tmp = new IGIROp(NOP);    
		return tmp; 
	}
	public static IGIROp pop(
int count
) { 
		IGIROp tmp = new IGIROp(POP);    
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp dup(
int count
) { 
		IGIROp tmp = new IGIROp(DUP);    
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp dup2(
) { 
		IGIROp tmp = new IGIROp(DUP2);    
		return tmp; 
	}
	public static IGIROp dup1x(
) { 
		IGIROp tmp = new IGIROp(DUP1X);    
		return tmp; 
	}
	public static IGIROp dup2x(
) { 
		IGIROp tmp = new IGIROp(DUP2X);    
		return tmp; 
	}
	public static IGIROp ret0(
) { 
		IGIROp tmp = new IGIROp(RET0);    
		return tmp; 
	}
	public static IGIROp ret1(
Type type
) { 
		IGIROp tmp = new IGIROp(RET1);    
		tmp.type = type;
		return tmp; 
	}
	public static IGIROp ret1_i32(
Type type
,
int value
) { 
		IGIROp tmp = new IGIROp(RET1_I32);    
		tmp.type = type;
		tmp.value = value;
		return tmp; 
	}
	public static IGIROp add_f64_vec(
int count
) { 
		IGIROp tmp = new IGIROp(ADD_F64_VEC);    
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp sub_f64_vec(
int count
) { 
		IGIROp tmp = new IGIROp(SUB_F64_VEC);    
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp mul_f64_vec(
int count
) { 
		IGIROp tmp = new IGIROp(MUL_F64_VEC);    
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp div_f64_vec(
int count
) { 
		IGIROp tmp = new IGIROp(DIV_F64_VEC);    
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp mul_f64_vec_val(
int count
) { 
		IGIROp tmp = new IGIROp(MUL_F64_VEC_VAL);    
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp cross_f64_vec(
) { 
		IGIROp tmp = new IGIROp(CROSS_F64_VEC);    
		return tmp; 
	}
	public static IGIROp dot_f64_vec(
int count
) { 
		IGIROp tmp = new IGIROp(DOT_F64_VEC);    
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp length_f64_vec(
int count
) { 
		IGIROp tmp = new IGIROp(LENGTH_F64_VEC);    
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp abs_f64_vec(
int count
) { 
		IGIROp tmp = new IGIROp(ABS_F64_VEC);    
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp iif(
Type type
,
int count
) { 
		IGIROp tmp = new IGIROp(IIF);    
		tmp.type = type;
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp iif_piecewise(
Type type
,
int count
) { 
		IGIROp tmp = new IGIROp(IIF_PIECEWISE);    
		tmp.type = type;
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp add_i32(
) { 
		IGIROp tmp = new IGIROp(ADD_I32);    
		return tmp; 
	}
	public static IGIROp sub_i32(
) { 
		IGIROp tmp = new IGIROp(SUB_I32);    
		return tmp; 
	}
	public static IGIROp mul_i32(
) { 
		IGIROp tmp = new IGIROp(MUL_I32);    
		return tmp; 
	}
	public static IGIROp div_i32(
) { 
		IGIROp tmp = new IGIROp(DIV_I32);    
		return tmp; 
	}
	public static IGIROp pow_i32(
) { 
		IGIROp tmp = new IGIROp(POW_I32);    
		return tmp; 
	}
	public static IGIROp pow_i32_const(
double value
) { 
		IGIROp tmp = new IGIROp(POW_I32_CONST);    
		tmp.value = value;
		return tmp; 
	}
	public static IGIROp mod_i32(
) { 
		IGIROp tmp = new IGIROp(MOD_I32);    
		return tmp; 
	}
	public static IGIROp mla_i32(
) { 
		IGIROp tmp = new IGIROp(MLA_I32);    
		return tmp; 
	}
	public static IGIROp and_i32(
) { 
		IGIROp tmp = new IGIROp(AND_I32);    
		return tmp; 
	}
	public static IGIROp or_i32(
) { 
		IGIROp tmp = new IGIROp(OR_I32);    
		return tmp; 
	}
	public static IGIROp xor_i32(
) { 
		IGIROp tmp = new IGIROp(XOR_I32);    
		return tmp; 
	}
	public static IGIROp asr_i32(
) { 
		IGIROp tmp = new IGIROp(ASR_I32);    
		return tmp; 
	}
	public static IGIROp lsr_i32(
) { 
		IGIROp tmp = new IGIROp(LSR_I32);    
		return tmp; 
	}
	public static IGIROp lsl_i32(
) { 
		IGIROp tmp = new IGIROp(LSL_I32);    
		return tmp; 
	}
	public static IGIROp not_i32(
) { 
		IGIROp tmp = new IGIROp(NOT_I32);    
		return tmp; 
	}
	public static IGIROp add_f64(
) { 
		IGIROp tmp = new IGIROp(ADD_F64);    
		return tmp; 
	}
	public static IGIROp sub_f64(
) { 
		IGIROp tmp = new IGIROp(SUB_F64);    
		return tmp; 
	}
	public static IGIROp mul_f64(
) { 
		IGIROp tmp = new IGIROp(MUL_F64);    
		return tmp; 
	}
	public static IGIROp pow_f64(
) { 
		IGIROp tmp = new IGIROp(POW_F64);    
		return tmp; 
	}
	public static IGIROp pow_f64_const(
double value
) { 
		IGIROp tmp = new IGIROp(POW_F64_CONST);    
		tmp.value = value;
		return tmp; 
	}
	public static IGIROp div_f64(
) { 
		IGIROp tmp = new IGIROp(DIV_F64);    
		return tmp; 
	}
	public static IGIROp mod_f64(
) { 
		IGIROp tmp = new IGIROp(MOD_F64);    
		return tmp; 
	}
	public static IGIROp mla_f64(
) { 
		IGIROp tmp = new IGIROp(MLA_F64);    
		return tmp; 
	}
	public static IGIROp cmp_eq_i32(
) { 
		IGIROp tmp = new IGIROp(CMP_EQ_I32);    
		return tmp; 
	}
	public static IGIROp cmp_ne_i32(
) { 
		IGIROp tmp = new IGIROp(CMP_NE_I32);    
		return tmp; 
	}
	public static IGIROp cmp_lt_i32(
) { 
		IGIROp tmp = new IGIROp(CMP_LT_I32);    
		return tmp; 
	}
	public static IGIROp cmp_le_i32(
) { 
		IGIROp tmp = new IGIROp(CMP_LE_I32);    
		return tmp; 
	}
	public static IGIROp cmp_eq_f64(
) { 
		IGIROp tmp = new IGIROp(CMP_EQ_F64);    
		return tmp; 
	}
	public static IGIROp cmp_lt_f64(
) { 
		IGIROp tmp = new IGIROp(CMP_LT_F64);    
		return tmp; 
	}
	public static IGIROp cmp_le_f64(
) { 
		IGIROp tmp = new IGIROp(CMP_LE_F64);    
		return tmp; 
	}
	public static IGIROp cmp_eq_i64(
) { 
		IGIROp tmp = new IGIROp(CMP_EQ_I64);    
		return tmp; 
	}
	public static IGIROp cmp_eq_fp(
) { 
		IGIROp tmp = new IGIROp(CMP_EQ_FP);    
		return tmp; 
	}
	public static IGIROp cmp_eq_obj(
) { 
		IGIROp tmp = new IGIROp(CMP_EQ_OBJ);    
		return tmp; 
	}
	public static IGIROp i32_to_f64(
) { 
		IGIROp tmp = new IGIROp(I32_TO_F64);    
		return tmp; 
	}
	public static IGIROp f64_to_i32(
) { 
		IGIROp tmp = new IGIROp(F64_TO_I32);    
		return tmp; 
	}
	public static IGIROp push_f64(
double value
) { 
		IGIROp tmp = new IGIROp(PUSH_F64);    
		tmp.value = value;
		return tmp; 
	}
	public static IGIROp push_i32(
int value
) { 
		IGIROp tmp = new IGIROp(PUSH_I32);    
		tmp.value = value;
		return tmp; 
	}
	public static IGIROp push_string(
String name
) { 
		IGIROp tmp = new IGIROp(PUSH_STRING);    
		tmp.name = name;
		return tmp; 
	}
	public static IGIROp push_obj_null(
) { 
		IGIROp tmp = new IGIROp(PUSH_OBJ_NULL);    
		return tmp; 
	}
	public static IGIROp push_fp_null(
) { 
		IGIROp tmp = new IGIROp(PUSH_FP_NULL);    
		return tmp; 
	}
	public static IGIROp local_load(
Type type
,
int index
) { 
		IGIROp tmp = new IGIROp(LOCAL_LOAD);    
		tmp.type = type;
		tmp.index = index;
		return tmp; 
	}
	public static IGIROp local_store(
Type type
,
int index
) { 
		IGIROp tmp = new IGIROp(LOCAL_STORE);    
		tmp.type = type;
		tmp.index = index;
		return tmp; 
	}
	public static IGIROp local_clear(
int index
,
Type type
) { 
		IGIROp tmp = new IGIROp(LOCAL_CLEAR);    
		tmp.index = index;
		tmp.type = type;
		return tmp; 
	}
	public static IGIROp local_load_vec(
Type type
,
int index
,
int count
) { 
		IGIROp tmp = new IGIROp(LOCAL_LOAD_VEC);    
		tmp.type = type;
		tmp.index = index;
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp local_store_vec(
Type type
,
int index
,
int count
) { 
		IGIROp tmp = new IGIROp(LOCAL_STORE_VEC);    
		tmp.type = type;
		tmp.index = index;
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp dup_vec(
int count
) { 
		IGIROp tmp = new IGIROp(DUP_VEC);    
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp this_load(
Type module
,
String name
) { 
		IGIROp tmp = new IGIROp(THIS_LOAD);    
		tmp.module = module;
		tmp.name = name;
		return tmp; 
	}
	public static IGIROp this_store(
Type module
,
String name
) { 
		IGIROp tmp = new IGIROp(THIS_STORE);    
		tmp.module = module;
		tmp.name = name;
		return tmp; 
	}
	public static IGIROp local_member_load(
Type module
,
String name
,
int index
) { 
		IGIROp tmp = new IGIROp(LOCAL_MEMBER_LOAD);    
		tmp.module = module;
		tmp.name = name;
		tmp.index = index;
		return tmp; 
	}
	public static IGIROp member_load(
Type module
,
String name
) { 
		IGIROp tmp = new IGIROp(MEMBER_LOAD);    
		tmp.module = module;
		tmp.name = name;
		return tmp; 
	}
	public static IGIROp member_store(
Type module
,
String name
) { 
		IGIROp tmp = new IGIROp(MEMBER_STORE);    
		tmp.module = module;
		tmp.name = name;
		return tmp; 
	}
	public static IGIROp static_load(
Type module
,
String name
) { 
		IGIROp tmp = new IGIROp(STATIC_LOAD);    
		tmp.module = module;
		tmp.name = name;
		return tmp; 
	}
	public static IGIROp static_store(
Type module
,
String name
) { 
		IGIROp tmp = new IGIROp(STATIC_STORE);    
		tmp.module = module;
		tmp.name = name;
		return tmp; 
	}
	public static IGIROp funcptr_member(
Type module
,
String name
) { 
		IGIROp tmp = new IGIROp(FUNCPTR_MEMBER);    
		tmp.module = module;
		tmp.name = name;
		return tmp; 
	}
	public static IGIROp funcptr_static(
Type module
,
String name
) { 
		IGIROp tmp = new IGIROp(FUNCPTR_STATIC);    
		tmp.module = module;
		tmp.name = name;
		return tmp; 
	}
	public static IGIROp _new(
Type module
) { 
		IGIROp tmp = new IGIROp(_NEW);    
		tmp.module = module;
		return tmp; 
	}
	public static IGIROp try_push(
String label
) { 
		IGIROp tmp = new IGIROp(TRY_PUSH);    
		tmp.label = label;
		return tmp; 
	}
	public static IGIROp catch_entry(
String label
) { 
		IGIROp tmp = new IGIROp(CATCH_ENTRY);    
		tmp.label = label;
		return tmp; 
	}
	public static IGIROp _throw(
Type module
) { 
		IGIROp tmp = new IGIROp(_THROW);    
		tmp.module = module;
		return tmp; 
	}
	public static IGIROp jmp(
String target_label
) { 
		IGIROp tmp = new IGIROp(JMP);    
		tmp.target_label = target_label;
		return tmp; 
	}
	public static IGIROp j0_i32(
String target_label
) { 
		IGIROp tmp = new IGIROp(J0_I32);    
		tmp.target_label = target_label;
		return tmp; 
	}
	public static IGIROp jn0_i32(
String target_label
) { 
		IGIROp tmp = new IGIROp(JN0_I32);    
		tmp.target_label = target_label;
		return tmp; 
	}
	public static IGIROp not(
) { 
		IGIROp tmp = new IGIROp(NOT);    
		return tmp; 
	}
	public static IGIROp and(
String target_label
) { 
		IGIROp tmp = new IGIROp(AND);    
		tmp.target_label = target_label;
		return tmp; 
	}
	public static IGIROp or(
String target_label
) { 
		IGIROp tmp = new IGIROp(OR);    
		tmp.target_label = target_label;
		return tmp; 
	}
	public static IGIROp cast_to(
Type module
) { 
		IGIROp tmp = new IGIROp(CAST_TO);    
		tmp.module = module;
		return tmp; 
	}
	public static IGIROp instance_of(
Type module
) { 
		IGIROp tmp = new IGIROp(INSTANCE_OF);    
		tmp.module = module;
		return tmp; 
	}
	public static IGIROp array_load(
Type module
,
Type type
) { 
		IGIROp tmp = new IGIROp(ARRAY_LOAD);    
		tmp.module = module;
		tmp.type = type;
		return tmp; 
	}
	public static IGIROp array_store(
Type module
,
Type type
) { 
		IGIROp tmp = new IGIROp(ARRAY_STORE);    
		tmp.module = module;
		tmp.type = type;
		return tmp; 
	}
	public static IGIROp array_new(
Type module
,
Type type
) { 
		IGIROp tmp = new IGIROp(ARRAY_NEW);    
		tmp.module = module;
		tmp.type = type;
		return tmp; 
	}
	public static IGIROp array_length(
Type module
,
Type type
) { 
		IGIROp tmp = new IGIROp(ARRAY_LENGTH);    
		tmp.module = module;
		tmp.type = type;
		return tmp; 
	}
	public static IGIROp array_resize(
Type module
,
Type type
) { 
		IGIROp tmp = new IGIROp(ARRAY_RESIZE);    
		tmp.module = module;
		tmp.type = type;
		return tmp; 
	}
	public static IGIROp array_fill(
Type module
,
Type type
) { 
		IGIROp tmp = new IGIROp(ARRAY_FILL);    
		tmp.module = module;
		tmp.type = type;
		return tmp; 
	}
	public static IGIROp array_copy(
Type module
,
Type type
) { 
		IGIROp tmp = new IGIROp(ARRAY_COPY);    
		tmp.module = module;
		tmp.type = type;
		return tmp; 
	}
	public static IGIROp local_inc_i32(
int index
,
double value
) { 
		IGIROp tmp = new IGIROp(LOCAL_INC_I32);    
		tmp.index = index;
		tmp.value = value;
		return tmp; 
	}
	public static IGIROp stack_inc_i32(
double value
) { 
		IGIROp tmp = new IGIROp(STACK_INC_I32);    
		tmp.value = value;
		return tmp; 
	}
	public static IGIROp stack_inc_f64(
double value
) { 
		IGIROp tmp = new IGIROp(STACK_INC_F64);    
		tmp.value = value;
		return tmp; 
	}
	public static IGIROp call_vtable(
Type module
,
String name
,
int count
) { 
		IGIROp tmp = new IGIROp(CALL_VTABLE);    
		tmp.module = module;
		tmp.name = name;
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp call_super(
Type module
,
String name
,
int count
) { 
		IGIROp tmp = new IGIROp(CALL_SUPER);    
		tmp.module = module;
		tmp.name = name;
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp call_static(
Type module
,
String name
,
int count
) { 
		IGIROp tmp = new IGIROp(CALL_STATIC);    
		tmp.module = module;
		tmp.name = name;
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp call_interface(
Type module
,
String name
,
int count
) { 
		IGIROp tmp = new IGIROp(CALL_INTERFACE);    
		tmp.module = module;
		tmp.name = name;
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp call_dynamic(
Type module
,
String name
,
int count
) { 
		IGIROp tmp = new IGIROp(CALL_DYNAMIC);    
		tmp.module = module;
		tmp.name = name;
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp call_ret1_vtable(
Type module
,
String name
,
int count
) { 
		IGIROp tmp = new IGIROp(CALL_RET1_VTABLE);    
		tmp.module = module;
		tmp.name = name;
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp call_ret1_super(
Type module
,
String name
,
int count
) { 
		IGIROp tmp = new IGIROp(CALL_RET1_SUPER);    
		tmp.module = module;
		tmp.name = name;
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp call_ret1_static(
Type module
,
String name
,
int count
) { 
		IGIROp tmp = new IGIROp(CALL_RET1_STATIC);    
		tmp.module = module;
		tmp.name = name;
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp call_ret1_interface(
Type module
,
String name
,
int count
) { 
		IGIROp tmp = new IGIROp(CALL_RET1_INTERFACE);    
		tmp.module = module;
		tmp.name = name;
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp call_ret1_dynamic(
Type module
,
String name
,
int count
) { 
		IGIROp tmp = new IGIROp(CALL_RET1_DYNAMIC);    
		tmp.module = module;
		tmp.name = name;
		tmp.count = count;
		return tmp; 
	}
	public static IGIROp end(
) { 
		IGIROp tmp = new IGIROp(END);    
		return tmp; 
	}
public static String convertToString(IGIROp op) {
	switch(op.op) {
		case NOP: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "NOP";
		}
		case POP: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "POP" + ", count: " + op.count;
		}
		case DUP: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "DUP" + ", count: " + op.count;
		}
		case DUP2: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "DUP2";
		}
		case DUP1X: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "DUP1X";
		}
		case DUP2X: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "DUP2X";
		}
		case RET0: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "RET0";
		}
		case RET1: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "RET1" + ", type: " + op.type;
		}
		case RET1_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "RET1_I32" + ", type: " + op.type + ", value: " + op.value;
		}
		case ADD_F64_VEC: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "ADD_F64_VEC" + ", count: " + op.count;
		}
		case SUB_F64_VEC: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "SUB_F64_VEC" + ", count: " + op.count;
		}
		case MUL_F64_VEC: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "MUL_F64_VEC" + ", count: " + op.count;
		}
		case DIV_F64_VEC: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "DIV_F64_VEC" + ", count: " + op.count;
		}
		case MUL_F64_VEC_VAL: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "MUL_F64_VEC_VAL" + ", count: " + op.count;
		}
		case CROSS_F64_VEC: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CROSS_F64_VEC";
		}
		case DOT_F64_VEC: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "DOT_F64_VEC" + ", count: " + op.count;
		}
		case LENGTH_F64_VEC: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "LENGTH_F64_VEC" + ", count: " + op.count;
		}
		case ABS_F64_VEC: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "ABS_F64_VEC" + ", count: " + op.count;
		}
		case IIF: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "IIF" + ", type: " + op.type + ", count: " + op.count;
		}
		case IIF_PIECEWISE: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "IIF_PIECEWISE" + ", type: " + op.type + ", count: " + op.count;
		}
		case ADD_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "ADD_I32";
		}
		case SUB_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "SUB_I32";
		}
		case MUL_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "MUL_I32";
		}
		case DIV_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "DIV_I32";
		}
		case POW_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "POW_I32";
		}
		case POW_I32_CONST: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "POW_I32_CONST" + ", value: " + op.value;
		}
		case MOD_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "MOD_I32";
		}
		case MLA_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "MLA_I32";
		}
		case AND_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "AND_I32";
		}
		case OR_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "OR_I32";
		}
		case XOR_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "XOR_I32";
		}
		case ASR_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "ASR_I32";
		}
		case LSR_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "LSR_I32";
		}
		case LSL_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "LSL_I32";
		}
		case NOT_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "NOT_I32";
		}
		case ADD_F64: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "ADD_F64";
		}
		case SUB_F64: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "SUB_F64";
		}
		case MUL_F64: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "MUL_F64";
		}
		case POW_F64: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "POW_F64";
		}
		case POW_F64_CONST: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "POW_F64_CONST" + ", value: " + op.value;
		}
		case DIV_F64: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "DIV_F64";
		}
		case MOD_F64: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "MOD_F64";
		}
		case MLA_F64: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "MLA_F64";
		}
		case CMP_EQ_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CMP_EQ_I32";
		}
		case CMP_NE_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CMP_NE_I32";
		}
		case CMP_LT_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CMP_LT_I32";
		}
		case CMP_LE_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CMP_LE_I32";
		}
		case CMP_EQ_F64: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CMP_EQ_F64";
		}
		case CMP_LT_F64: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CMP_LT_F64";
		}
		case CMP_LE_F64: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CMP_LE_F64";
		}
		case CMP_EQ_I64: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CMP_EQ_I64";
		}
		case CMP_EQ_FP: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CMP_EQ_FP";
		}
		case CMP_EQ_OBJ: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CMP_EQ_OBJ";
		}
		case I32_TO_F64: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "I32_TO_F64";
		}
		case F64_TO_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "F64_TO_I32";
		}
		case PUSH_F64: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "PUSH_F64" + ", value: " + op.value;
		}
		case PUSH_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "PUSH_I32" + ", value: " + op.value;
		}
		case PUSH_STRING: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "PUSH_STRING" + ", name: " + op.name;
		}
		case PUSH_OBJ_NULL: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "PUSH_OBJ_NULL";
		}
		case PUSH_FP_NULL: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "PUSH_FP_NULL";
		}
		case LOCAL_LOAD: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "LOCAL_LOAD" + ", type: " + op.type + ", index: " + op.index;
		}
		case LOCAL_STORE: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "LOCAL_STORE" + ", type: " + op.type + ", index: " + op.index;
		}
		case LOCAL_CLEAR: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "LOCAL_CLEAR" + ", index: " + op.index + ", type: " + op.type;
		}
		case LOCAL_LOAD_VEC: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "LOCAL_LOAD_VEC" + ", type: " + op.type + ", index: " + op.index + ", count: " + op.count;
		}
		case LOCAL_STORE_VEC: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "LOCAL_STORE_VEC" + ", type: " + op.type + ", index: " + op.index + ", count: " + op.count;
		}
		case DUP_VEC: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "DUP_VEC" + ", count: " + op.count;
		}
		case THIS_LOAD: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "THIS_LOAD" + ", module: " + op.module + ", name: " + op.name;
		}
		case THIS_STORE: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "THIS_STORE" + ", module: " + op.module + ", name: " + op.name;
		}
		case LOCAL_MEMBER_LOAD: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "LOCAL_MEMBER_LOAD" + ", module: " + op.module + ", name: " + op.name + ", index: " + op.index;
		}
		case MEMBER_LOAD: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "MEMBER_LOAD" + ", module: " + op.module + ", name: " + op.name;
		}
		case MEMBER_STORE: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "MEMBER_STORE" + ", module: " + op.module + ", name: " + op.name;
		}
		case STATIC_LOAD: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "STATIC_LOAD" + ", module: " + op.module + ", name: " + op.name;
		}
		case STATIC_STORE: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "STATIC_STORE" + ", module: " + op.module + ", name: " + op.name;
		}
		case FUNCPTR_MEMBER: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "FUNCPTR_MEMBER" + ", module: " + op.module + ", name: " + op.name;
		}
		case FUNCPTR_STATIC: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "FUNCPTR_STATIC" + ", module: " + op.module + ", name: " + op.name;
		}
		case _NEW: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "_NEW" + ", module: " + op.module;
		}
		case TRY_PUSH: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "TRY_PUSH" + ", label: " + op.label;
		}
		case CATCH_ENTRY: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CATCH_ENTRY" + ", label: " + op.label;
		}
		case _THROW: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "_THROW" + ", module: " + op.module;
		}
		case JMP: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "JMP" + ", target_label: " + op.target_label;
		}
		case J0_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "J0_I32" + ", target_label: " + op.target_label;
		}
		case JN0_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "JN0_I32" + ", target_label: " + op.target_label;
		}
		case NOT: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "NOT";
		}
		case AND: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "AND" + ", target_label: " + op.target_label;
		}
		case OR: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "OR" + ", target_label: " + op.target_label;
		}
		case CAST_TO: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CAST_TO" + ", module: " + op.module;
		}
		case INSTANCE_OF: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "INSTANCE_OF" + ", module: " + op.module;
		}
		case ARRAY_LOAD: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "ARRAY_LOAD" + ", module: " + op.module + ", type: " + op.type;
		}
		case ARRAY_STORE: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "ARRAY_STORE" + ", module: " + op.module + ", type: " + op.type;
		}
		case ARRAY_NEW: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "ARRAY_NEW" + ", module: " + op.module + ", type: " + op.type;
		}
		case ARRAY_LENGTH: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "ARRAY_LENGTH" + ", module: " + op.module + ", type: " + op.type;
		}
		case ARRAY_RESIZE: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "ARRAY_RESIZE" + ", module: " + op.module + ", type: " + op.type;
		}
		case ARRAY_FILL: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "ARRAY_FILL" + ", module: " + op.module + ", type: " + op.type;
		}
		case ARRAY_COPY: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "ARRAY_COPY" + ", module: " + op.module + ", type: " + op.type;
		}
		case LOCAL_INC_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "LOCAL_INC_I32" + ", index: " + op.index + ", value: " + op.value;
		}
		case STACK_INC_I32: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "STACK_INC_I32" + ", value: " + op.value;
		}
		case STACK_INC_F64: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "STACK_INC_F64" + ", value: " + op.value;
		}
		case CALL_VTABLE: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CALL_VTABLE" + ", module: " + op.module + ", name: " + op.name + ", count: " + op.count;
		}
		case CALL_SUPER: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CALL_SUPER" + ", module: " + op.module + ", name: " + op.name + ", count: " + op.count;
		}
		case CALL_STATIC: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CALL_STATIC" + ", module: " + op.module + ", name: " + op.name + ", count: " + op.count;
		}
		case CALL_INTERFACE: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CALL_INTERFACE" + ", module: " + op.module + ", name: " + op.name + ", count: " + op.count;
		}
		case CALL_DYNAMIC: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CALL_DYNAMIC" + ", module: " + op.module + ", name: " + op.name + ", count: " + op.count;
		}
		case CALL_RET1_VTABLE: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CALL_RET1_VTABLE" + ", module: " + op.module + ", name: " + op.name + ", count: " + op.count;
		}
		case CALL_RET1_SUPER: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CALL_RET1_SUPER" + ", module: " + op.module + ", name: " + op.name + ", count: " + op.count;
		}
		case CALL_RET1_STATIC: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CALL_RET1_STATIC" + ", module: " + op.module + ", name: " + op.name + ", count: " + op.count;
		}
		case CALL_RET1_INTERFACE: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CALL_RET1_INTERFACE" + ", module: " + op.module + ", name: " + op.name + ", count: " + op.count;
		}
		case CALL_RET1_DYNAMIC: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "CALL_RET1_DYNAMIC" + ", module: " + op.module + ", name: " + op.name + ", count: " + op.count;
		}
		case END: {
			String lbl = (op.label != null) ? ("	" + op.label + ":	") : ("		");
			return op.stack + "\t" + lbl + "END";
		}
		default:{
			throw new RuntimeException("Invalid");
		}
	}
}
}
