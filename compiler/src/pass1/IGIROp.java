package pass1;

public class IGIROp
{
	public int    op       = 0;
	public int    stack    = 9999;
	//public int    sp_delta = 0;



	public Type   module   = null;		// ie.  iglang.Object
	public String name     = null;		// ie.  toString

	
	public int count = 0;
	public int    index    = 0;			// ie. local varible index
	public Type   type     = null;


	public double value = 0;		// int and double values
	public String label = null;
	public String target_label = null;	// the label this op refers t


	public IGIROp dup() {
		IGIROp other = new IGIROp(op);
		other.stack = stack;
		other.module = module;
		other.name = name;
		other.count = count;
		other.index = index;
		other.type = type;
		other.value = value;
		other.label = label;
		other.target_label = target_label;
		return other;
	}

	public IGIROp(int _op) {
		op = _op;
	}

	public int getParameterCount() {
		return count;
	}

	// this is just for preparing those little cute chains
	// this is not the "next" op
	public IGIROp m_next;
	public IGIROp chain(IGIROp next) {
		next.m_next = this;
		return next;
	}

	public String toString() {
		return IGIR.convertToString(this);
	}
}