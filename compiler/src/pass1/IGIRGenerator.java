/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;


import util.*;
import pass1.*;

import java.util.*;
import java.io.*;



public final class IGIRGenerator extends IGConsts
{
	/////////////////////////////////////////////
	// Processor Code
	/////////////////////////////////////////////

	private  Token []   s_token_list = null;
	private   int             s_token_index = 0;
	private   int             s_token_end = 0;
	private   Token           m_eof;
	
	
	
	public  void error(Token stok, String text)
	{

		stok.printError(text);
		System.exit(1);

	}
	
	public Token expect(int token_type) {
		return expect(token_type, "no message defined");
	}
	
	public  Token expect(int token_type, String text)
	{
		Token stok = read();
		if (stok.m_type != token_type)
		{
			stok.printError(text);
			System.exit(1);
		}
		return stok;
	}
	
	protected int getTokenIndex() {
		return s_token_index;
	}
	
	protected void setTokenIndex(int ti) {
		s_token_index = ti;
	}
	
	
	protected  void setSource(Token [] list, int start_index, int end_index)
	{
		s_token_list = list;
		s_token_index = start_index;
		s_token_end = end_index;
		m_eof = list[list.length - 1];
	}
	
	protected  Token read()
	{
		if (s_token_index >= s_token_end) return m_eof;
		
		Token t = s_token_list[s_token_index];
		s_token_index ++;
		
		if (t.m_discarded) {
			t = read();
		}
		
		return t;
	}
	
	protected  int peekTokenType()
	{
		return peek().m_type;
	}
	
	protected  Token prev()
	{
		s_token_index --;
		Token t = s_token_list[s_token_index];
		return t;
	}
	
	protected Token peekAt(int absolute_position)
	{
		if (absolute_position >= s_token_end) return m_eof;
		
		Token t = s_token_list[absolute_position];
		return t;
	}
	
	protected Token findPair(int absolute_position)
	{
		Token tok = peekAt(absolute_position);
		if (tok.m_type != TOK_LBRACE) {
			error(tok, "findPair only works on { at the moment");
		}
		
		int height = 1;
		while (height > 0 && tok.m_type != TOK_EOF) 
		{
			if (tok.m_type == TOK_LBRACE) {
				height ++;
			}
			if (tok.m_type == TOK_RBRACE) {
				height --;
				
				if (height == 0) {
					break;
				}
			}
			
			absolute_position++;
			tok = peekAt(absolute_position);
		}
		
		return tok;
	}

	protected  Token peek()
	{
		if (s_token_index >= s_token_end) return m_eof;
		
		Token t = s_token_list[s_token_index];
		
		if (t.m_discarded) {
			read();
			return peek();
		}
		return t;
	}

	public static boolean hasNode(Token t, int node_type)
	{
		return t.m_node != null && t.m_node.m_node_type == node_type;
	}

	///////////////////////////////////////////////////////


	public static final boolean INCREMENT_OPTIMIZATION = true;
	private  int s_current_protection = 0;
	
	private  void newHeaderSource()
	{
		s_current_protection = 0;
	}
	

	private ArrayList<String> m_string_list            = new ArrayList<String>();
	private StringIntHashtable m_string_map = new StringIntHashtable();
	
	private int getStringIndex(String s) {
		if (!m_string_map.containsKey(s)) {
			m_string_map.put(s, m_string_list.size());
			m_string_list.add(s);
		}
		
		return m_string_map.get(s);
	}
	
	
	
	//////////////////////
	// Above this is most likely old relic code
	
	private int m_label_id = 0;
	
	
	private final void resetLabelCounter() {
		m_label_id = 0;
	}
	
	/**
	 * Allocate a label to use.  Is guaranteed to be unique within the file
	 */
	
	private final String getLabel() {
		String lbl =  "L" + m_label_id;
		m_label_id ++;
		return lbl;
	}
	
	private final String getLabel(String pre) {
		String lbl =  pre + m_label_id;
		m_label_id ++;
		return lbl;
	}
	
	private final String helperTrace(IGNode n) {
		String t = "";
		if (n.m_children.size() > 0) {
			t += helperTrace(n.m_children.get(0));
		}
		
		t += " " + n.m_node_type + "'" + n.m_token.m_value + "'";
		
		if (n.m_children.size() > 1) {
			t += helperTrace(n.m_children.get(1));
		}
		
		return t;
	}
	
	
	/*
	 * Cast a node to a particular dst_type.
	 */ 
	
	
	private final void outputFunctionWrapperCast(IGMember m, IGNode right, Type z_scope_type, IGIRModule.Function pw) throws IOException
	{
		while (right.m_node_type == IGNode.NODE_TYPE_BRACKETED_EXPRESSION) {
			right = right.m_children.get(0);
		}

		// not sure how I confused this
		// scopes on types are only valid for IGSource references, not for IGMember references
		
		final IGMember fn = (IGMember)right.m_scope;
		final int scope_type = fn.getScopeType();
		
		Type source_type = m_ctx.resolveType(fn.getSource().getInstanceDataType());
		
		if (IGScopeItem.SCOPE_ITEM_STATIC_FUNCTION == scope_type)
		{
			pw.addFunctionDependency(source_type, fn.getMemberNameAsString());
			pw.op(IGIR.funcptr_static(source_type, fn.getMemberNameAsString()));
		}
		else if (IGScopeItem.SCOPE_ITEM_MEMBER_FUNCTION == scope_type) 
		{
			if (right.m_node_type == IGNode.NODE_DOT)
			{
				outputFunctionBody(m, right.m_children.get(0), pw);
				
				pw.addFunctionDependency(source_type, fn.getMemberNameAsString());
				pw.op(IGIR.funcptr_member(source_type, fn.getMemberNameAsString()));			
			}
			else if (right.m_node_type == IGNode.FIELD_ACCESS)
			{

				pw.addFunctionDependency(source_type, fn.getMemberNameAsString());
			
				pw.op(IGIR.local_load(source_type, 0));
				pw.op(IGIR.funcptr_member(source_type, fn.getMemberNameAsString()));	
			}
			else
			{
				right.m_token.printError("Unsupported case");
				throw new RuntimeException("Unsupported case!");
			}
		}
		else {
			throw new RuntimeException("How did this get compiled?");
		}	
	}
	
	
	private final void outputCast(IGMember m, IGNode right, Type dst_type, IGIRModule.Function pw) throws IOException
	{
		Type right_type = right.m_type;
		
		if (right.m_type.isFunctionWrapper())
		{
			outputFunctionWrapperCast(m, right, null, pw);
		}
		else
		{

			outputFunctionBody(m, right, pw);
			castTo(right_type, dst_type, pw);		
		}	
	}
	
	
	
	/**
	 * output the assembly necessary to cast one type to another
	 *  e.g. double to int
	 *  e.g. Object to String
	 */
	
	private final void castTo(Type src_type, Type dst_type, IGIRModule.Function pw) throws IOException
	{
		castTo(src_type, dst_type, pw, false);
	}
	
	private final void castTo(Type src_type, Type dst_type, IGIRModule.Function pw, boolean explicit) throws IOException
	{
		// save the original values for debugging purposes
		final Type ps = src_type;
		final Type pd = dst_type;
		
		
		src_type = m_ctx.resolveType(src_type);
		dst_type = m_ctx.resolveType(dst_type);
							 
		if (src_type == Type.SHORT || src_type == Type.BYTE) { src_type = Type.INT; }
		if (dst_type == Type.SHORT || dst_type == Type.BYTE) { dst_type = Type.INT; }							 
		 
		if (src_type == Type.FLOAT) { src_type = Type.DOUBLE; }
		if (dst_type == Type.FLOAT) { dst_type = Type.DOUBLE; }	

		if (dst_type == null) {
			return;
		}
		
		if (src_type == dst_type) {
			return;
		}
		
	
		Type type_bool   = Type.BOOL;
		Type type_int    = Type.INT;
		//Type type_uint   = Type.UINT;
		Type type_double = Type.DOUBLE;
		Type type_string = Type.STRING;
		
		
		
		boolean src_intish = src_type == Type.INT;;//  || src_type == Type.UINT;
		boolean dst_intish = dst_type == Type.INT;//  ||  dst_type == Type.UINT;	
	
		
		//boolean explicit = true;
		
		int work = src_type.workRequiredToCastTo(dst_type, explicit);

		
		
		
		if (work == Type.WORK_NONE) {
			return;
		}
		else if (work == Type.WORK_INCOMPATIBLE) 
		{
			//System.err.println("attempted to cast: " + ps + " => " + pd);
			throw new RuntimeException("Invalid cast: " + src_type + " => " + dst_type + " explicit: " + explicit);
		}
		
		if (work == Type.WORK_ENUM_CAST) {
			// do nothing
		}
		else if (work == Type.WORK_NULL_TO_DELEGATE_NULL)
		{
			pw.op(IGIR.pop(1));
			pw.op(IGIR.push_fp_null());
		}
		else if (work == Type.WORK_NIL_OBJECT) {
			pw.op(IGIR.push_obj_null());
		}
		else if (work == Type.WORK_NIL_DELEGATE) {
			pw.op(IGIR.push_fp_null());
		}
		else if (work == Type.WORK_NIL_INT ||
				work == Type.WORK_NIL_BOOL) {
			pw.op(IGIR.push_i32(0));
		}
		else if (work == Type.WORK_NIL_DOUBLE) {
			pw.op(IGIR.push_f64(0));
		}
		else if (work == Type.WORK_OBJECT_CAST) {
			if (dst_type == Type.STRING) {
				pw.op(IGIR.call_ret1_static(Type.STRING,  "stringFromObject", 1));
				pw.addFunctionDependency(Type.STRING, "stringFromObject");
			}
			else
			{
				pw.addModuleDependency(dst_type);
				pw.op(IGIR.cast_to(dst_type));
			}
		}
		else if (work == Type.WORK_PRIMITIVE_CAST) {
			if ((src_intish || src_type == type_bool) && dst_type == type_double) {
				pw.op(IGIR.i32_to_f64());
			}
			else if (src_type == type_double && dst_intish) {
				pw.op(IGIR.f64_to_i32());
			}
			else if (src_type == type_bool && dst_intish) {
				// do nothing
			}	
			else if (src_intish && dst_type == type_bool) {
				pw.op(IGIR.push_i32(0));
				pw.op(IGIR.cmp_ne_i32());
			}
			else
			{
				throw new RuntimeException("Invalid primitive cast");
			}
			//i32_to_f64
			//f64_to_i32
		}
		else if (work == Type.WORK_PRIMITIVE_TO_STRING)
		{
			if (src_intish) {
				pw.addFunctionDependency(Type.STRING, "stringFromInt");
				pw.op(IGIR.call_ret1_static(Type.STRING, "stringFromInt",1));
			}
			else if (src_type == type_bool) {
				pw.addFunctionDependency(Type.STRING, "stringFromBool");
				pw.op(IGIR.call_ret1_static(Type.STRING, "stringFromBool",1));
			}
			else if (src_type == type_double) {
				pw.addFunctionDependency(Type.STRING, "stringFromDouble");
				pw.op(IGIR.call_ret1_static(Type.STRING, "stringFromDouble",1));
			}
			
			else
			{
				throw new RuntimeException("Unknown cast: '" + src_type + "' => '" + dst_type + "'");
			}
		}
		else if (work == Type.WORK_STRING_TO_PRIMITIVE)
		{
			if (dst_intish) {
				pw.addFunctionDependency(Type.STRING, "intValue");
				pw.op(IGIR.call_ret1_static(Type.STRING, "intValue",1));
			}
			else if (dst_type == type_bool) {
				pw.addFunctionDependency(Type.STRING, "boolValue");
				pw.op(IGIR.call_ret1_static(Type.STRING, "boolValue",1));
			}
			else if (dst_type == type_double) {
				pw.addFunctionDependency(Type.STRING, "doubleValue");
				pw.op(IGIR.call_ret1_static(Type.STRING, "doubleValue",1));			
			}	
			else
			{
				throw new RuntimeException("Unknown cast: " + src_type + " => " + dst_type);
			}
		}		
		else if (work == Type.WORK_ENUM_TO_STRING) {
			pw.addFunctionDependency(src_type, "toString");
			pw.op(IGIR.call_ret1_static(src_type, "toString", 1));
		}
		else
		{
			pw.addModuleDependency(dst_type);
			pw.op(IGIR.cast_to(dst_type));	//, src_type.toString(), dst_type.toString());
		}
	}
	
	
	private final IGIROp [] outputFunctionBody(IGMember m, IGNode node, IGIRModule.Function pw) throws IOException
	{
		return outputFunctionBody(m, node, pw, 0);
	}
	
	private final int getLocalIndex(IGMember m, Token name, boolean setter_context)
	{

		int idx =  m.lookupLocalVariable(name, setter_context);
		if (idx == -1) {
			name.printError("what ?");
			throw new RuntimeException("Local not found with name: " + name.m_value);
		}
		
		// but what about things that "DON'T" have super declared
		if (!m.hasModifier(IGMember.STATIC) && m.m_parent_source.m_class_extends != null) 
		{ 
			// super and this are declared as the first parameters
			idx = idx - 1;
			if (idx < 0) idx = 0;
		}
		
		return idx;

	}
	
	private final int getLocalIndex(IGMember m, Token blame, String name, int index, boolean setter_context)
	{

		int idx =  m.lookupLocalVariable(name, index, setter_context);
		if (idx == -1) {	
			blame.printError("what ?");
			
			m.debugVariables();
			throw new RuntimeException("Variable not found with name: " + name);
		}
		
		// THIS IS SUPER HACKY
		// TODO REPLACE WITH A DIFFERENT SOLUTION
		// SHOULD REALLY CALCULATE THIS UP FRONT BASED OFF OF PARAMETERS
		// AND LIFECYCLES
		if (!m.hasModifier(IGMember.STATIC) && m.m_parent_source.m_class_extends != null) 
		{ 
			// super and this are declared as the first parameters
			idx = idx - 1;
			if (idx < 0) idx = 0;
		}
		
		return idx;

	}
	
	
	private static final int BEHAVIOUR_LOAD  = 0;
	private static final int BEHAVIOUR_STORE = 1;
	
	
	private ArrayList<String> m_continue_break = new ArrayList<String>();
	private final void pushContinueBreak(String c_label, String b_label) {
		m_continue_break.add(c_label);
		m_continue_break.add(b_label);
	}
	
	private final String getContinueLabel() {
		return m_continue_break.get(m_continue_break.size() - 2);
	}

	private final String getBreakLabel() {
		return m_continue_break.get(m_continue_break.size() - 1);
	}
	
	private final void popContinueBreak() {
		// pop the two records off
		m_continue_break.remove(m_continue_break.size() - 1);
		m_continue_break.remove(m_continue_break.size() - 1);
	}
	
	/**
	 * Return the op necessary to append a specific type to the string builder
	 */
	
	private final String stringBuilderAppend(Type type, IGIRModule.Function pw) throws IOException {
	
		Type dst_type  = Type.STRING;
		
	
		Type src_type = m_ctx.resolveType(type);					 
		int work = src_type.workRequiredToCastTo(dst_type, true);

		
		if (work == Type.WORK_NONE) {
			return "appendObject";
		}
		else if (work == Type.WORK_INCOMPATIBLE) {

			throw new RuntimeException("stringBuilderAppend Invalid cast: " + src_type + " => " + dst_type);
		}
	
		if (work == Type.WORK_OBJECT_CAST) {
			return "appendObject";
		}
		else if (work == Type.WORK_PRIMITIVE_TO_STRING)
		{
			if (src_type == Type.INT) { // || src_type == Type.UINT) {
				return "appendInt";
			}
			else if (src_type == Type.SHORT || src_type == Type.BYTE) {
				return "appendInt";
			}
			else if (src_type == Type.DOUBLE) {
				return "appendDouble";
			}
			else if (src_type == Type.BOOL) {
				return "appendBool";
			}
			else {
				throw new RuntimeException("Invalid PRIMITIVE TO STRING");
			}
		}	
		else if (work == Type.WORK_ENUM_TO_STRING) {
			pw.addFunctionDependency(src_type, "toString");
			pw.op(IGIR.call_ret1_static(src_type, "toString", 1));	
			return "appendObject";
		}
		else
		{
			throw new RuntimeException("stringBuilderAppend Invalid cast: " + src_type + " => " + dst_type);
		}
	}
	
	private final IGIROp[] processStringBuilder(IGMember m, IGNode node, IGIRModule.Function pw, int behaviour) throws IOException
	{

		Type type_string_builder = Type.get("iglang.StringBuilder");

		pw.addModuleDependency(Type.STRING);

		pw.op(IGIR._new(type_string_builder));
		pw.op(IGIR.dup(1));

		
		pw.addFunctionDependency(type_string_builder, "<constructor>");
		pw.addFunctionDependency(type_string_builder, "toString");

		pw.op(IGIR.call_static(type_string_builder,"<constructor>",1));
		
		ArrayList<IGNode> to_process = new ArrayList<IGNode>();
	
		while (node.m_node_type == IGNode.NODE_BINARY &&
		       node.m_token.m_type == TOK_ADD &&
		       node.m_type == Type.STRING) {
		       
		 	IGNode left = node.m_children.get(1);
		 	IGNode right = node.m_children.get(0);
		 	
		 	to_process.add(left);
		 	node = right; 
		}
	
		to_process.add(node);
		
		for (int i = to_process.size() - 1; i >= 0; i--) {
			node = to_process.get(i);
			outputFunctionBody(m, node, pw);
			
			String append_function = stringBuilderAppend(node.m_type, pw);

			pw.addFunctionDependency(type_string_builder, append_function);
			pw.op(IGIR.call_ret1_static(type_string_builder, append_function, 2));
		}
			 		
		pw.op(IGIR.call_ret1_static(type_string_builder,"toString",1));

		return empty2();
	}
	
	private static IGIROp [] s_empty2 = null;
	private static IGIROp [] s_empty3 = null;
	
	private final IGIROp[] empty2() {
		if (s_empty2 == null) {
			s_empty2 = new IGIROp[2];
			s_empty2[0] = null;
			s_empty2[1] = null;
		}
		return s_empty2;
	}
	
	private final IGIROp[] empty3() {
		if (s_empty3 == null) {
			s_empty3 = new IGIROp[3];
			s_empty3[0] = null;
			s_empty3[1] = null;
			s_empty3[2] = null;
		}
		return s_empty3;
	}


	public static final int BEHAVIOUR_READ_WRITE = 1;

	private final IGIROp []  localLoad(IGIRModule.Function pw, 
			Type type, int load_index)
		{	
		return localLoadStore(pw, type, load_index, -9999);
	}

	private final IGIROp []  localLoadStore(IGIRModule.Function pw, 
			Type type, int load_index, int store_index)
	{
		 if (store_index >= 0)
	 	{
 			IGIROp [] diffs =  new IGIROp[2];

 			int vector_size = type.getTupleCount();
 			if (vector_size == 1) {

				diffs[0] = IGIR.local_load(type, load_index);
				diffs[1] = IGIR.dup(1).chain(IGIR.local_store(type, store_index));
			}
			else 
			{
				diffs[0] = IGIR.local_load_vec(type,load_index, vector_size);
				diffs[1] = IGIR.dup_vec(vector_size).chain(IGIR.local_store_vec(type,store_index, vector_size));
			}
			return diffs;
	 	}
	 	else
	 	{

	 		final int tuple_size =  type.getTupleCount();

	 		if (tuple_size == 1) {
	 			pw.op(IGIR.local_load(type, load_index));
	 		}
	 		else {
	 			pw.op(IGIR.local_load_vec(type,load_index, tuple_size));
	 		}

		 	return null;
		}
	}
	
	
	private final IGIROp[] outputFunctionBody(IGMember m, IGNode node, IGIRModule.Function pw, int behaviour) throws IOException
	{
		switch (node.m_node_type)
		{
			case IGNode.NODE_RETURN: break;
			case IGNode.NODE_BLOCK: break;
			case IGNode.NODE_CALL: break;
			case IGNode.NODE_PARAMETER_LIST: break;
			case IGNode.NODE_TYPE_LOCAL_VARIABLE_DEFINITION: break;
			case IGNode.ID_TYPE_PAIR: break;
			case IGNode.LOCAL_VARIABLE_ACCESS: break;
			case IGNode.STATIC_FIELD_ACCESS: break;
			case IGNode.NODE_TRINARY: break;
			case IGNode.NODE_VALUE: break;
			case IGNode.NODE_TYPE_BRACKETED_EXPRESSION: break;
			case IGNode.NODE_TYPE_CAST:  break;
			case IGNode.NODE_BINARY: break;
			case IGNode.NODE_IF: break;
			case IGNode.NODE_DOT: break;
			case IGNode.NODE_NEW: break;		
			case IGNode.DOT_ACCESSOR: break;
			case IGNode.STATIC_DOT_ACCESSOR: break;
			case IGNode.NATIVE_BLOCK: break;
			case IGNode.FIELD_ACCESS: break;
			case IGNode.INCREMENT: break;
			case IGNode.NODE_INDEX : break;
			case IGNode.LOCAL_ACCESSOR: break;
			case IGNode.STATIC_LOCAL_ACCESSOR: break;
			case IGNode.NODE_UNARY: break;
			case IGNode.STATEMENT: break;
			case IGNode.PARAMETER_CAST: break;
			case IGNode.THROW: break;
			case IGNode.TRY: break;
			case IGNode.NODE_FOR: break;
			case IGNode.NODE_TYPE_FOREACH: break;
			case IGNode.FLOW_CHANGE:	break;	// breaks, continues etc
			case IGNode.SWITCH:	break;
			case IGNode.SWITCH_CASE:	break;
			case IGNode. SWITCH_DEFAULT:	break;
			default:
				//pw.comment("Unknown node: " + node.m_node_type + " " + node.m_children.size());
				if (node.m_node_type == 0 && node.m_token != null) {
					node.debug(1);
					node.m_token.printError("Non typed node: ");
				}
				
				break;
		}	
		switch (node.m_node_type)
		{
			case IGNode.NODE_TYPE_LOCAL_VARIABLE_NAME 		: { throw new RuntimeException("what?"); }

			 case IGNode.NODE_TYPE_LOCAL_VARIABLE_DEFINITION : { 
			 	if (node.m_children.size() != 1) {
			 		throw new RuntimeException("Invalid arg");
			 	}
			 	
			 	outputFunctionBody(m, node.m_children.get(0), pw);
			 	
			 	// pop the values off the stack following a store
			 	if (node.m_children.get(0).m_token.m_type == TOK_ASSIGN) {
			 		pw.op(IGIR.pop(node.m_type.getTupleCount()));
			 	}

			 	
			 	break;
			 }
			 
			 case IGNode.STATEMENT: 
			 {
			 	outputFunctionBody(m, node.m_children.get(0), pw);
			 	
			 	// if there's anything still left on the stack at this point pop it off
			 	if (node.m_type != Type.VOID) {
			 		pw.op(IGIR.pop(node.m_type.getTupleCount()));
			 	}
			 	break;
			 }

			 //////////////////////////////////////////////////////////////
			 
			 case IGNode.LOCAL_VARIABLE_ACCESS     : 
			 { 
			 	if (behaviour == BEHAVIOUR_READ_WRITE)
			 	{
		 			
		 			final int load_index  = getLocalIndex(m, node.m_token, false);
		 			final int store_index = getLocalIndex(m, node.m_token, true);
		 			
		 			final String local_variable_name = node.m_token.m_value;
		 			
		 			return localLoadStore(pw, node.m_type, load_index, store_index);
			 	}
			 	else
			 	{

			 		final int load_index  = getLocalIndex(m, node.m_token,false);

			 		localLoad(pw, node.m_type, load_index);
				}
			 	break;
			 }

	
				// ID : TYPE
				case IGNode.ID_TYPE_PAIR              : { 
					if (node.m_children.size() != 2) {
						throw new RuntimeException("Invalid arg");
					}
					
					IGNode id_node = node.m_children.get(0);
					
					
					if (behaviour == BEHAVIOUR_READ_WRITE) 
					{
			 			final int index  = getLocalIndex(m, id_node.m_token, true);
	
			 			final String local_variable_name = id_node.m_token.m_value;
			 			return localLoadStore(pw, id_node.m_type, index, index);
					}
					else
					{
						final int local_index = getLocalIndex(m, id_node.m_token, true) ;
						if (id_node.m_type.isTuple()) 
						{
							final Type [] tuple_types = id_node.m_type.getTupleTypes();
							for (int i = 0; i < tuple_types.length; i++) {
								pw.op(IGIR.local_clear(local_index + i, tuple_types[i]));
				 			}
						}
						else
						{
							Type var_type =  id_node.m_type;
							pw.op(IGIR.local_clear(local_index, var_type));		
						}
					}
					
					break;
			 	}

	
				case IGNode.NODE_TYPE_TYPE: { throw new RuntimeException("what?"); }
				case IGNode.NODE_TYPE_VAR:  { throw new RuntimeException("what?"); }

	
			case IGNode.FIELD_ACCESS              : 
			{ 
				Type my_data_type = m_ctx.getType(m.m_parent_source);
				String field_name = node.m_token.m_value;

				pw.addMemberDependency(my_data_type, node.m_token.m_value);

				if (behaviour == BEHAVIOUR_READ_WRITE)
				{
					IGIROp [] diffs =  new IGIROp[2];
					diffs[0] = IGIR.this_load(my_data_type, field_name);
					diffs[1] = IGIR.dup(1).chain(IGIR.this_store(my_data_type, field_name));
				
					return diffs;
				}
				else
				{
					pw.op(IGIR.this_load(my_data_type, field_name));
				}		
				break;
			}

	

				case IGNode.STATIC_FIELD_ACCESS: 
				{ 	
					Type my_data_type = m_ctx.getType(m.m_parent_source);
					String field_name = node.m_token.m_value;
					
					pw.addMemberDependency(my_data_type, node.m_token.m_value);

					if (behaviour == BEHAVIOUR_READ_WRITE)
					{
						IGIROp [] diffs =  new IGIROp[2];
						diffs[0] = IGIR.static_load(my_data_type, field_name);
						diffs[1] = IGIR.dup(1).chain(IGIR.static_store(my_data_type, field_name));
						return diffs;
					}
					else
					{
						pw.op(IGIR.static_load(my_data_type, field_name));
					}			
					break;
				}

				// TYPE (<child0>)
				case IGNode.NODE_TYPE_CAST: 
				{ 
					Type dst_type = node.m_type;
					Type src_type = node.m_children.get(1).m_type;
					
					outputFunctionBody(m, node.m_children.get(1), pw);
					castTo(src_type, dst_type, pw, true);
				
			 		break;
			 	}

			 	case IGNode.NODE_IIF: {
			 		Type dst_type = node.m_type;

			 		IGNode left = node.m_children.get(1);
			 		IGNode right = node.m_children.get(2);

			 		outputFunctionBody(m, node.m_children.get(0), pw);

			 		castTo(left.m_type, dst_type, pw, true);
					outputFunctionBody(m, left, pw);

					castTo(right.m_type, dst_type, pw, true);
					outputFunctionBody(m, right, pw);
					
					pw.op(IGIR.iif(left.m_type, left.m_type.getTupleCount()));
					break;
			 	}

			 	case IGNode.NODE_IIF_PIECEWISE: {
					Type dst_type = node.m_type;

			 		IGNode left = node.m_children.get(1);
			 		IGNode right = node.m_children.get(2);

			 		outputFunctionBody(m, node.m_children.get(0), pw);

			 		castTo(left.m_type, dst_type, pw, true);
					outputFunctionBody(m, left, pw);

					castTo(right.m_type, dst_type, pw, true);
					outputFunctionBody(m, right, pw);
					
					pw.op(IGIR.iif_piecewise(left.m_type, left.m_type.getTupleCount()));
					break;
			 	}


				// XXXXX<child0> . ID<child1>
				case IGNode.NODE_DOT: 
				{  
					Characterization ch = new Characterization(m_ctx, Characterization.FIELD, this, m, node);
					IGNode id_node = node.m_children.get(1);
					

					Type characterization_type = ch.getType();
					String characterization_name = ch.getName();

					pw.addMemberDependency(characterization_type, characterization_name);

					if (ch.isObject()) 
					{
						outputFunctionBody(m, node.m_children.get(0), pw);
					
						// dispatch the alternatives to be used for a setter/modifier context
						if (behaviour == BEHAVIOUR_READ_WRITE) {
							IGIROp [] diffs =  new IGIROp[3];
							diffs[0] = IGIR.member_load(ch.getType(), ch.getName());
							diffs[1] = IGIR.dup1x().chain(IGIR.member_store(ch.getType(), ch.getName()));
							diffs[2] = IGIR.dup(1);
							return diffs;
						}
						else {
							pw.op(IGIR.member_load(ch.getType(), ch.getName()));
						}
					}
					else if (ch.isStatic()) 
					{
						if (behaviour == BEHAVIOUR_READ_WRITE) {
							IGIROp [] diffs =  new IGIROp[2];
							diffs[0] = IGIR.static_load(ch.getType(), ch.getName());
							diffs[1] = IGIR.dup(1).chain(IGIR.static_store(ch.getType(), ch.getName()));
							return diffs;
						}
						else {
							pw.op(IGIR.static_load(ch.getType(), ch.getName()));
						}					
					}
					break;
				}
				

				case IGNode.LOCAL_ACCESSOR:
				case IGNode.STATIC_LOCAL_ACCESSOR:
				{
				
					
				
					Characterization ch = new Characterization(m_ctx, Characterization.FIELD, this, m, node);

					IGMember the_actual_function = (IGMember)node.m_scope;

					//pw.comment("NODE_LOCAL_GET or NODE_LOCAL_SET o:" + ch.isObject() + 
					//		" s: " + ch.isStatic() + " f:" + ch.isFinal() + " e:" + ch.isEnum() + 
					//		" the_actual_function: " + the_actual_function.m_name + " " + the_actual_function);

					if (ch.isInterface())
					{
						throw new RuntimeException("Impossible case");
					}
					// should actually be called is member?
					else if (ch.isObject()) 
					{
						
					
						
						String call_type =  "vtable";				
						Type callee_type = ch.getType();
						




						if (ch.isSuper()) {
							call_type =  "super";	// this shouldn't be possible in this context
							callee_type = m_ctx.getType(m.m_parent_source);
						}
						else if (ch.isFinal())
						{
							call_type =  "static";
							if (!ch.isEnum()) {
								call_type =  "vtable";	//"static_vtable";
							}
						}

						pw.op(IGIR.local_load(callee_type, 0));
						pw.addFunctionDependency(callee_type, ch.getName() + "__get");
						pw.addFunctionDependency(callee_type, ch.getName() + "__set");
							
						if (behaviour == 1) 
						{
							IGIROp [] diffs =  new IGIROp[3];							
							if ("static".equals(call_type))
							{
								diffs[0] = IGIR.call_ret1_static(callee_type, ch.getName() + "__get",1);
								diffs[1] = IGIR.dup1x().chain(IGIR.call_ret1_static(callee_type,ch.getName() + "__set", 2));
							}
							else if ("vtable".equals(call_type)) {
								diffs[0] = IGIR.call_ret1_vtable(callee_type, ch.getName() + "__get",1);
								diffs[1] = IGIR.dup1x().chain(IGIR.call_ret1_vtable(callee_type,ch.getName() + "__set", 2));
							}
							else if ("super".equals(call_type)) {
								diffs[0] = IGIR.call_ret1_super(callee_type, ch.getName() + "__get",1);
								diffs[1] = IGIR.dup1x().chain(IGIR.call_ret1_super(callee_type,ch.getName() + "__set", 2));
							}
							diffs[2] = IGIR.dup(1);
							return diffs;
						}
						else 
						{
							if ("static".equals(call_type)) {
								pw.op(IGIR.call_ret1_static(callee_type, ch.getName() + "__get", 1));
							}
							else if ("vtable".equals(call_type)) {
								pw.op(IGIR.call_ret1_static(callee_type, ch.getName() + "__get", 1));
							}
							else if ("super".equals(call_type)) {
								pw.op(IGIR.call_ret1_super(callee_type, ch.getName() + "__get", 1));
							}
						}
					}
					else if (ch.isStatic()) 
					{
					
						//String callee_type = ch.getType();
						//if (call_type.equals("static_vtable"))
						//{
						//if (ch.getFunction().m_parent_source != ch.getObjectType().m_scope) {
						//		}
						//}

						Type callee_type = m_ctx.resolveType(
							ch.getFunction().m_parent_source.getInstanceDataType());
					

						pw.addFunctionDependency(callee_type, ch.getName() + "__get");
						pw.addFunctionDependency(callee_type, ch.getName() + "__set");

						if (behaviour == 1) {
							IGIROp [] diffs =  new IGIROp[2];
							diffs[0] = IGIR.call_ret1_static(callee_type, ch.getName() + "__get", 0);
							diffs[1] = IGIR.dup(1).chain(IGIR.call_static(callee_type, ch.getName() + "__set", 1));
							return diffs;
						}
						else {
							IGIR.call_ret1_static(callee_type, ch.getName() + "__get", 0);
						}					
					}
					break;
				}
				

				//case IGNode.NODE_DOT_GET:
				//case IGNode.NODE_DOT_SET: 
				
				case IGNode.STATIC_DOT_ACCESSOR:
				case IGNode.DOT_ACCESSOR:
				{ 
					//pw.comment("NODE_DOT_GET or NODE_DOT_SET");
					Characterization ch = new Characterization(m_ctx, Characterization.FIELD, this, m, node);
					IGNode id_node = node.m_children.get(1);
					
					if (ch.isInterface())
					{
						outputFunctionBody(m, node.m_children.get(0), pw);
					
						Type field_type = ch.getType();

						pw.addFunctionDependency(field_type, ch.getName() + "__get");
						pw.addFunctionDependency(field_type, ch.getName() + "__set");


						if (behaviour == 1) {
							IGIROp [] diffs =  new IGIROp[3];
							diffs[0] = IGIR.call_ret1_interface(field_type, ch.getName() + "__get", 1);
							diffs[1] = IGIR.dup1x().chain(IGIR.call_interface(field_type, ch.getName() + "__set", 2));
							diffs[2] = IGIR.dup(1);
							return diffs;
						}
						else {
							pw.op(IGIR.call_ret1_interface(ch.getType(), ch.getName()+ "__get", 1));
						}
					}
					else if (ch.isObject()) 
					{
						outputFunctionBody(m, node.m_children.get(0), pw);
						
						final Type   object_type = ch.getType();
						final String method_name = ch.getName();

						if (object_type.isArray() && method_name.equals("length")) {
						
							if (behaviour == 1) {
								throw new RuntimeException("error");
							}
							else
							{
								pw.op(IGIR.array_length(object_type, object_type.m_left));
							}
						}
						else
						{	
							boolean can_inline = false;						

							String call_type = "vtable";	
							Type callee_type = ch.getObjectType();
							

							if (ch.isSuper()) 
							{
								call_type =  "super";
								callee_type = m_ctx.getType(m.m_parent_source);	
							}
							else if (ch.isFinal())
							{
								call_type = "static";
								// does this annotation even matter?
								if (!ch.isEnum()) {
									call_type =  "vtable";
								}
								can_inline = true;
							}

							pw.addFunctionDependency(callee_type, ch.getName() + "__get");
							pw.addFunctionDependency(callee_type, ch.getName() + "__set");

							if (behaviour == 1) 
							{
								IGIROp [] diffs =  new IGIROp[3];							
								if ("static".equals(call_type))
								{
									diffs[0] = IGIR.call_ret1_static(callee_type, ch.getName() + "__get",1);
									diffs[1] = IGIR.dup1x().chain(IGIR.call_ret1_static(callee_type,ch.getName() + "__set", 2));
								}
								else if ("vtable".equals(call_type)) {
									diffs[0] = IGIR.call_ret1_vtable(callee_type, ch.getName() + "__get",1);
									diffs[1] = IGIR.dup1x().chain(IGIR.call_ret1_vtable(callee_type,ch.getName() + "__set", 2));
								}
								else if ("super".equals(call_type)) {
									diffs[0] = IGIR.call_ret1_super(callee_type, ch.getName() + "__get",1);
									diffs[1] = IGIR.dup1x().chain(IGIR.call_ret1_super(callee_type,ch.getName() + "__set", 2));
								}
								diffs[2] = IGIR.dup(1);
								return diffs;
							}
							else 
							{
								if ("static".equals(call_type)) {
									pw.op(IGIR.call_ret1_static(callee_type, ch.getName() + "__get", 1));
								}
								else if ("vtable".equals(call_type)) {
									pw.op(IGIR.call_ret1_static(callee_type, ch.getName() + "__get", 1));
								}
								else if ("super".equals(call_type)) {
									pw.op(IGIR.call_ret1_super(callee_type, ch.getName() + "__get", 1));
								}
							}
						}
					}
					else if (ch.isStatic()) 
					{
						pw.addFunctionDependency(ch.getType(), ch.getName() + "__get");
						pw.addFunctionDependency(ch.getType(), ch.getName() + "__set");

						if (behaviour == 1) {
							IGIROp [] diffs =  new IGIROp[2];
							diffs[0] = IGIR.call_ret1_static(ch.getType(), ch.getName() + "__get", 0);
							diffs[1] = IGIR.dup(1).chain(IGIR.call_static(ch.getType(),ch.getName() + "__set",1));
							return diffs;
						}
						else {
							pw.op(IGIR.call_ret1_static(ch.getType(), ch.getName() + "__get", 0));
						}					
					}
					break;
				}

				// XXXXX '[' index ']'
				// an array or map will always be on the right side
				case IGNode.NODE_INDEX: 
				{ 
					outputFunctionBody(m, node.m_children.get(0), pw);
					
					outputFunctionBody(m, node.m_children.get(1), pw);
					//castTo(left_type,  cast_type, pw, explicit);
				
					Type t = m_ctx.resolveType(node.m_children.get(0).m_type);
				
					if (t.isArray()) 
					{
						final int storage_class = t.m_left.getStorageClass();
						
						if (behaviour == 1) {
							IGIROp [] toks = new IGIROp[3];
							toks[0] = IGIR.array_load(t, t.m_left);
							toks[1] = IGIR.dup2x().chain(IGIR.array_store(t,t.m_left));
							toks[2] = IGIR.dup2();
							return toks;
						}
						else {
							pw.op(IGIR.array_load(t, t.m_left));
						}
					}
					else
					{
						IGSource source = (IGSource)t.m_scope;

						pw.addFunctionDependency(t, "<get[]>");
						pw.addFunctionDependency(t, "<set[]>");

						if (behaviour == 1) 
						{
							IGMember getter = source.getMember("<get[]>");
							IGMember setter = source.getMember("<set[]>");

							IGIROp [] toks = new IGIROp[3];
							toks[0] = IGIR.call_ret1_vtable(t, "<get[]>", 2);
							toks[1] = IGIR.dup2x().chain(IGIR.call_vtable(t, "<set[]>", 3));
							toks[2] = IGIR.dup2();
							return toks;
						}
						else 
						{
							IGMember getter = source.getMember("<get[]>");
							pw.op(IGIR.call_ret1_vtable(t, "<get[]>", 2));
						}
					}
				
				 	break;
				}

				// callee '(' call_args ')'
				// this is all black magic and needs to be refactored
				////////////////////////////
				case IGNode.NODE_CALL: 
				{ 
					IGNode callee    = node.m_children.get(0);
					IGNode call_args = node.m_children.get(1);
					
					// calculate the true number of arguments
					// this includes tuples
					int num_args = 0;
					{
						for (int ai = 0; ai < call_args.m_children.size(); ai++) {
							Type arg_type = call_args.m_children.get(ai).m_type;
							if (!arg_type.isTuple()) {
								num_args++;
								continue;
							}

							num_args += arg_type.getTupleTypes().length;
						}
					}


					boolean returns_value = (node.m_type != Type.VOID);
	
	
					//node.m_token.printError("check: ");
			
					Characterization ch = new Characterization(m_ctx, Characterization.CALL, this, m, callee);
					
					IGNode to_process = ch.getProcessingNode();
					if (to_process != null) {
						outputFunctionBody(m, to_process, pw);
					}
					
					if (ch.needsThisPush()) {
						pw.op(IGIR.local_load(ch.getType(), 0));
					}
					


					if (ch.isFunctionPointer()) 
					{
						outputFunctionBody(m, call_args, pw);	
						if (returns_value) {
							pw.op(IGIR.call_ret1_dynamic(Type.VOID, ch.getName(), num_args));
						}	
						else {
							pw.op(IGIR.call_dynamic(Type.VOID, ch.getName(), num_args));
						}
					}
					else if (ch.isStatic())
					{
						pw.addFunctionDependency(ch.getType(), ch.getName());

						// umm.. wtf statics actually need to refer to their container.. not the class its being called on
						outputFunctionBody(m, call_args, pw);

						if (returns_value) {
							pw.op(IGIR.call_ret1_static(ch.getType(), ch.getName(), num_args));
						}	
						else {
							pw.op(IGIR.call_static(ch.getType(), ch.getName(), num_args));
						}
					}
					else if (ch.isInterface())
					{
						outputFunctionBody(m, call_args, pw);
						if (returns_value) {
							pw.op(IGIR.call_ret1_interface(ch.getType(), ch.getName(), num_args + 1));
						}	
						else {
							pw.op(IGIR.call_interface(ch.getType(), ch.getName(), num_args + 1));
						}
					}
					else if (ch.isObject())
					{
						outputFunctionBody(m, call_args, pw);	
						
						if (ch.isSuper()) {
							ch.setType(m_ctx.getType(m.m_parent_source));
							
							String fn_name = ch.getName();
							

							pw.addFunctionDependency(ch.getType(), fn_name);
							
							//BAD CONSTRUCTOR SET UP
							if ("<constructor>".equals(fn_name)) {
								// constructors DO NOT RETURN VALUES
								pw.op(IGIR.call_super(ch.getType(), fn_name, num_args + 1));	
								// this is to deal with the pop that'll inevitably follow the super call
								pw.op(IGIR.local_load(ch.getType(), 0));
							}
							else {
								if (returns_value) {
									pw.op(IGIR.call_ret1_super(ch.getType(), ch.getName(), num_args + 1));
								}	
								else {
									pw.op(IGIR.call_super(ch.getType(), ch.getName(), num_args + 1));
								}
							}
						}
						else 
						if (ch.isFinal())
						{
							Type   callee_type = ch.getType();
							final  String callee_name = ch.getName();

							if (!ch.isEnum()) 
							{		
								if ("resize".equals(callee_name)    && callee_type.isArray()) {
									// should this be callee_type??
									pw.op(IGIR.array_resize(callee_type,callee_type.m_left));
								}
								else if ("fill".equals(callee_name) && callee_type.isArray()) {
									// should this be callee type??
									pw.op(IGIR.array_fill(callee_type,callee_type.m_left));
								}
								else if ("copy".equals(callee_name) && callee_type.isArray()) {
									// should this be callee type??
									pw.op(IGIR.array_copy(callee_type,callee_type.m_left));
								}
								else
								{		

									pw.addFunctionDependency(ch.getType(), callee_name);
									
									if (returns_value) {
										pw.op(IGIR.call_ret1_vtable(ch.getType(), ch.getName(), num_args + 1));
									}	
									else {
										pw.op(IGIR.call_vtable(ch.getType(), ch.getName(), num_args + 1));
									}

									//pw.op_call(call_prefix + "static_vtable", num_args + 1, callee_type, callee_name);				
								}
							}
							else
							{	
								/// TODO VERIFY THAT THE FUNCTIONS IN QUESTION ARE SUBS
								if ("equals".equals(callee_name) && callee_type.isIntishEnum() && ((IGSource)callee_type.m_scope).canOptimizeMemberFunctionOut("equals")) {
									pw.op(IGIR.cmp_eq_i32());
								}
								else if ("hashCode".equals(callee_name) && callee_type.isIntishEnum() && ((IGSource)callee_type.m_scope).canOptimizeMemberFunctionOut("hashCode")) {
									// do nothing.. there's no work to be done here
								}
								else
								{

									pw.addFunctionDependency(ch.getType(), callee_name);

									if (returns_value) {
										pw.op(IGIR.call_ret1_static(ch.getType(), ch.getName(), num_args + 1));
									}	
									else {
										pw.op(IGIR.call_static(ch.getType(), ch.getName(), num_args + 1));
									}

									//pw.op_call(call_prefix + "static", num_args + 1, ch.getType(), callee_name);				
								}
							}
							
						}
						else
						{
							pw.addFunctionDependency(ch.getType(), ch.getName());

							if (returns_value) {
								pw.op(IGIR.call_ret1_vtable(ch.getType(), ch.getName(), num_args + 1));
							}	
							else {
								pw.op(IGIR.call_vtable(ch.getType(), ch.getName(), num_args + 1));
							}
						//	pw.op_call(call_prefix + "vtable", num_args + 1, ch.getType(), ch.getName());
						}
					}
					
					
			 		break;
			 	}

				// '(' EXPRESSION ')'
				case IGNode.NODE_TYPE_BRACKETED_EXPRESSION		: {   

					final IGNodeList children = node.m_children;
					final int        children_size = children.size();
					
					if (behaviour == BEHAVIOUR_READ_WRITE)
					{
						if (children_size == 1) {
							IGIROp [] diffs = outputFunctionBody(m, children.get(0), pw, 1);
							return diffs;
						}

						boolean possible = true;

						for (int c = 0; c < children_size; c++) {
							int ct = children.get(c).m_node_type;
							if (ct != IGNode.FIELD_ACCESS &&
								ct != IGNode.STATIC_FIELD_ACCESS) {
								possible = false;
							}	
						}

						if (possible) {
							IGIROp [] diffs = new IGIROp[3];
							diffs[0] = IGIR.nop();
							diffs[1] = IGIR.dup(children_size);
							diffs[2] = IGIR.pop(children_size);

							Type my_data_type = m_ctx.getType(m.m_parent_source);

							

							for (int c = 0; c < children_size; c++) 
							{
								IGNode n0       = children.get(c);
								int    n0_type  = n0.m_node_type;
								String f0_name = n0.m_token.m_value;

								if (n0_type == IGNode.FIELD_ACCESS) {
									diffs[0] = diffs[0].chain(IGIR.this_load (my_data_type, f0_name));
								}
								else if (n0_type == IGNode.STATIC_FIELD_ACCESS) {
									diffs[0] = diffs[0].chain(IGIR.static_load (my_data_type, f0_name));	
								}

								IGNode n1        = children.get(children_size - 1 - c);
								int    n1_type  = n1.m_node_type;
								String f1_name  = n1.m_token.m_value;


								if (n1_type == IGNode.FIELD_ACCESS) {
									diffs[1] = diffs[1].chain(IGIR.this_store (my_data_type, f1_name));
								}
								else if (n1_type == IGNode.STATIC_FIELD_ACCESS) {
									diffs[1] = diffs[1].chain(IGIR.static_store (my_data_type, f1_name));	
								}

							
							}

							return diffs;
						}

						//System.err.println("WHAT TO DO: " + node);
						node.debug();
						throw new RuntimeException("NYI");
					}
					else
					{
						for (int i = 0; i < children_size; i++) {
							outputFunctionBody(m, children.get(i), pw);
						}
					}	
					break;
				}

				case IGNode.NODE_NEW: 
				{ 
					IGNode new_type = node.m_children.get(0);
					IGNode new_args = node.m_children.get(1);
					
					Type new_t = m_ctx.resolveType(new_type.m_type);
					
					if (new_t.isArray()) 
					{
						IGNode args = new_args.m_children.get(0);
						outputFunctionBody(m, args, pw);
						
						pw.op(IGIR.array_new(new_t,new_t.m_left));
					}
					else
					{
						int arg_count = 0;
					
						pw.op(IGIR._new(new_t));
						pw.op(IGIR.dup(1));
					
						// note sure exactly why I created a NEW_CALL node type
						if (new_args.m_node_type != IGNode.NEW_CALL) {
							throw new RuntimeException("ex");
						}	
						else
						{
							if (new_args.m_children.size() > 0) 
							{
								IGNode sub = new_args.m_children.get(0);
								arg_count = sub.m_children.size();
								outputFunctionBody(m, sub, pw);
							}
						}
					
						// wouldn't this imply that all MUST have constructors
						pw.addFunctionDependency(new_t, "<constructor>");
						pw.op(IGIR.call_static(new_t, "<constructor>", arg_count + 1));
					}
					break;
				}

	
	
				// The following 3 are used when there is no specific 
				// operator mentioned
				/////////////////////////////////////////////////////////////
	
				case IGNode.NODE_UNARY						: 
				{ 
					//pw.println("#unary " + node.m_token.m_value);
					Type type = node.m_type;

					final IGNode child0 = node.m_children.get(0);

					if (node.m_token.m_type == TOK_SUB) 
					{
						if (child0.m_node_type == IGNode.NODE_VALUE)
						{
							Token t = child0.m_token;
							
							// this feels like an early optimization
							// but for int min values it really isn't
							////////////////////////////////////////
							if (t.m_type == IGLexer.LEXER_INT) {
								pw.op(IGIR.push_i32(Integer.parseInt("-" + t.m_value)));
							}
							else if (t.m_type == IGLexer.LEXER_FLOAT) {
								pw.op(IGIR.push_f64(util.Encoding.parseDouble("-" +t.m_value)));
							}
							else if (t.m_type == IGLexer.LEXER_HEX) {
								long value = Long.decode(t.m_value);
								pw.op(IGIR.push_i32((int)-value));
							}
							else if (t.m_type == IGLexer.LEXER_CHAR) {
								pw.op(IGIR.push_i32(-util.Decoder.escapedCharToInt(t.m_value)));
							}
							else
							{
								throw new RuntimeException("Unknown unary optimization");
							}
						}
						else
						{
							if (type == Type.INT) {
								pw.op(IGIR.push_i32(0));
								outputFunctionBody(m, child0, pw);
								pw.op(IGIR.sub_i32());
							}
							else if (type == Type.DOUBLE) {
								pw.op(IGIR.push_f64(0));
								outputFunctionBody(m, child0, pw);
								pw.op(IGIR.sub_f64());
							}
							else {
								node.m_token.printError("huh?");
								throw new RuntimeException("Unknown case: " + type);
							}
						}
					}
					else if (node.m_token.m_type == TOK_ADD) {
						// nothing needs be done
						outputFunctionBody(m, child0, pw);
					}
					else if (node.m_token.m_type == TOK_NOT) {
						outputFunctionBody(m, child0, pw);
						pw.op(IGIR.not());
					}
					else if (node.m_token.m_type == TOK_BIT_NOT) {
						outputFunctionBody(m, child0, pw);
						pw.op(IGIR.not_i32());
					}
					else if (node .m_token.m_type == TOK_MATH_ABS) 
					{
						if (!type.isTupleDoubles()) { 
							throw new RuntimeException("Abs only works on doubles");
						}
						outputFunctionBody(m, child0, pw);
						
						pw.op(IGIR.abs_f64_vec(type.getTupleCount()));
						
					}
					else if (node .m_token.m_type == TOK_MATH_LENGTH) 
					{
						if (!child0.m_type.isTupleDoubles()) { 
							throw new RuntimeException("Length only works on doubles");
						}
						outputFunctionBody(m, child0, pw);
						
						final int tupple_count = child0.m_type.getTupleCount();
						if (tupple_count == 1) {
							pw.op(IGIR.abs_f64_vec(tupple_count));
						}
						else {
							pw.op(IGIR.length_f64_vec(tupple_count));			
						}
					}
					else {
						throw new RuntimeException("Unknown case");
					}
					// this isn't fully implemented either
					break;
				}


				case IGNode.INCREMENT:
				{
					//pw.comment("increment " + node.m_token.m_value);
					
					boolean optimized_increment = false;
					IGNode storage_node = node.m_children.get(0);
					Type t = node.m_type;
					
						
					if (t == Type.INT) // || t == Type.UINT) 
					{
						if (storage_node.m_node_type == IGNode.LOCAL_VARIABLE_ACCESS && 
							!"super".equals(storage_node.m_token.m_value))
						{
							int load_index  = getLocalIndex(m, storage_node.m_token, false);
							int store_index = getLocalIndex(m, storage_node.m_token, true);
							if (load_index == store_index)
							{
								if (node.m_token.m_type == TOK_INC) {
									pw.op(IGIR.local_inc_i32(load_index, 1));
								}
								else if (node.m_token.m_type == TOK_DEC) {
									pw.op(IGIR.local_inc_i32(load_index, -1));
								}	

								optimized_increment = true;
							}
						}
					}
				
					if (!optimized_increment)
					{
						IGIROp [] diffs = outputFunctionBody(m, node.m_children.get(0), pw, 1);
							

						if (diffs.length > 2) {
							pw.op(diffs[2]); // this should also have the number of dup values
						}
					
						// load the current value
						pw.op(diffs[0]); 
						
						
						
					
						if (t == Type.DOUBLE) 
						{
							if (node.m_token.m_type == TOK_INC) {
								pw.op(IGIR.stack_inc_f64(1));
							}
							else if (node.m_token.m_type == TOK_DEC) {
								pw.op(IGIR.stack_inc_f64(-1));
							}	
						}
						else if (t == Type.INT) 
						{
							if (node.m_token.m_type == TOK_INC) {
								pw.op(IGIR.stack_inc_i32(1));
							}
							else if (node.m_token.m_type == TOK_DEC) {
								pw.op(IGIR.stack_inc_i32(-1));
							}						
						}
						else {
							throw new RuntimeException("Invalid increment");
						}
				
						// write back the value	
						pw.op(diffs[1]);
						pw.op(IGIR.pop(1));
					}
					break;
				}

				case IGNode.NODE_TUPLE_BINARY:
				{
					final Token  op = node.m_token;
					final IGNode left  = node.m_children.get(0);
					final IGNode right = node.m_children.get(1);

					final int count = left.m_type.getTupleCount();
					
					outputFunctionBody(m, left, pw);
					outputFunctionBody(m, right, pw);

					IGIROp result = null;
					switch(op.m_type) 
					{	
						case TOK_ADD: {
							result = IGIR.add_f64_vec(count); 
							break;
						}
						case TOK_SUB: {
							result = IGIR.sub_f64_vec(count); 
							break;
						}
						case TOK_MUL: {
							result = IGIR.mul_f64_vec(count);
							break;
						}
						case TOK_DIV: {
							result = IGIR.div_f64_vec(count);
							break;
						}
						default: {
							throw new RuntimeException("Unhandled vectoriztion path");
						}
					}
					pw.op(result);

					break;
				}

				case IGNode.NODE_TUPLE_BINARY_SINGLE:
				{
					Token  op    = node.m_token;
					IGNode left  = node.m_children.get(0);
					IGNode right = node.m_children.get(1);

					int count = left.m_type.getTupleCount();
					
					switch(op.m_type) 
					{	
						case TOK_MUL:
						{
							outputFunctionBody(m, left, pw);
							outputFunctionBody(m, right, pw);

							pw.op(IGIR.mul_f64_vec_val(count)); 
							break;
						}
					}

					break;
				}


				case IGNode.NODE_BINARY						: 
				{ 
					//pw.println("#binary " + node.m_token.m_value);
					
					Token  op = node.m_token;
					IGNode left  = node.m_children.get(0);
					IGNode right = node.m_children.get(1);
					
					IGIROp result = null;
					//String mid_result = null;
					
					
					Type node_type = node.m_type;
					boolean node_is_int    = (node_type == Type.INT);// ||  (node_type == Type.UINT);
					boolean node_is_bool   = node_type == Type.BOOL;
					boolean node_is_double = node_type == Type.DOUBLE;
					
					if (node_type.isEnum()) {
						Type prim = node_type.getEnumPrimitive();
						
						node_is_int    = (prim == Type.INT);// ||  (prim == Type.UINT);
						node_is_bool   = prim == Type.BOOL;
						node_is_double = prim == Type.DOUBLE;
					}
					
					boolean params_are_doubleish = false;
					boolean params_are_intish    = false;
					boolean string_comparison_possible = false;
					
					if (left.m_type == Type.STRING && right.m_type != Type.NULL) {
						string_comparison_possible = true;
					}
					else if (right.m_type == Type.STRING  && left.m_type != Type.NULL) {
						string_comparison_possible = true;
					}
					
					
					if (left.m_type.getEnumPrimitive() == Type.DOUBLE ||
						left.m_type  == Type.DOUBLE || 
						right.m_type == Type.DOUBLE ||
						right.m_type.getEnumPrimitive() == Type.DOUBLE) 
					{
						params_are_doubleish = true;
					}
					else if (
						left.m_type.getEnumPrimitive() == Type.INT  ||
						left.m_type.getEnumPrimitive() == Type.BOOL  ||
						left.m_type == Type.INT || 
						left.m_type == Type.BOOL ||
						left.m_type == Type.SHORT ||
						left.m_type == Type.BYTE ||

						right.m_type.getEnumPrimitive() == Type.INT  ||
						right.m_type.getEnumPrimitive() == Type.BOOL  ||
						right.m_type == Type.INT || 
						right.m_type == Type.BOOL ||
						right.m_type == Type.SHORT ||
						right.m_type == Type.BYTE)
					{
						params_are_intish = true;
					}
					
					
					Type left_type = left.m_type;
					Type right_type = right.m_type;
					
					Type cast_type = null;
					
					
					
					if (op.m_type == TOK_ASSIGN)
					{
						IGIROp [] diffs = outputFunctionBody(m, left, pw, 1);
						outputCast(m, right, left_type, pw);
						pw.op(diffs[1]);
					}
					else if (op.m_type == TOK_ADD_ASSIGN ||
							 op.m_type == TOK_SUB_ASSIGN ||
							 op.m_type == TOK_MUL_ASSIGN ||
							 op.m_type == TOK_DIV_ASSIGN ||
							 op.m_type == TOK_MOD_ASSIGN || 
							 op.m_type == TOK_XOR_ASSIGN || 
							 op.m_type == TOK_OR_ASSIGN || 
							 op.m_type == TOK_AND_ASSIGN)
					{
						//if (left_count 
						IGIROp [] diffs = outputFunctionBody(m, left, pw, 1);
						
						// to any load/store busy work
						if (diffs.length > 2) {
							pw.op(diffs[2]);
						}
						
						// load the current value
						pw.op(diffs[0]);
			
						// The type we need to cast both sides to before proceeding
						Type intermediate_type = null;
						
						// the result always has to be turned back into the same type as the left side
						cast_type = left_type;
						
						// String +=
						if (node_type == Type.STRING)
						{
							if (op.m_type == TOK_ADD_ASSIGN) 
							{
								pw.addFunctionDependency(Type.STRING, "append");
								intermediate_type = Type.STRING;
								result = IGIR.call_ret1_static(Type.STRING, "append", 2);
								
								// is this even necessary?
								cast_type = Type.STRING;
							}
							else {
								throw new RuntimeException("Invalid");
							}
						}
						// left or right hand side is a double
						else if (params_are_doubleish) {
							intermediate_type = Type.DOUBLE;
							
							switch(op.m_type) {
								case TOK_ADD_ASSIGN: result = IGIR.add_f64(); break;
								case TOK_SUB_ASSIGN: result = IGIR.sub_f64(); break;
								case TOK_MUL_ASSIGN: result = IGIR.mul_f64(); break;
								case TOK_DIV_ASSIGN: result = IGIR.div_f64(); break;
								case TOK_MOD_ASSIGN: result = IGIR.mod_f64(); break;
							}

						}
						// left and right hand sides are either enums, ints or uints
						else if (params_are_intish) {
							intermediate_type = Type.INT;
							
							switch(op.m_type) {
								case TOK_ADD_ASSIGN: result = IGIR.add_i32(); break;
								case TOK_SUB_ASSIGN: result = IGIR.sub_i32(); break;
								case TOK_MUL_ASSIGN: result = IGIR.mul_i32(); break;
								case TOK_DIV_ASSIGN: 	
									// why am I maintaining the same division for floats as ints?
									result = IGIR.div_f64(); 
									intermediate_type = Type.DOUBLE; 
									break;
								case TOK_MOD_ASSIGN: result = IGIR.mod_i32(); break;
								case TOK_XOR_ASSIGN: result = IGIR.xor_i32(); break;
								case TOK_OR_ASSIGN: result = IGIR.or_i32(); break;
								case TOK_AND_ASSIGN: result =IGIR.and_i32(); break;
							}
						}
						
						if (result == null || intermediate_type == null) {
							op.printError("Invalid assignment");
							throw new RuntimeException("Invalid assignment");
						}
			
						castTo(left_type, intermediate_type, pw);
			
						outputFunctionBody(m, right, pw);
						castTo(right_type, intermediate_type, pw);			
						
						pw.op(result);
						
						// cast back to the storage type (is this 100% correct?)
						castTo(intermediate_type, cast_type, pw);			
						
						pw.op(diffs[1]);				
					}
					else if (op.m_type == TOK_AND) {
					
						// a && b
						//     eval(a)
						//     if FALSE goto B1:
						//     eval(b)
						// B1: nop
					
						String term = getLabel("A");
						outputFunctionBody(m, left, pw);
		
						pw.op(IGIR.and(term));
						outputFunctionBody(m, right, pw);	
						pw.label(term);	
					}
					// current grouping of A || B || C is (A || B) || C
					// what we really want is  A || (B || C)
					// see UIWidget hitTEst with point ~2291
					else if (op.m_type == TOK_OR) 
					{
						// a || b
						//     eval(a)
						//     if TRUE goto B1:
						//     eval(b)
						// B1: nop
						String term = getLabel("O");
						outputFunctionBody(m, left, pw);
		
						pw.op(IGIR.or(term));
						outputFunctionBody(m, right, pw);	
						pw.label(term);			
					}
					else
					{
						boolean explicit = false;

						if (op.m_type == TOK_LSL && node_is_int) {
							result = (IGIR.lsl_i32());
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_LSR && node_is_int) {
							result = (IGIR.lsr_i32());
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_ASR && node_is_int) {
							result = (IGIR.asr_i32());
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_ADD && node_is_int) {
							result = (IGIR.add_i32());
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_SUB && node_is_int) {
							result = (IGIR.sub_i32());
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_MUL && node_is_int) {
							result = (IGIR.mul_i32());
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_POW && node_is_int) {
							result = IGIR.pow_i32();
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_MOD && node_is_int) {
							result = (IGIR.mod_i32());
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_BIT_AND && node_is_int) {
							result = (IGIR.and_i32());
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_BIT_OR && node_is_int) {
							result = (IGIR.or_i32());
							cast_type = Type.INT;
						}
						else if (op.m_type == TOK_BIT_XOR && node_is_int) {
							result = (IGIR.xor_i32());
							cast_type = Type.INT;
						}
						// not a boolean... what was I thinking
						//else if (op.m_type == TOK_BIT_NOT && node_is_int) {
						//	result = "bit_not_i32";
						//	cast_type = Type.INT;
						//}
						else if (op.m_type == TOK_EQ && params_are_intish) {
							result = (IGIR.cmp_eq_i32());
							//cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_NE && params_are_intish) {
							result = IGIR.cmp_ne_i32();							
							//cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_LT && params_are_intish) {
							result = (IGIR.cmp_lt_i32());							
							//cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_LE && params_are_intish) {
							result = (IGIR.cmp_le_i32());							
							//cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_GT && params_are_intish) {
							result = (IGIR.cmp_le_i32()).chain(IGIR.not());						
						}
						else if (op.m_type == TOK_GE && params_are_intish) {
							result = (IGIR.cmp_lt_i32()).chain(IGIR.not());	
						}					
						
						// what about bit not
						
						else if (op.m_type == TOK_ADD && node_is_double) {
							result = (IGIR.add_f64());
							cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_SUB && node_is_double) {
							result = (IGIR.sub_f64());
							cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_POW && node_is_double) {
							result = (IGIR.pow_f64());
							cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_MUL && node_is_double) {
							result = (IGIR.mul_f64());
							cast_type = Type.DOUBLE;
						}
						// all divisions are promoted to float division
						else if (op.m_type == TOK_DIV) {				// && node_is_double
							if (node.m_type == Type.DOUBLE) {
								result = (IGIR.div_f64());
								cast_type = Type.DOUBLE;
							}
							else {
								result = (IGIR.div_i32());
								cast_type = Type.INT;
							}
						}
						else if (op.m_type == TOK_MOD && node_is_double) {
							result = (IGIR.mod_f64());
							cast_type = Type.DOUBLE;
						}
						
						
						else if (op.m_type == TOK_EQ && params_are_doubleish) {
							result = (IGIR.cmp_eq_f64());
							cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_NE && params_are_doubleish) {
							result = (IGIR.cmp_eq_f64()).chain(IGIR.not());								
							cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_LT && params_are_doubleish) {
							result = (IGIR.cmp_lt_f64());							
							cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_LE && params_are_doubleish) {
							result = (IGIR.cmp_le_f64());							
							cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_GT && params_are_doubleish) {
							result = (IGIR.cmp_le_f64()).chain(IGIR.not());								
							cast_type = Type.DOUBLE;
						}
						else if (op.m_type == TOK_GE && params_are_doubleish) {
							result = (IGIR.cmp_lt_f64()).chain(IGIR.not());								
							cast_type = Type.DOUBLE;
						}
						
						///////////////
						// String concatenation and equality
						///////////////
						else if (op.m_type == TOK_ADD && node_type == Type.STRING) {
							return processStringBuilder(m, node, pw, behaviour);
						}
						else if (op.m_type == TOK_EQ && string_comparison_possible) {

							pw.addFunctionDependency(Type.STRING, "__ig_equals");
							result    = IGIR.call_ret1_static(Type.STRING, "__ig_equals", 2);
							cast_type = Type.STRING;
							explicit  = true;
						}
						else if (op.m_type == TOK_NE && string_comparison_possible) {
							pw.addFunctionDependency(Type.STRING, "__ig_equals");
							result    = IGIR.call_ret1_static(Type.STRING, "__ig_equals", 2).chain(IGIR.not());
							cast_type = Type.STRING;
							
							explicit = true;
						}
						// default catch all for object equality
						else if (op.m_type == TOK_EQ || op.m_type == TOK_NE) {
							// what about function pointers really?
							if (left_type.isFunctionPointer() || 
								right_type.isFunctionPointer()) 
							{
								result = (IGIR.cmp_eq_fp());
								
								// ensure that the appropriate casting occurs
								if (left_type == Type.NULL) { cast_type = right_type; }
								if (right_type == Type.NULL) { cast_type = left_type; }
							}
							else
							{
								result = (IGIR.cmp_eq_obj());
							}
							
							if (op.m_type == TOK_NE) {
								result = result.chain(IGIR.not());  // note the chains are on purpose in reverse order
							}
						}
						else if (op.m_type == TOK_MATH_CROSS) {
							int tc = left_type.getTupleCount();
							result = IGIR.cross_f64_vec();
						}
						else if (op.m_type == TOK_MATH_DOT) {
							int tc = left_type.getTupleCount();
							result = IGIR.dot_f64_vec(tc);
						}
						else 
						{
							throw new RuntimeException("missed case: " + node.m_token.m_value + " " +
								node.m_type + " <= " + left.m_type + " " + right.m_type);
						}
					
						try
						{
							outputFunctionBody(m, left, pw);
							castTo(left_type,  cast_type, pw, explicit);
										
							outputFunctionBody(m, right, pw);
							castTo(right_type, cast_type, pw, explicit);					
						}
						catch (RuntimeException ex) {
							op.printError("Weird token");
							throw ex;
						}			
						
						if (result == null) {
							throw new RuntimeException("What gives?");
						}
						
						pw.op(result);
						
					}

					
					
				
					break;
				}

				case IGNode.NODE_TRINARY					: 
				{
					final String l1    = getLabel();
					final String lexit = getLabel();
					
					// calculate condition
					outputFunctionBody(m, node.m_children.get(0), pw);
					pw.op(IGIR.j0_i32(l1));

					//true case
					outputFunctionBody(m, node.m_children.get(1), pw);
					pw.op(IGIR.jmp(lexit));
					pw.label(l1);
					
					//false case
					outputFunctionBody(m, node.m_children.get(2), pw);
					pw.label(lexit);
					
			 		break;
			 	}
			 	
			 	/*
			 	case IGNode.NATIVE_BLOCK: {
			 		// do nothing.. this might be used for something else at a later point

					if (node.m_token.m_value.startsWith("@asm-comment")) {
						pw.comment(node.m_token.m_value.substring("@asm-comment".length()));
					}
					else if (node.m_token.m_value.equals("@asm-debug-block-start")) {
						pw.op(IGIR.debug_block_start);
					}
					else if (node.m_token.m_value.equals("@asm-debug-block-end")) {
						pw.op(IGIR.debug_block_end);
					}
			 		break;
			 	}
				*/

				case IGNode.THROW: 
				{
					outputFunctionBody(m, node.m_children.get(0), pw);
					Type t = node.m_children.get(0).m_type;
					
					pw.op(IGIR._throw(t));
					break;
				}
				
				case IGNode.WHILE: {
				
					IGNode  c1 = node.m_children.get(0);
					IGNode block = node.m_children.get(1);
					
					
					final String lblHead = getLabel();
					final String lblTail = getLabel();
					final String lblContinue = lblHead;
					
					pushContinueBreak(lblContinue, lblTail);
					{
						// initializer
						pw.label(lblHead);			// target for "continue"
					
						// check to see if the loop should progress
						outputFunctionBody(m, c1, pw);
						pw.op(IGIR.j0_i32(lblTail));
					
						// perform the body of the loop
						outputFunctionBody(m, block, pw);

						// if the block always breaks... then
						pw.op(IGIR.jmp(lblHead));
						pw.label(lblTail);			// target for "break"
					}
					popContinueBreak();			
									
			 		break;
				}
				
				case IGNode.DO: {

					IGNode block = node.m_children.get(0);				
					IGNode  c1 = node.m_children.get(1);
					
					String lblHead = getLabel();
					String lblContinue = getLabel();
					String lblTail = getLabel();
					
					pushContinueBreak(lblContinue, lblTail);
					{
						// initializer
						pw.label(lblHead);
					
						// perform the body of the loop
						outputFunctionBody(m, block, pw);

						// check to see if the loop should progress
						pw.label(lblContinue);	    // target for "continue"
						outputFunctionBody(m, c1, pw);
					
						pw.op(IGIR.jn0_i32(lblHead));		// if true go back to the top
						pw.label(lblTail);			// target for "break"
					}	
					popContinueBreak();			
									
			 		break;				
				}	


				case IGNode.NODE_FOR : { 
				
					IGNode  c0 = node.m_children.get(0);
					
					IGNode  c1 = node.m_children.get(1);
					IGNode  c2 = node.m_children.get(2);
					IGNode block = node.m_children.get(3);
					
					
					final String lblHead = getLabel();
					final String lblTail = getLabel();
					final String lblContinue = getLabel();
					
					pushContinueBreak(lblContinue, lblTail);
					
					// initializer
					outputFunctionBody(m, c0, pw);	
					pw.label(lblHead);
					
					// check to see if the loop should progress
					outputFunctionBody(m, c1, pw);
					pw.op(IGIR.j0_i32(lblTail));
					
					outputFunctionBody(m, block, pw);
					
					// incrementer				
					pw.label(lblContinue);
					outputFunctionBody(m, c2, pw);
					
					//if (!block.m_returns) {
					
					//System.out.println("NODE FOR CONTROL FLOW: " + c2.m_control_flow);
					
					// this was c2 before
					//if (block.m_control_flow == 0)
					{
						pw.op(IGIR.jmp(lblHead));
					}
					//}
					pw.label(lblTail);
								
					popContinueBreak();			
									
			 		break;
			 	}

	
	
				// for ( <child-0 VAR or EXPRESSION>  <child-1 NODE_TYPE_FOREACH_IN> )
				case IGNode.NODE_TYPE_FOREACH                   : { 
				
					/*
					
						#local_load container
						push_it					// creates an iterator from the container
						
					
					*/
					
					
					// this really needs a local variable to allocate it
				
				
					String iterator_variable_name = node.m_token.getUniqueVariableId();
					int    iterator_variable_index = getLocalIndex(m, node.m_token, iterator_variable_name,
									node.m_token.m_token_index , true);
				
				
					//pw.comment("foreach");
				
					// for a standard foreach, variable will be a definition or a statement
					// for a range    foreach, variable with be a definition or an expression	
					IGNode variable  = node.m_children.get(0);
					if (variable.m_node_type == IGNode.NODE_TYPE_LOCAL_VARIABLE_DEFINITION) {
						variable = variable.m_children.get(0);
					}
				
					
					IGNode container = node.m_children.get(1).m_children.get(0);
					IGNode iterator_fn = node.m_children.get(1).m_children.get(1);
					IGNode block     = node.m_children.get(2);
					
					
					
					
					
					if (iterator_fn.m_node_type == IGNode.FOREACH_IN_RANGE)
					{
					
						//node.debug(0);
						// NOTE in this block iterator_variable_index IS NOT THAT
						// its actually the contents of the max value
					
						String lblHead = getLabel();
						String lblTail = getLabel();
						String lblContinue = getLabel();
					
						// push the labels onto the stack for where to return execution for
						// continues or breaks
						pushContinueBreak(lblContinue, lblTail);
					
						// store the min value into the iterator
						outputFunctionBody(m, container, pw, 0);
						
						//System.out.println("VNT " + variable.m_node_type);
						IGIROp [] toks = outputFunctionBody(m, variable, pw, 1);
						pw.op(toks[1]);  	// store the value into the iteration variable
						pw.op(IGIR.pop(1));		// pop off the result of the store
							
						// store the max value
						outputFunctionBody(m, iterator_fn.m_children.get(0), pw, 0);
						pw.op(IGIR.local_store(Type.INT, iterator_variable_index));
						//pw.op(IGIR.pop);
					
						pw.label(lblHead);
						pw.op(toks[0]);
						
						pw.op(IGIR.local_load(Type.INT, iterator_variable_index));
						pw.op(IGIR.cmp_lt_i32());
						pw.op(IGIR.j0_i32(lblTail));
					
					
					
					
						// now what if there is a return within the function body... well hell I guess it'd just have
						// to push off the extra iterators in the tack
						outputFunctionBody(m, block, pw);
					
						//System.out.println("NODE FOR_EACH_IN CONTROL FLOW: " + block.m_control_flow);			
						pw.label(lblContinue);
					
						//if (block.m_control_flow == 0)
						{
							boolean optimized_increment = false;
						
							if (variable.m_node_type == IGNode.LOCAL_VARIABLE_ACCESS && 
								!"super".equals(variable.m_token.m_value))
							{
								final int load_index = getLocalIndex(m, variable.m_token, false);
								final int store_index = getLocalIndex(m, variable.m_token, true);
								if (load_index == store_index)
								{
									pw.op(IGIR.local_inc_i32(store_index, 1));
									optimized_increment = true;
								}
							}
						
					
							if (!optimized_increment) {
								pw.op(toks[0]);
								pw.op(IGIR.stack_inc_i32(1));
								pw.op(toks[1]);
								pw.op(IGIR.pop(1));
							}
							pw.op(IGIR.jmp(lblHead));
						
							// TODO verify if correct
						}
						
						pw.label(lblTail);	// pop off the iterator
					}
					else
					{
						String lblHead = getLabel();
						String lblTail = getLabel();
						String lblContinue = lblHead;
					
						// push the labels onto the stack for where to return execution for
						// continues or breaks
						pushContinueBreak(lblContinue, lblTail);
					
						Type container_type = m_ctx.resolveType(container.m_type);
						Type iterator_type  = m_ctx.resolveType(iterator_fn.m_type.unwrap().m_right);		// this is incorrect
					
						// okay so really how should this behave in terms of tries and catches
						//pushFinalizer("destroy_it " + iterator_variable_index + " #" + iterator_variable_name);
						{		
							outputFunctionBody(m, container, pw);

							pw.addFunctionDependency(container_type, "iterator");
							pw.addFunctionDependency(iterator_type , "hasNext");	
							pw.addFunctionDependency(iterator_type,  "next");

							pw.op(IGIR.call_ret1_vtable(container_type, "iterator", 1));
							pw.op(IGIR.local_store(iterator_type, iterator_variable_index));

							pw.label(lblHead);
							pw.op(IGIR.local_load(iterator_type, iterator_variable_index));
							pw.op(IGIR.call_ret1_vtable(iterator_type, "hasNext", 1));
							pw.op(IGIR.j0_i32(lblTail));
							pw.op(IGIR.local_load(iterator_type, iterator_variable_index));
							pw.op(IGIR.call_ret1_vtable(iterator_type, "next",    1));					
							
							IGIROp [] toks = outputFunctionBody(m, variable, pw, 1);
							pw.op(toks[1]); 	 	// store the value into the iteration variable
							pw.op(IGIR.pop(1));		// pop off the result of the store
											
							// now what if there is a return within the function body... well hell I guess it'd just have
							// to push off the extra iterators in the tack
							outputFunctionBody(m, block, pw);
							pw.op(IGIR.jmp(lblHead));
							pw.label(lblTail);	// pop off the iterator
						}
					}
					// call release.. this'll hint to the garbage collector that the object doesn't
					// have any references at this point in time
					
					//pw.println("\trelease " + iterator_variable_index + " #" + iterator_variable_name);
					
					popContinueBreak();
					
			 		break;
			 	}

	
				// in <child-0 EXPRESSION> 
				case IGNode.NODE_TYPE_FOREACH_IN                : {  throw new RuntimeException("error"); }

		
				//case IGNode.NODE_TYPE_RETURN_ID                 : { 
			// break;}
	
	
				case IGNode.NODE_BLOCK: 
				{ 
					node.calcMinLineNumber();
					
					final IGNodeList children = node.m_children;
					int children_count = children.size();
					//for (IGNode child : node.m_children)
					
					for (int child_idx = 0; child_idx < children_count; child_idx ++)
					{
						IGNode child = children.get(child_idx);
						// this would probably be the ideal place to output line #'s etc
						//pw.line(child.m_min_line_number);
						outputFunctionBody(m, child, pw);
					}
					break;
				}

				case IGNode.SWITCH:
				{
					String switch_term = getLabel();
					boolean switch_term_used = false;
					
					String switch_default_lbl = getLabel();
				
					String [] switch_labels = new String[node.m_children.size()];
				
					IGNode clause_block = node.m_children.get(0);
					outputFunctionBody(m, clause_block, pw);
					
					Type dst_type = node.m_children.get(0).m_type;
					Type cmp_type = dst_type;
					
					if (cmp_type.isEnum()) {
						cmp_type = cmp_type.getEnumPrimitive();
					}
					
					// TODO circle back and fix this switch statement
					for (int i = 1; i < node.m_children.size(); i++) 
					{
						IGNode n = node.m_children.get(i);
						if (n.m_node_type != IGNode.SWITCH_CASE) {
							continue;
						}	
						switch_labels[i] = getLabel();
						
						pw.op(IGIR.dup(1));
						outputFunctionBody(m, n.m_children.get(0), pw);
						castTo(n.m_children.get(0).m_type, dst_type, pw);
						if (cmp_type == Type.INT || cmp_type == Type.BOOL) { 
							pw.op(IGIR.cmp_eq_i32()); 
						}
						else if (cmp_type == Type.DOUBLE) { 
							pw.op(IGIR.cmp_eq_f64()); 
						}
						else {
							throw new RuntimeException("Currently unsupported switch type: " + dst_type);
						}
						pw.op(IGIR.jn0_i32(switch_labels[i]));
					}
					
					pw.op(IGIR.pop(1));
					pw.op(IGIR.jmp(switch_default_lbl));
				

					IGNode switch_default = null;
					// print out each individual case block
					for (int i = 1; i < node.m_children.size(); i++) {
						IGNode n = node.m_children.get(i);

						if (n.m_node_type == IGNode.SWITCH_DEFAULT) {
							switch_default = n;
							continue;
						}

						if (n.m_node_type != IGNode.SWITCH_CASE) {
							throw new RuntimeException("Bad Switch");
						}					
						
						pw.label(switch_labels[i]);
						pw.op(IGIR.pop(1));					// pop off the unwanted value
						
						IGNode case_node = n.m_children.get(1);
						outputFunctionBody(m, case_node, pw);
						
						if ((case_node.m_control_flow & IGNode.CF_RETURN) == 0) {
							switch_term_used = true;
							pw.op(IGIR.jmp(switch_term));
						}
					}
					
					// print out each individual switch default block
					pw.label(switch_default_lbl);

					if (null != switch_default)
					{
						IGNode n            = switch_default;
						IGNode default_node = n.m_children.get(0);
						outputFunctionBody(m, default_node, pw);
					}

					if (switch_term_used) 
					{
						pw.label(switch_term);	
					}
					
					break;
				}
	
				case IGNode.NODE_IF	: 
				{ 
					boolean terminate_if_label_used = false;

					String terminate_if_label = getLabel("TI");	
					String next_if_label      = getLabel("MI");
					
				
					
					IGNode if_condition = node.m_children.get(0);
					outputFunctionBody(m, if_condition, pw);
					pw.op(IGIR.j0_i32(next_if_label));
					
					IGNode if_block     = node.m_children.get(1);
					outputFunctionBody(m, if_block, pw);
					
					if (node.m_children.size() >= 3 && if_block.m_control_flow == 0) {
						terminate_if_label_used = true;
						pw.op(IGIR.jmp(terminate_if_label));
					}
	
					for (int i = 2; i < node.m_children.size(); i++)
					{
						IGNode if_sub_node = node.m_children.get(i);
						if (if_sub_node.m_node_type == IGNode.NODE_ELSE_IF) 
						{
							pw.label(next_if_label);
							next_if_label = getLabel("NI");
							
							IGNode if_sub_condition = if_sub_node.m_children.get(0);
							outputFunctionBody(m, if_sub_condition, pw);
							pw.op(IGIR.j0_i32(next_if_label));
							
							IGNode if_sub_block     = if_sub_node.m_children.get(1);
							outputFunctionBody(m, if_sub_block, pw);
							
							// see 2072 in UIWidget render
							// skip the terminate if we are the last element
							if (if_sub_block.m_control_flow == 0 && i != node.m_children.size() - 1) 
							{
								// this jump is also not necessary if not else clause is used
								terminate_if_label_used = true;
								pw.op(IGIR.jmp(terminate_if_label));
							}
						}
						else
						{
							pw.label(next_if_label);
							next_if_label = null;
							
							IGNode if_sub_block     = if_sub_node.m_children.get(0);
							outputFunctionBody(m, if_sub_block, pw);
						}
					}
					
					if (next_if_label != null) {
						pw.label(next_if_label);
					}
									
					if (terminate_if_label_used) { //node.m_children.size() >= 3) {
						pw.label(terminate_if_label);
					}
					//else {
					//	
					//}
					//pw.comment("endif");
					
				
			 		break;
				 }

				case IGNode.NODE_ELSE_IF:                   { throw new RuntimeException("fail");  }
				case IGNode.NODE_ELSE:                      { throw new RuntimeException("fail");   }
				case IGNode.NODE_TYPE_STATIC_VARIABLE_NAME: { throw new RuntimeException("fail");  }
				case IGNode.NODE_TYPE_VARIABLE_NAME:        { throw new RuntimeException("fail");  }
			 
			 
	
	
	
				// wraps inline ints, Strings, booleans, nulls
				case IGNode.NODE_VALUE							: { 
					Token t = node.m_token;
					
					if (t.m_type == TOK_TRUE) {		
						pw.op(IGIR.push_i32(1));
					}
					else if (t.m_type == TOK_FALSE) {
						pw.op(IGIR.push_i32(0));
					}
					else if (t.m_type == TOK_NULL) {
						pw.op(IGIR.push_obj_null());
					}	
					else if (t.m_type == TOK_NIL) {
						// the "work" will handle most of this
					}					
					else if (t.m_type == IGLexer.LEXER_INT) {
						pw.op(IGIR.push_i32(Integer.parseInt(t.m_value)));
					}
					else if (t.m_type == IGLexer.LEXER_FLOAT) {
						pw.op(IGIR.push_f64(util.Encoding.parseDouble(t.m_value)));
					}
					else if (t.m_type == IGLexer.LEXER_STRING) {
					
						pw.addModuleDependency(Type.STRING);

						String s     = t.m_value;
						int    s_len = s.length();
						
						int found_escape = -1;
						for (int ci = 0; ci < s_len; ci++) {
							char c = s.charAt(ci);
							if (c == '\\') {
								found_escape = ci;
								break;
							}
						}
						// i guess before the escaping procedure took care of this?
						if (-1 == found_escape) {
							pw.op(IGIR.push_string(t.m_value));
						}
						else 
						{
							//System.out.println("processing: '" + s + "'");
							StringBuilder result = new StringBuilder(s_len);	// initial cap?

							int s0 = 0;
								// CharSequence
							for (int ci = found_escape; ci < s_len; ci++) {
								char c0 = s.charAt(ci);
								if (c0 != '\\') {
									continue;
									
								}

								result.append(s, s0, ci);
								char c1 = s.charAt(ci + 1);
								boolean is_unicode_escape = false;
								
								switch (c1) {
									case 'n' :  result.append('\n'); break;
									case 'r' :  result.append('\r'); break;
									case 't' :  result.append('\t'); break;
									case '\\' : result.append('\\'); break;
									case '"'  : result.append('"'); break;
									case '\'' : result.append('\''); break;
									case 'u'  : 	
										is_unicode_escape = true;
										break;
										
									default: {
										throw new RuntimeException("Unsupported escape char: " + c1);
									}
								}
								
								if (is_unicode_escape)
								{
									char u0 = s.charAt(ci + 2);
									char u1 = s.charAt(ci + 3);
									char u2 = s.charAt(ci + 4);
									char u3 = s.charAt(ci + 5);
									
									
									int v = (fromHex(u0) << 12) | (fromHex(u1) << 8) | (fromHex(u2) << 4) | (fromHex(u3) << 0);
									
									result.append((char)v);
									
									
									s0 = ci + 2 + 4;
									ci = ci + 1 + 4;
								}
								else
								{
									s0 = ci + 2;
									ci++;			// skip the next character
								}
							}
							
							if (s0 < s_len) {
								result.append(s, s0, s_len);
							}
							pw.op(IGIR.push_string(result.toString()));
						}
					}
					else if (t.m_type == IGLexer.LEXER_HEX) {
						long value = Long.decode(t.m_value);
						pw.op(IGIR.push_i32((int)value));
					}
					else if (t.m_type == IGLexer.LEXER_CHAR) {
						pw.op(IGIR.push_i32(util.Decoder.escapedCharToInt(t.m_value)));
					}
					else
					{
						t.printError("What?");
						throw new RuntimeException(" IGNode.NODE_VALUE fail: " + t.m_type + " " + t.m_value);
					}
			 		break;
			 	}

	
				case IGNode.NODE_PARAMETER_LIST					: { 
				
					final IGNodeList children = node.m_children;
					int children_count = children.size();
					
					for (int child_idx = 0;  child_idx < children_count; child_idx++) {
						outputFunctionBody(m, children.get(child_idx), pw);
					}
					break;
				}

				case IGNode.PARAMETER_CAST: {
					outputCast(m, node.m_children.get(0), node.m_type, pw);
					break;
				}
				
				case IGNode.ARRAY_INITIALIZER : 
				{

					Type t = m_ctx.resolveType(node.m_children.get(0).m_type);
					//Type value_type = t.m_left;

					//int storage_class = t.m_left.getStorageClass();
						
					outputFunctionBody(m, node.getChild(0), pw);
						
					final IGNodeList children = node.getChild(1).m_children;
					int children_count = children.size();	
						
					//for (IGNode child : node.getChild(1).m_children)
					for (int child_idx = 0; child_idx < children_count; child_idx++)
					{	
						IGNode child = children.get(child_idx);
						if (child.m_node_type == IGNode.ARRAY_INITIALIZER_ITEM) 
						{
							int initializer_index = child.m_user_index;
							
							pw.op(IGIR.dup(1));		// duplicate the pointer
							pw.op(IGIR.push_i32(initializer_index));
							outputFunctionBody(m, child.m_children.get(0), pw);
							pw.op(IGIR.array_store(t, t.m_left));	//, intToString(storage_class));
						}
					}
					break;
				}

				case IGNode.VECTOR_INITIALIZER : 
				{
					Type t = m_ctx.resolveType(node.m_children.get(0).m_type);
					pw.addFunctionDependency(t, "push");

					int storage_class = t.m_left.getStorageClass();
						
					outputFunctionBody(m, node.getChild(0), pw);	
						
					final IGNodeList children = node.getChild(1).m_children;
					
					final int children_count = children.size();	
					for (int child_idx = 0; child_idx < children_count; child_idx++)
					{	
						IGNode child = children.get(child_idx);
						if (child.m_node_type == IGNode.VECTOR_INITIALIZER_ITEM) 
						{
							//int initializer_index = child.m_user_index;
							
							pw.op(IGIR.dup(1));		// duplicate the pointer
							outputFunctionBody(m, child.m_children.get(0), pw);
							pw.op(IGIR.call_vtable(t, "push", 2));
						}
					}
					break;
				}
				
				case IGNode.MAP_INITIALIZER: 
				{
					Type t = m_ctx.resolveType(node.m_children.get(0).m_type);
					pw.addFunctionDependency(t, "<set[]>");

					int storage_class = t.m_left.getStorageClass();
						
					outputFunctionBody(m, node.getChild(0), pw);	
						
					final IGNodeList children = node.getChild(1).m_children;
					int children_count = children.size();	
						
					//for (IGNode child : node.getChild(1).m_children)
					for (int child_idx = 0; child_idx < children_count; child_idx++)
					{	
						IGNode child = children.get(child_idx);
						if (child.m_node_type == IGNode.MAP_INITIALIZER_ITEM) 
						{
							int initializer_index = child.m_user_index;
							
							pw.op(IGIR.dup(1));		// duplicate the pointer
							outputFunctionBody(m, child.m_children.get(0), pw);
							// node 1 is ignored
							outputFunctionBody(m, child.m_children.get(2), pw);
							pw.op(IGIR.call_vtable(t,"<set[]>",3));
						}
					}
					break;
				}
	
				//  : TYPE  following a function declaration
				case IGNode.FUNCTION_RETURN_TYPE				: {  throw new RuntimeException("fail"); }

	


	
				case IGNode.PACKAGE								: {  throw new RuntimeException("fail"); }
				case IGNode.IMPORT								: {  throw new RuntimeException("fail"); }
				case IGNode.SOURCE								: {  throw new RuntimeException("fail"); }
				case IGNode.CLASS								: {  throw new RuntimeException("fail"); }
				case IGNode.CLASS_VARIABLE						: {  throw new RuntimeException("fail"); }
				case IGNode.CLASS_STATIC_VARIABLE				: {  throw new RuntimeException("fail"); }
				case IGNode.CLASS_FUNCTION						: {  throw new RuntimeException("fail"); }
				case IGNode.CLASS_STATIC_FUNCTION				: {  throw new RuntimeException("fail"); }


				case IGNode.TRY: 
				{
					int catch_count = node.m_children.size() -1;
					String []labels= new String[catch_count];

					Type   []types  = new Type[catch_count];

					final String lblTerm = getLabel();
					final String lblTryStart = getLabel();
					
					
					
					//System.err.println("TRY DEBUG");
					
					// mark that all exceptions between this instruction
					// and the mentioned label, should redirect to the label in question
					pw.op(IGIR.try_push(lblTryStart));
					
			
					for (int i = 0; i < labels.length; i++) 
					{
						labels[i] = getLabel();
						
						
						IGNode catch_node = node.m_children.get(i+1);
						
						// okay so if its a var statement it doesn't technically have a return type
						// thus we WILL need to drill down lower in the heirarchy to determine typing
						types[i] = catch_node.m_children.get(0).m_type;
						
						//catch_node.m_children.get(0).debug();
					}
				

					outputFunctionBody(m, node.m_children.get(0), pw);
					
					pw.op(IGIR.jmp(lblTerm));
					
					// the catch block disambiguator
					{
						pw.op(IGIR.catch_entry(lblTryStart));
						 
						for (int i = 0; i < catch_count; i++) 
						{
							pw.op(IGIR.dup(1));
							pw.op(IGIR.instance_of(types[i]));
							pw.op(IGIR.jn0_i32(labels[i]));
						}
						pw.op(IGIR._throw(Type.OBJECT));		// rethrow the exception in the case we found nothing
					}
					
					// process each catch block
					for (int i = 0; i < catch_count; i++) 
					{
						pw.label(labels[i]);
						IGNode catch_unit =	node.m_children.get(i + 1);
						
						// get ready the variable for the try catch
						IGIROp [] catch_expr = outputFunctionBody(m, catch_unit.m_children.get(0), pw, 1);
						pw.op(catch_expr[1]);
						pw.op(IGIR.pop(1));
							
						// prepare the body of the try catch						
						outputFunctionBody(m, catch_unit.m_children.get(1), pw);
	
						// if its not the last unit then jump to the end of the catch segment					
						if (i != catch_count - 1) {
							pw.op(IGIR.jmp(lblTerm));
						}
					}
					
					// try_pop pops all try blocks whose destination is prior to this 
					pw.label(lblTerm);
				
					break;
				}
				
				case IGNode.NODE_CATCH							: {  throw new RuntimeException("fail"); }

				case IGNode.NODE_FUNCTION_SIGNATURE				: { 
			 break;}

				case IGNode.FUNCTION_PARAMETERS             : {
					break;
				}

				case IGNode.NODE_RETURN							: 
				{ 
					 if ("<constructor>".equals(m.m_name.m_value) || 
					 	m.m_return_type == Type.VOID) {
					 	pw.op(IGIR.ret0());
					 }
					 else {
						outputFunctionBody(m, node.m_children.get(0), pw);
						
						Type return_type = m_ctx.resolveType(m.m_return_type);

						castTo(node.m_children.get(0).m_type, m.m_return_type, pw);
					
					 	pw.op(IGIR.ret1(return_type));
					 	
					 }
					 break; 
				}
				
				case IGNode.FLOW_CHANGE:
				{
					if (node.m_token.m_type == TOK_CONTINUE) {
						pw.op(IGIR.jmp(getContinueLabel()));
					}
					else if (node.m_token.m_type == TOK_BREAK) {
						pw.op(IGIR.jmp(getBreakLabel()));					
					}
					else
					{
						throw new RuntimeException("Unknown flow change event");
					}
					break;
				}

				case IGNode.NODE_ENUM_MEMBER					: { break;}
				case IGNode.NODE_ENUM							: { break;}
				case IGNode.NODE_INTERFACE						: {  break;}
				//case IGNode.SWITCH							    : { break; }
	
				case IGNode.SCOPE_CHAIN_START					: { 
					 break;
			 	}
 
 			default:
 				//this seems to just be for @cpp calls etc...
 				//node.m_token.printError("Where am I?");
 				System.err.println("Unknown: " + node.m_node_type);
 				throw new RuntimeException("ASM Failure");
 				//break;
		}
		
		return empty2();
	}


	//////////////////////////////////////////////////////
	// Main entry point
	/////////////////////////////////////////////


	IGIRTemplateUtil m_template_util = new IGIRTemplateUtil();

	public IGContext m_ctx = null;
		
	public void configContext(IGContext ctx)
	{
		ctx.clearTypeReplace();
		ctx.setCollapseObjectTemplateTypes(true);
	}
	


	//private int m_asm_writer_mode = IGIRModule.Function.MODE_WRITE_HEADERS;

	/**
		THIS IS THE MAIN ENTRY POINT
	*/

	//public static class Config 
	//{/
	//	public String dst_extension;
	//}


	private Map<Type, IGIRModule> m_modules = new HashMap<Type,IGIRModule>();

	public IGIRModule resolveToModule(int category, Type type) {
		IGIRModule m = m_modules.get(type);
		if (m != null) {
			return m;
		}

		m = new IGIRModule();
		m.init(category, type);
		m_modules.put(type, m);
		return m;
	}
	
	public  Vector<IGIRModule> run(
		IGPackage        root, 
		Vector<IGSource> sources)
	{	
	
		//m_asm_writer_mode = 0;//	asm_writer_mode;
		
		// avoid weird load patterns
		Characterization.touch();
		
		try
		{
		
			Vector<IGIRModule> to_run = new Vector<IGIRModule>();
		
			// process all non templated classes
			for (IGSource s : sources)
			{
				if (s.isTemplatedClass()) 
				{
					continue;
				}
			
				m_ctx = new IGContext();
				configContext(m_ctx);
				
				try
				{
					to_run.add(processSource(s));
				}
				catch (RuntimeException ex) {
					System.out.println("IR compilation failed in: " + s);
					throw ex;
				}
			}
			
			
			// start working through templated variants
			while (m_template_util.augmentTemplateVariants())
			{		
				for (IGSource s : sources)
				{
					if (!s.isTemplatedClass()) 
					{
						continue;
					}
				
				
					boolean isTemplated = false;
				
				
					Type idt = s.getInstanceDataType();
				
					// there's literally no reason to ever output this source file
					//if (!template_variants.containsKey(idt.m_primary)) {
					//}
				
					IGIRTemplateUtil.IGIRTemplateVariant tv = m_template_util.getVariant(idt.m_primary);
					if (tv == null) { continue; }
					
					int template_count = tv.m_lefts.size();
				
					//System.err.println("Templated class output");
				
			
			
					for (int template_index = 0; template_index < template_count; template_index++)
					{
						m_ctx = new IGContext();
						configContext(m_ctx);
					
						Type l = tv.m_lefts.get(template_index);
						Type r = tv.m_rights.get(template_index);
				
						ArrayList<IGTemplateParameter> tpl = s.m_template_parameters;
						for (int k = 0; k < tpl.size(); k++) {
							if (k == 0) {
								m_ctx.addTypeReplace(tpl.get(0).type, l);
							}
							else if (k == 1) {
								m_ctx.addTypeReplace(tpl.get(1).type, r);
							}
						}
				
						
						to_run.add(processSource(s));
					}
					
					// clear out the processed list of lefts and rights
					tv.mark();
				}
			}
			
			////////////////////////////////////////////////////////////////
			// Convert asm files to binary asm files
			
			
			
			return to_run;
			
		}
		catch (IOException ex)
		{
			ex.printStackTrace();
			return null;
		}
	}
	
	private int fromHex(char c) {
		if ('0' <= c && c <= '9') {
			return c - '0';
		}
		else if ('a' <= c && c <= 'f') {
			return 10 + (c - 'a');
		}
		else if ('A' <= c && c <= 'F') {
			return 10 + (c - 'A');
		}
		return 0;
	}

	/*
	 * Process a source file.  The particulars of the type remapping will be set up prior to calling this
	 * 
	 */
	
	public IGIRModule processSource(IGSource s) throws IOException
	{
		resetLabelCounter();
	
		//String extension = generateExtension(s);
	
		IGIRModule module = null;
		Type module_type = m_ctx.getType(s);
					


		if (s.m_type == IGScopeItem.SCOPE_ITEM_ENUM)
		{
			module = resolveToModule(IGIRModule.ENUM, module_type);	
		}
		else if (s.m_type == IGScopeItem.SCOPE_ITEM_INTERFACE)
		{
			module = resolveToModule(IGIRModule.INTERFACE, module_type);	
		}
		else if (s.m_type == IGScopeItem.SCOPE_ITEM_CLASS) 
		{
			module = resolveToModule(IGIRModule.CLASS, module_type);
			{
				IGSource s2 = s;
				while (s2 != null) 
				{
					for (Type t : s2.m_class_implements) {
						module.addImplements(resolveToModule(IGIRModule.INTERFACE, t));
					}

					if (s2.m_class_extends != null && m_ctx.getType(s2) != Type.get("iglang.Object"))
					{					
						module.addExtends(resolveToModule(IGIRModule.CLASS, s2.m_class_extends));
					}
					else {
						break;
					}

					s2 = (IGSource)s2.m_class_extends.m_scope;
				}
			}
		
			
			//for (Type t : s.m_class_implements) {
			//	module.addImplements(resolveToModule(IGIRModule.INTERFACE, t));
			//}						
		}
	
		IGIRModule.Function found_constructor = null;
		IGMember            found_constructor_member = null;
			// TODO resolve this differently
		if (s.m_type != IGScopeItem.SCOPE_ITEM_INTERFACE) 
		{
			
	
			for (IGMember m : s.m_members) 
			{
				if (m.hasModifier(IGMember.STUB)) {
					continue;
				}
		
		

				
				if (m.m_type == IGMember.TYPE_VAR) 
				{
					IGValueRange vr = null;

					IGIRModule.Variable var = null;
		

					boolean fin = false;

					// look for static variables
					if (m.hasModifier(IGMember.STATIC)) 
					{
						var = module.createStaticVariable();
						if (m.hasModifier(IGMember.ENUM_VALUE)) 
						{
							vr  = m.m_return_value_range;
							fin = true;
						}
						else
						{
							fin = m.hasModifier(IGMember.FINAL);

							// see if this is an assignment statement
							IGNode node = m.m_node.findRightMostWithNodeAndTokenType(IGNode.NODE_BINARY, TOK_ASSIGN);
							//System.out.println("" + node);
							
							if (node != null) {
								//node.debug();
								vr = node.m_value_range;
							}
						}
					}
					else {
						var = module.createMemberVariable();
					}

					//var.setPermissions(permissions);
					var.setName(m.m_name.m_value);
					var.setFullType(m_ctx.resolveTypeDoNotCollapse(m.m_return_type));
					var.setType(m_ctx.resolveType(m.m_return_type));
					var.setFinal(fin);
					
				
					if  (vr != null) { //} && vr.isConcrete()) {
						var.setInitialValue(vr);
					}
				}
				else if (m.m_type == IGMember.TYPE_FUNCTION)
				{
					IGIRModule.Function fn = null;
					String [] sb = null;
				
					if (m.hasModifier(IGMember.STATIC)) {
						fn = module.createStaticFunction();
					}
					else if (s.getScopeType() == IGScopeItem.SCOPE_ITEM_ENUM) {
						fn = module.createStaticFunction();
						fn.setFinal(true);		
					}
					else
					{
						fn = module.createMemberFunction();
					}


					if ("<constructor>".equals(m.m_name.m_value)) {
						found_constructor = fn;
						found_constructor_member = m;
					}

	


					fn.setName(m.m_name.m_value);
					fn.setParameterCount(m.getMinParameterCount(), m.getMaxParameterCount());
					//fn.setLocalCount(intToString(m.getConservativeLocalVariableCount()));

					if ("<constructor>".equals(m.m_name.m_value)) {
						fn.setReturnType(Type.VOID);
					}
					else {
						fn.setReturnType(m_ctx.resolveType(m.m_return_type));
					}			

					if (m.hasModifier(IGMember.FINAL)) {	
						fn.setFinal(true);
					}
					
					int idx = 0;
					if (!m.hasModifier(IGMember.STATIC)) 
					{
						fn.addParameter("this", module_type, null); 
						idx ++;
					}
				
					// set the default values for all parameters
					// all values have a default of binary 0 unless otherwise specified
					
					ArrayList<IGVariable> parameters = m.m_parameters;
					for (int parameter_idx = 0; parameter_idx < parameters.size(); parameter_idx++) 
					{
						IGVariable p = parameters.get(parameter_idx);
						if (!p.m_has_default) {
							fn.addParameter(p.m_name.m_value, m_ctx.resolveType(p.m_type), null);
						}
						else {
							fn.addParameter(p.m_name.m_value, m_ctx.resolveType(p.m_type), p.getDefaultValueRange());
						}
					}							
			
				
					// output the rest of the body if this
					// is not the constructor
					if (found_constructor != fn) 
					{
						IGNode body_node = m.m_node.getChild(1);
						outputFunctionBody(m, body_node, fn);
					
						if (m.m_return_type == Type.VOID) {
							fn.op(IGIR.ret0());
						}
									 
						fn.op(IGIR.end());
						IGIROptimizer.optimize(fn.m_ops);
					}
				}
			}
		}

		if (s.getScopeType() == IGScopeItem.SCOPE_ITEM_CLASS) 
		{
			if (found_constructor != null)
			{
				IGIRModule.Function fn = found_constructor;
				IGMember             m = found_constructor_member;

			
				////////////////////////////////////////////
				// Call the super class, if it hasn't already been
				// done so by the user.
				////////////////////////////////////////////
				if (s.m_class_extends != null && !m.m_super_constructor_called) 
				{
					// TODO this needs to be modified
					// the previous layer really NEEDS to error if there are more
					// than 1 parameters to a constructor and it isn't explicitly called
				
					Type super_type = m_ctx.getType(s);

					fn.addFunctionDependency(super_type, "<constructor>");

					fn.op(IGIR.local_load(super_type, 0));
					fn.op(IGIR.call_super(super_type, "<constructor>", 1));
				}									
			
				boolean found_possible_state_disruption = false;
			
				////////////////////////////////////////
				// Output Member variable initializers
				// These are only generated for NON DEFAULT values
				// ie. a 0 bit encoding
				////////////////////////////////////////
				for (IGMember source_member : s.m_members) 
				{
					if (source_member.m_type != IGMember.TYPE_VAR ||
						source_member.hasModifier(IGMember.STATIC)) {
					
						continue;	
					}
					
					
					// figure out if the member variable has an initial value
					IGNode n = source_member.m_node.findRightMostWithNodeAndTokenType(IGNode.NODE_BINARY, TOK_ASSIGN);
					if (n != null) 
					{
						Type t = n.m_children.get(1).m_type;	
						
						if (n.m_children.get(1).m_type.isFunctionWrapper()) {
							outputFunctionWrapperCast(source_member,n.m_children.get(1), source_member.m_return_type, fn);
						}		
						else
						{
							outputFunctionBody(source_member, n.m_children.get(1), fn);
							castTo(t, source_member.m_return_type, fn, true);
						}

						fn.addMemberDependency(module_type,  source_member.m_name.m_value);

						fn.op(IGIR.dup(1));
						fn.op(IGIR.this_store(module_type,  source_member.m_name.m_value));
						fn.op(IGIR.pop(1));
					}
				}		
				

				IGNode body_node = m.m_node.getChild(1);
				outputFunctionBody(m, body_node, fn);
				
				fn.op(IGIR.ret0());
						fn.op(IGIR.end());
						IGIROptimizer.optimize(fn.m_ops);
			}
			else if (null == found_constructor) 
			{
				// okay so this actually seems to work correctly for when
				// there is no constructor found for a particular class


				IGMember base_constructor = s.getFirstConstructor();
		
				int parameter_count = base_constructor.getMaxParameterCount();
				int min_param_count = base_constructor.getMinParameterCount();
		
				IGIRModule.Function fn = module.createMemberFunction();
				fn.setName("<constructor>");
				fn.setParameterCount(min_param_count, parameter_count);
				fn.setReturnType(Type.VOID);
				fn.addParameter("this", module_type, null);

				final ArrayList<IGVariable> parameters = base_constructor.m_parameters;
				//final int parameter_count = parameters.size();


				for (int parameter_idx = 0; parameter_idx < parameters.size(); parameter_idx++) 
				{
					IGVariable p = parameters.get(parameter_idx);
					if (!p.m_has_default) {
						fn.addParameter(p.m_name.m_value, p.m_type, null);
					}
					else {
						fn.addParameter(p.m_name.m_value, p.m_type, p.getDefaultValueRange());
					}
				}	
		
		
		
		
				if (s.m_class_extends != null) 
				{
					fn.op(IGIR.local_load(module_type, 0));
					// ? why does this start at 1?
					for (int i = 0; i < parameters.size(); i++) {

						IGVariable p = parameters.get(i);
						fn.op(IGIR.local_load(p.m_type, i));	
					}

					fn.addFunctionDependency(module_type, "<constructor>");
					fn.op(IGIR.call_super(module_type, "<constructor>", parameter_count));
				}	
		
				
				for (IGMember source_member : s.m_members) 
				{
					if (source_member.m_type == IGMember.TYPE_VAR &&
						!source_member.hasModifier(IGMember.STATIC)) 
					{
						IGNode n = source_member.m_node.findRightMostWithNodeAndTokenType
							(IGNode.NODE_BINARY, TOK_ASSIGN);

						if (n != null) 
						{
							Type t = n.m_children.get(1).m_type;	
							
							if (n.m_children.get(1).m_type.isFunctionWrapper()) {
								outputFunctionWrapperCast(source_member,n.m_children.get(1), source_member.m_return_type, fn);
							}		
							else
							{
								outputFunctionBody(source_member, n.m_children.get(1), fn);
								castTo(t, source_member.m_return_type, fn, true);
							}
							
							fn.op(IGIR.dup(1));
							fn.op(IGIR.this_store(module_type,  source_member.m_name.m_value));
							fn.op(IGIR.pop(1));
						}
					}
				
				}						
	
				fn.op(IGIR.ret0());
						fn.op(IGIR.end());
						IGIROptimizer.optimize(fn.m_ops);
			}
		}


		// print out the static initializers	
		/////////////////////////////////////////////			
		{
			IGIRModule.Function fn = module.createStaticFunction();
			fn.setName("<static>");
			fn.setParameterCount(0,0);
			fn.setPublic();
			fn.setFinal(true);
			fn.setReturnType(Type.VOID);
			//Type module_type = m_ctx.getType(s);

			for (IGMember m : s.m_members) 
			{
				if (m.m_type == IGMember.TYPE_VAR) 
				{
					if (m.hasModifier(IGMember.STATIC)) 
					{
						if (m.hasModifier(IGMember.ENUM_VALUE)) 
						{
							IGValueRange vr = m.m_return_value_range;
							
							if (vr.m_type == IGValueRange.TYPE_INT) {
								fn.op(IGIR.push_i32(vr.intValue()) );	
							}
							else if (vr.m_type == IGValueRange.TYPE_DOUBLE) {
								fn.op(IGIR.push_f64(vr.doubleValue()));		
							}
							else if (vr.m_type == IGValueRange.TYPE_POINTER_NULL) {
								fn.op(IGIR.push_obj_null());	
							}
							else if (vr.m_type == IGValueRange.TYPE_DELEGATE_NULL) {
								fn.op(IGIR.push_fp_null());
							}
							else if (vr.m_type == IGValueRange.TYPE_BOOL_FALSE) {
								fn.op(IGIR.push_i32(0));	
							}
							else if (vr.m_type == IGValueRange.TYPE_BOOL_TRUE) {
								fn.op(IGIR.push_i32(1));	
							}
							else {
								throw new RuntimeException("Invalid parameter value range");
							}
	
							//pw.opIGIR.dup(1);
							fn.addMemberDependency(module_type, m.m_name.m_value);
							fn.op(IGIR.static_store(module_type, m.m_name.m_value));
						}
						else
						{
							IGNode n = m.m_node.findRightMostWithNodeAndTokenType(IGNode.NODE_BINARY, TOK_ASSIGN);
				
							if (n != null) {
								Type t = n.m_children.get(1).m_type;	
								
								if (n.m_children.get(1).m_type.isFunctionWrapper()) {
										outputFunctionWrapperCast(m,n.m_children.get(1), m.m_return_type, fn);
								}		
								else
								{
									outputFunctionBody(m, n.m_children.get(1), fn);
									castTo(t, m.m_return_type, fn, true);
								}
								
								fn.addMemberDependency(module_type, m.m_name.m_value);
								fn.op(IGIR.static_store(module_type, m.m_name.m_value));
							}
						}
					}
				}
			}				
	
			fn.op(IGIR.ret0());
						fn.op(IGIR.end());
						IGIROptimizer.optimize(fn.m_ops);
		}
		
		
		///////////////
		// Additional functionality for enums
		// 1) toString
		// 2) equals
		// 3) hashCode
		// it looks like all enums not backed by ints are broken
		if (s.getScopeType() == IGScopeItem.SCOPE_ITEM_ENUM) 
		{
			Type backing_type = Type.INT;




			// Print out the default enum toString
			////////////////
			
			if (!s.hasUserMemberFunction("toString"))
			{
				IGIRModule.Function fn = module.createStaticFunction();
				fn.setName("toString");
				fn.setParameterCount(1,1);
				fn.setPublic();
				fn.setFinal(true);

				String lblEnd = getLabel();
				for (IGMember m : s.m_members) 
				{
					if (m.m_type == IGMember.TYPE_VAR && 
						m.hasModifier(IGMember.STATIC) && 
						m.hasModifier(IGMember.ENUM_VALUE))
					{
						String next_label = getLabel();
				
						IGValueRange vr = m.m_return_value_range;
						
						if (vr.m_type == IGValueRange.TYPE_INT) {
							fn.op(IGIR.local_load(backing_type, 0));
							fn.op(IGIR.push_i32(vr.intValue()));	
							fn.op(IGIR.cmp_eq_i32());	
						}
						else {
							throw new RuntimeException("NYI");
						}
						/*
						else if (vr.m_type == IGValueRange.TYPE_DOUBLE) {
							fn.op(IGIR.local_load(backing_type, 0));
							fn.op(IGIR.push_f64(vr.doubleValue()));		
							fn.op(IGIR.cmp_eq_f64());
						}
						else if (vr.m_type == IGValueRange.TYPE_POINTER_NULL) {
							fn.op(IGIR.local_load(backing_type, 0));
							fn.op(IGIR.push_obj_null());
							fn.op(IGIR.cmp_eq_obj());
						}
						else if (vr.m_type == IGValueRange.TYPE_DELEGATE_NULL) {
							fn.op(IGIR.local_load(backing_type, 0));
							fn.op(IGIR.push_fp_null());
							fn.op(IGIR.cmp_eq_fp());
						}
						else if (vr.m_type == IGValueRange.TYPE_BOOL_FALSE) {
							fn.op(IGIR.local_load(backing_type, 0));
							fn.op(IGIR.push_i32(0));
							fn.op(IGIR.cmp_eq_i32());	
						}
						else if (vr.m_type == IGValueRange.TYPE_BOOL_TRUE) {
							fn.op(IGIR.local_load(backing_type, 0));
							fn.op(IGIR.push_i32(1));	
							fn.op(IGIR.cmp_eq_i32());
						}
						else {
							throw new RuntimeException("Invalid parameter value range");
						}
						*/
						
						fn.op(IGIR.j0_i32(next_label));
						fn.op(IGIR.push_string(m.m_name.m_value));
						fn.op(IGIR.jmp(lblEnd));
						fn.label(next_label);
					
					}
				}
		
				fn.op(IGIR.push_string("undefined"));
				fn.label(lblEnd);
				fn.op(IGIR.ret1(Type.BOOL));	
						fn.op(IGIR.end());
						IGIROptimizer.optimize(fn.m_ops);
			}
		
			// add a default equals
			if (!s.hasUserMemberFunction("equals")) 
			{

				IGIRModule.Function fn = module.createStaticFunction();
				fn.setName("equals");
				fn.setParameterCount(2,2);
				fn.setPublic();
				fn.setFinal(true);

				fn.op(IGIR.local_load(backing_type, 0));
				fn.op(IGIR.local_load(backing_type, 1));
				fn.op(IGIR.cmp_eq_i32());
				fn.op(IGIR.ret1(Type.BOOL));
						fn.op(IGIR.end());
						IGIROptimizer.optimize(fn.m_ops);
			}
		
			// add a default hashCode
			if (!s.hasUserMemberFunction("hashCode")) 
			{
				IGIRModule.Function fn = module.createStaticFunction();
				fn.setName("hashCode");
				fn.setParameterCount(2,2);
				fn.setPublic();
				fn.setFinal(true);
			
				fn.op(IGIR.local_load(backing_type, 0));
				fn.op(IGIR.ret1(Type.INT));
						fn.op(IGIR.end());
						IGIROptimizer.optimize(fn.m_ops);	
			}
		}

		return module;
	}
}

/*
 * Characterization
 * - this is used to determine HOW a named variable or function should be accessed
 */

final class  Characterization
{
	public static void touch() {
	}


	private String m_name = null;
	private Type   m_type = null;
	private IGNode m_to_process = null;
	private boolean m_needs_this_push = false;
	private boolean m_function_pointer = false;
	private IGMember  m_function = null;
	private boolean m_super_call = false;
	
	public IGNode getProcessingNode()
	{
		return m_to_process;
	}
	
	public boolean needsThisPush()
	{
		return m_needs_this_push;
	}
	
	
	public static final int FIELD = 0;
	public static final int CALL  = 1;
	
	public Characterization(IGContext ctx, int mode, IGIRGenerator o, IGMember m, IGNode root)
	{
		// are we specifically accessing a field?
		if (mode == FIELD) 
		{
			switch (root.m_node_type) 
			{
				//case IGNode.NODE_LOCAL_GET:
				//case IGNode.NODE_LOCAL_SET:
				
				case IGNode.LOCAL_ACCESSOR:
				{
					m_name = root.m_token.m_value;
					
					IGMember the_actual_function = (IGMember)root.m_scope;
					// okay so this doesn't detect that the function in question is in fact static
					// this just detects the scope that the function (getter) was called within
					// TODO fix
					
					IGSource s = m.m_parent_source;
					//if (the_actual_function.hasModifier(IGMember.STATIC)) {
					//	m_type = s.getDataType(null);
					//}
					//else
					//{
						m_type = ctx.getType(s);
					//}
					
					m_function = the_actual_function;
					
					break;
				}
				case IGNode.STATIC_LOCAL_ACCESSOR:
				{
					m_name = root.m_token.m_value;
					
					IGMember the_actual_function = (IGMember)root.m_scope;
					// okay so this doesn't detect that the function in question is in fact static
					// this just detects the scope that the function (getter) was called within
					// TODO fix
					
					IGSource s = m.m_parent_source;
					//if (the_actual_function.hasModifier(IGMember.STATIC)) {
						m_type = s.getDataType(null);
					//}
					//else
					//{
					//	m_type = ctx.getType(s);
					//}
					
					m_function = the_actual_function;
					
					break;
				}
				
				//case IGNode.NODE_DOT_GET:
				//case IGNode.NODE_DOT_SET:
				
				case IGNode.STATIC_DOT_ACCESSOR:
				case IGNode.DOT_ACCESSOR:
					
					m_function = (IGMember)root.m_scope;
					
					// handle the case
					// super DOT getter-or-setter
					{
						IGNode left_node = root.m_children.get(0);
						if (left_node.m_node_type == IGNode.LOCAL_VARIABLE_ACCESS &&
							left_node.m_token.m_value.equals("super")) {
							m_super_call = true;		
						}
					}
					
					// fall through
				default:
					m_name = root.m_children.get(1).m_token.m_value;
					m_type = root.m_children.get(0).m_type;
					m_to_process = root.m_children.get(0);
		
					if (isStatic()) {
						m_to_process = null;
					}
			}
			
			
			m_type = ctx.resolveType(m_type);
			
			return;
		}
	
		// okay so we're accessing a function
	
		//if (root.m_children.size() != 2) {
		//	root.m_token.printError("what?");
		//	root.debug(1);
		//	throw new RuntimeException("How does a call node NOT have 2 children?");
		//	
		//}
	
		IGNode left = root; //.m_children.get(0);
		//IGNode right = root.m_children.get(1);	
	
		/////////////////////////////////////////
		// Call Delegate
		/////////////////////////////////////////
		if (!left.m_type.isFunctionWrapper()) {
		
		
			
			
			m_to_process = left;
			m_function_pointer = true;
			m_type = null;
			m_name = null;
			
			m_type = ctx.resolveType(m_type);
			
			//System.out.println("AAAA: " + left.m_type);
		}
		/////////////////////////////////////////
		// Call Function/Static-Function/
		/////////////////////////////////////////
		else
		{
			m_function_pointer = false;
			
			//System.out.println("BBBB: " + left.m_type);
			
			switch (root.m_node_type) {
				case IGNode.NODE_DOT:
				case IGNode.DOT_ACCESSOR:
				case IGNode.STATIC_DOT_ACCESSOR:
				
				//case IGNode.NODE_DOT_SET:
				//case IGNode.NODE_DOT_GET:
				
					m_function 	 = (IGMember)root.m_scope;
					m_name 		 = left.m_children.get(1).m_token.m_value;
					m_type 		 = left.m_children.get(0).m_type;
					m_to_process = left.m_children.get(0);
				
					if (m_to_process.m_node_type == IGNode.LOCAL_VARIABLE_ACCESS &&
						m_to_process.m_token.m_value.equals("super")) {
						m_super_call = true;		
					}
				
					// the function is viewed as being final
					// so we should set up the type so that it matches that of the member?
					
				
					//if (root.m_node_type == IGNode.NODE_DOT_GET) {
					//	System.out.println("" + root);
					//}
				
					if (isStatic()) {
						m_to_process = null;
					}
					break;
				
				// accessing a static function
				case IGNode.STATIC_FIELD_ACCESS:
				{
					m_function = (IGMember)root.m_scope;
					m_name = left.m_token.m_value;
					
					IGMember the_actual_function = (IGMember)root.m_scope;
					// okay so this doesn't detect that the function in question is in fact static
					// this just detects the scope that the function (getter) was called within
					// TODO fix
					
					IGSource s = m.m_parent_source;
					if (the_actual_function.hasModifier(IGMember.STATIC)) {
						m_type = the_actual_function.m_parent_source.getDataType(null);
					}
					else
					{
						m_type = the_actual_function.m_parent_source.getInstanceDataType();
					}
					
		
					m_to_process = null;
					break;
				}
			
				// accessing a member function
				case IGNode.FIELD_ACCESS:
					m_function = (IGMember)root.m_scope;
					m_name = left.m_token.m_value;
					
					
					m_type = m.m_parent_source.getInstanceDataType();
					m_to_process = null;
					m_needs_this_push = true;
					break;
				
				case IGNode.LOCAL_VARIABLE_ACCESS:
					// more than likely a delegate
					throw new RuntimeException("Invalid");
					//break;
				
				default:
					System.err.println("Unknown characterization for: " + root.m_node_type);
			}
			
			Type prev_type = m_type;
			m_type = ctx.resolveType(m_type);	
			
			if (prev_type != m_type) 
			{
				String function_name = m_function.m_name.m_value;
				
				m_function = ((IGSource)m_type.m_scope).getMember(function_name);
				
				
			}
			
			///if (isFinal()) 
			//		{
						//System.out.println("fuck fuck fuck fuck fuck");
						//if (m_function.m_parent_source != (IGSource)m_type.m_scope)
						//{	
							//System.out.println("need drill down: " + 			m_type.m_scope + " " + 			m_function.m_parent_source);
						//}
			//		}
				
			// okay so the problem is 
		}
		
			
		
		//if (m_function != null && m_function.hasModifier(IGMember.FINAL)) {
		//	m_type 
		//}
		
		//System.err.println("Characterization");

		//System.err.println("name: " + m_name);
		//System.err.println("type: " + m_type);
		//System.err.println("root-type: " + root.m_node_type);
	
		//root.debug(1);
		/*
		if (n.m_type == IGNode.STATIC_FIELD_ACCESS) {
		
		}
		else if (n.m_type == IGNode.NODE_DOT) {
		
		}
		*/
	}
	
	public boolean isObject() {
		return !m_type.isNamespace();
	}
	
	public boolean isSuper() {
		//System.out.println("is super");
		return m_super_call;
	}
	
	public boolean isStatic() {
		return m_type.isNamespace();
	}
	
	// fordebugging
	public IGMember getFunction() {
		return m_function;
	}
	
	public boolean isFinal() {
	
		if (m_function_pointer) return false;
		
		if (m_function != null && m_function.hasModifier(IGMember.FINAL)) {
			return true;
		
		}	
		
		return false;
	}
	
	public boolean isInterface() {
		if (isObject()) {
			IGScopeItem si = m_type.m_scope;
			if (si instanceof IGSource) {
				IGSource m2  = (IGSource)si;
				return m2.m_type == IGScopeItem.SCOPE_ITEM_INTERFACE;
			}
		}
		return false;
	}
	
	public boolean isEnum() {
		if (isObject()) {
			IGScopeItem si = m_type.m_scope;
			if (si instanceof IGSource) {
				IGSource m2  = (IGSource)si;
				return m2.m_type == IGScopeItem.SCOPE_ITEM_ENUM;
			}
		}
		return false;
	}
	
	public boolean isFunctionPointer() {
		return m_function_pointer;
	}
	
	
	public String  getName()
	{
		return m_name;
	}
	
	public void setType(Type t) {
		m_type = t;
	}
	
	public Type getObjectType() {
		return m_type;
	}
	

	/**
	 * If a function call is on a primitive it is actually on
	 * the backing code of that primitive
	 */

	public Type getType()
	{
		// replacing of overridden scope types
		// or should this just mark that the type has been overridden... probably
		if (m_type == Type.INT) {
			return Type.igInteger;
		}
		else if (m_type == Type.DOUBLE) {
			return Type.igDouble;
		}
		else if (m_type == Type.FLOAT) {
			return Type.igFloat;
		}
		else if (m_type == Type.BOOL) {
			return Type.igBoolean;
		}
		else if (m_type == Type.SHORT) {
			return Type.igShort;
		}
		else if (m_type == Type.BYTE) {
			return Type.igByte;
		}
	
		if (m_type == null) {
			throw new RuntimeException("Null type");
		}

		String s =  m_type.toString();
		if (s.indexOf("source:") != -1) {
			return Type.get(s.substring("source:".length()));
		}
		else
		{
			return m_type;
		}
		
	} 



}





 final class IGIRTemplateUtil
{
		
	private HashMap<String, IGIRTemplateVariant> m_template_variants = new HashMap<String,IGIRTemplateVariant>();
	private boolean m_has_unprocessed_template_variants = true;
	
	
	/**
	 * Get a variant identified by the template class path.  e.g.  iglang.Vector
	 */
	
	public IGIRTemplateVariant getVariant(String primary_key) {
		if (!m_template_variants.containsKey(primary_key)) {
			return null;
		}
		return m_template_variants.get(primary_key);
	}
	
	/**
	 * Add to the list any new template variants newly discovered.
	 * This scans the entire contents of the Types database and checks if anything is missing
	 */
	
	public boolean augmentTemplateVariants()
	{
		m_has_unprocessed_template_variants = false;
	
		ArrayList<Type> templated_types = new ArrayList<Type>();
		Type.getTemplatedTypes(templated_types);
	
		// group the list of template variants by their primary type
		for (Type t: templated_types) {
			if (!m_template_variants.containsKey(t.m_primary)) {
				m_template_variants.put(t.m_primary, new IGIRTemplateVariant());
			}
			
			IGIRTemplateVariant tv = m_template_variants.get(t.m_primary);
			if (tv.add(t)) {
				m_has_unprocessed_template_variants = true;
			}
		}	
		
		return m_has_unprocessed_template_variants;
	}


	static class IGIRTemplateVariant
	{
		Vector<Type> m_lefts  = new Vector<Type>();
		Vector<Type> m_rights = new Vector<Type>();		
		
		
		HashMap<String,Type> m_existing = new HashMap<String,Type>();
		
		public void mark() 
		{
			// clear out all the processed variants
			m_lefts  = new Vector<Type>();
			m_rights = new Vector<Type>();			
		}
		
		public boolean add(Type t) 
		{
			Type left = t.m_left;
			Type right = t.m_right;
			
			// throw out templated stubs
			//if (left == Type.get("iglang.T1") || 
			//	left == Type.get("iglang.T2") || 
			//	right == Type.get("iglang.T1") || 
			//    right == Type.get("iglang.T2")) {

			if ((left != null && left.isTemplated()) || 
				(right != null && right.isTemplated())) {
			 	return false;   
			
			}

			IGSource source = (IGSource)(t.m_scope);
			
			if (left.m_primary.equals("Function") || left.m_primary.equals("StaticFunction") || left.m_primary.equals("MemberFunction")) {
				left = Type.get("iglang.util.igDelegate");		// ?? should this even exist?
			}
			
			if (left.m_scope != null)
			{
				// This is the only case in which type erasure is permitted
				if (left.m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_CLASS ||
					left.m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_INTERFACE) {
					if (source.isTemplatedAny(0)) {
						left = Type.OBJECT; 
					}
				}
			}
			
			if (right != null)
			{
				if (right.m_primary.equals("Function") || right.m_primary.equals("StaticFunction") || left.m_primary.equals("MemberFunction")) {
					right = Type.get("iglang.util.igDelegate");	// ?? should this even exist
				}
				
				// This is the only case in which type erasure is permitted
				if (right.m_scope != null) {
					if (right.m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_CLASS ||
						right.m_scope.getScopeType() == IGScopeItem.SCOPE_ITEM_INTERFACE) {
						if (source.isTemplatedAny(1)) {
							right = Type.OBJECT;
						}
					}
				}
			}
			
			String key = left + "|" + right;
			if (!m_existing.containsKey(key)) {
			
				m_existing.put(key, t);
				m_lefts.add(left);
				m_rights.add(right);
				
				return true;
			}
			
			return false;
		}	
	}
}