/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;


import java.util.*;

public class IGContext
{

	HashMap<Type,Type> m_replace = new HashMap<Type,Type>();
	private boolean m_has_replace = false;
	private boolean m_cot = false;
	
	
	public void clearTypeReplace() {
		m_replace = new HashMap<Type,Type>();
		m_has_replace = false;
	}
	
	public void setCollapseObjectTemplateTypes(boolean v )
	{
		m_cot = v;
	}	
	
	public Type getType(IGSource s) {
		return resolveType(s.getInstanceDataType());

/*
		ArrayList<IGTemplateParameter> tpl = s.m_template_parameters;

		if (tpl.size() == 0) {
			return resolveType(s.getInstanceDataType());
		}
		else 
		{
			Type t0 = tpl.get(0).type;
			Type t1 = (tpl.size() == 1) ? null : tpl.get(1).type;
			return resolveType(Type.get(s.getInstanceDataType().m_primary, t0, t1));
		}
*/
	}
	
	public void addTypeReplace(Type pattern, Type replace)
	{
		m_replace.put(pattern, replace);
		m_has_replace = true;
	}
	
	public Type resolveType(Type type) {
		return _resolveType(type, false);
	}

	public Type resolveTypeDoNotCollapse(Type type)
	{
		return _resolveType(type, true);
	}

	// THIS IS INCREDIBLY SIMILAR TO IGResolver
	
	private Type _resolveType(Type type, boolean do_not_collapse)
	{
		if (type == null) {
			return null;
		}
	
		// so this has an early out if no replacements are desired
		// should we bother?
		if (!m_cot && (!m_has_replace || type == null)) {
			return type;
		}
		
		
		if (m_replace.containsKey(type)) {
			return m_replace.get(type);
		}	
				
		// no need to molest int, uint, double, void, bool
		if (type.isPrimitive())
		{
			return type;
		}
		else if (type.m_primary.equals("<wrapper>")) 
		{
			Type left  = _resolveType(type.m_left, do_not_collapse);
			Type right = _resolveType(type.m_right, do_not_collapse);
				
			return Type.get("<wrapper>", left, right);
		}
		else if (type.isFunctionOrTuple()) 
		{
			Type right = _resolveType(type.m_right,do_not_collapse);
			
			
			int count = type.m_left.getMinimumParameters();	//Integer.parseInt(type.m_left.m_primary.substring(1));
			
			
			ArrayList<Type> list = Type.allocTempVector();	//new Vector<Type>();
			for (Type t : type.m_left.m_types) {
				list.add(_resolveType(t,do_not_collapse));
			}
			
			type = Type.get(type.m_primary, Type.get(list, count), right);
			Type.freeTempVector(list);
			
		}
		else
		{	
			IGScopeItem old_scope = type.m_scope;
		
			if (type.m_left != null) 
			{
				// handle Object, Map, Array, Vector
				Type left  = _resolveType(type.m_left,do_not_collapse);
				Type right = _resolveType(type.m_right,do_not_collapse);
				
				if (m_cot) {

				
					if (!do_not_collapse) 
					{
						IGSource template_source = (IGSource)type.m_scope;

						if (left != null && left.isObjectInstance() && template_source.isTemplatedAny(0)) {
							left = Type.OBJECT;	
						}


						if (right != null && right.isObjectInstance() && template_source.isTemplatedAny(1)) {
							right = Type.OBJECT;
						}
					}
					
					if (left != null &&  (left.m_primary.equals("Function") || left.m_primary.equals("StaticFunction"))) {
						left = Type.get("iglang.util.igDelegate");
					}
					
					if (right != null &&  (right.m_primary.equals("Function") || right.m_primary.equals("StaticFunction"))) {
						right = Type.get("iglang.util.igDelegate");
					}
				}
			
				type = Type.get(type.m_primary, left, right);
			}
			
			type.m_scope = old_scope;
		}
		
		return type;
	}
}