/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

public class IGTokenSource
{
	String m_file;
	char [] m_data;
	
	public IGTokenSource(String file, char [] data) {
		m_file = file;
		m_data = data;
	}
	
	public int getLineFromPosition(int position) {
		int line_number = 0;
		for (int i = 0; i < position; i++) {
			if (m_data[i] == '\n') {
				line_number ++;
			}
		}	
		return line_number;
	}
	
	public int getColumnFromPosition(int position) {
		int last_line_start = 0;
		for (int i = 0; i < position; i++) {
			if (m_data[i] == '\n') {
				last_line_start = i + 1;
			}
		}	
		
		return position - last_line_start;
	}
}