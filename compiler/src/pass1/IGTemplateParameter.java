
package pass1;

public class IGTemplateParameter 
{

	public String name;

	// the token in the classes type ig "T1"
	public Token token;

	// the package contains this template
	public IGScopePath parent;

	// the internal type that that resolve to
	public Type  type;			//"iglang.T1"


	public int template_action;  // 0 == enum, 1 == class, 2 = interface

	// the constraint put on the templating type
	public Type  extends_type;	

	public IGTemplateParameter(Token t, IGScopePath parent, Type ty, int action, Type extends_ty) {
		name = t.m_value;
		token = t;
		type = ty;
		this.parent   = parent;
		extends_type = extends_ty;  // 
		template_action = action;
	}
}