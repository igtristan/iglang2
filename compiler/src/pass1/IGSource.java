/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

/*
	A IGSource represents a logical compilation unit.  
	Either:
		- SCOPE_ITEM_CLASS        (standard class, that can have 1 base class and implement many interfaces)
		- SCOPE_ITEM_ENUM         (wrapper over a simple int) 
		- SCOPE_ITEM_INTERFACE    (defines required functionality)
		
	Each component of an IGSource file has the same name of the file contained within it
	ie.   class Animation  will be contained in the file Animation.ig
	This name is defined in the field:
		getName()
		
	
	





*/



import java.util.Vector;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;

public class IGSource extends IGScopeItem
{
	public static final int MEMBER_DEFAULT = 0;
	public static final int MEMBER_GET = 1;
	public static final int MEMBER_SET = 2;


	public static final int STATIC 		= 1;
	public static final int FINAL  		= 2;		// const also maps to this
	public static final int PUBLIC 		= 4;
	public static final int PROTECTED 	= 8;
	public static final int VIRTUAL 	= 16;
	public static final int OVERRIDE    = 32;
	public static final int PRIVATE     = 64;
	public static final int INLINE      = 128;
	public static final int INTERNAL    = 256;
	public static final int SET         = 512;
	public static final int GET			= 1024;
	public static final int ENUM_VALUE	= 2048;
	public static final int STUB        = 4096;
	//public static final int REF_COUNTED = 8192;

	public boolean m_failed_pass0 = false;
	
	private Hashtable<Token, Annotation> m_annotations = new Hashtable<Token, Annotation>();
	private Hashtable<Object, Token>     m_annotation_object_to_token = new Hashtable<Object, Token>();
	

	public  String m_src_path;
	private String m_dst_path;				// excludes final extension
	private String m_dst_root;
	

	
	// a unique String representing this source file
	// for use later
	public String            m_guid = pass2.Util.getGUID();
	
	// what package is this source file a member of
	//public ArrayList<String>   m_file_based_package = null;
	public IGScopePath         m_package = null;
	
	// what is the name of the primary object in this file
	public Token			   m_name = null;

	// what is the fully qualified path of this source
	// ie  blah.blah.ThisClass
	public IGScopePath         m_scope_path = null;
	

	
	// what is the type of the primary object in this file
	//public static final int SCOPE_ITEM_CLASS     = 1;
	//public static final int SCOPE_ITEM_INTERFACE = 2;
	//public static final int SCOPE_ITEM_ENUM      = 3;
	public int              m_type;
	
	public ArrayList<IGTemplateParameter> m_template_parameters = new ArrayList<IGTemplateParameter>();
	
	////////////////////////////////////
	// Modifiers for protect level 
	//////////////////////////////////
	public int m_modifiers = 0;

	// what scopes are included
	public ArrayList<IGScopePath>    m_scopes     = new ArrayList<IGScopePath>();
	
	// what members does this container have
	public ArrayList<IGMember>       m_members    = new ArrayList<IGMember>();
	public HashMap<String, IGMember> m_member_map = new HashMap<String, IGMember>();

	

	//tokens contained in the file
	public Token [] m_token_list = null;
	
	// the AST node representing this entire file
	public IGNode            m_root_node = null;
	

	/////////////////////////////////////////////////
	// Fields pertaining to a m_type == SCOPE_ITEM_CLASS
	/////////////////////////////////////////////////
	public Type				 m_class_extends = null;
	public ArrayList<Type>   m_class_implements = new ArrayList<Type>();

	/////////////////////////////////////////////////
	// Fields pertaining to a m_type == SCOPE_ITEM_ENUM
	/////////////////////////////////////////////////
	public Type				 m_enum_extends = null;

	//////////////////////////////////////////////////////
	// Derrived
	/////////////////////////////////////////////////////
	public Type              m_instance_data_type = null;
	public Type              m_source_data_type = null;




	//////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////

	public static IGSource createPlaceholderEnum(IGScopePath pkg, Token name) {
		IGSource stub = new IGSource();
		stub.m_enum_extends = Type.INT;
		stub.m_package      = pkg;
		stub.setNameAndType(name, IGScopeItem.SCOPE_ITEM_ENUM);
		stub.generateInternalTypes();
		return stub;
	}

	public void stubExtendClass(IGSource other) {
		m_class_extends = other.getInstanceDataType();
		m_type = IGScopeItem.SCOPE_ITEM_CLASS;
	}

	public void stubImplementsClass(IGPackage root, IGSource other) {
		m_class_implements.add(other.getInstanceDataType());
		m_class_extends = Type.OBJECT;
		m_type = IGScopeItem.SCOPE_ITEM_CLASS;

		IGResolver resolver = new IGResolver();
		resolver.init(root, other);
		resolver.addReplacement(other.getInstanceDataType(), this.getInstanceDataType());

		// basically declare that this has all the things that the interface does
		for (IGMember m : other.m_members) {
			this.addMember(m.cloneReplacing(this, resolver));
		}
	}


	public void stubExtendEnum(IGPackage root, IGSource other) 
	{
		m_enum_extends = other.m_enum_extends;
		m_type = IGScopeItem.SCOPE_ITEM_ENUM;

		IGResolver resolver = new IGResolver();
		resolver.init(root, other);
		resolver.addReplacement(other.getInstanceDataType(), this.getInstanceDataType());

		for (IGMember m : other.m_members) {
			if (m.hasModifier(IGMember.PUBLIC)) {
				this.addMember(m.cloneReplacing(this, resolver));
			}
		}
	}


	static class Annotation {
		public Object object;
		public int       type;
		public Annotation(Object o, int t) {
			object = o;
			type = t;
		}
	}

	public Object getAnnotationObject(Token t) {
		return m_annotations.get(t).object;
	}

	public int getAnnotationType(Token t) {
		if (!m_annotations.containsKey(t)) {
			return IGConsts.ANNOTATION_UNKNOWN;
		}	
		return m_annotations.get(t).type;
	}

	public void addAnnotation(Token t, int type, Object o) {
		m_annotations.put(t, new Annotation(o, type));
		if (o != null) {
			m_annotation_object_to_token.put(o, t);
		}
	}
	
	public Token getTokenForAnnotationObject(Object o) {
		return m_annotation_object_to_token.get(o);
	}
	

	/**
	 * getDestinationPath()
	 *
	 * Return the path that this resource is expected to be compiled to.
	 * ie.  a class in  src/iglang/Object.ig  if its compilation destination was 
	 * set to dst/  the expected value would be dst/iglang/Object 
	 * note: no extension is specified since it varies on render target
	 **/ 

	public String getDestinationPath() {
		return m_dst_root + m_dst_path;
	}

	public String getPartialDestinationPath()
	{
		return m_dst_path;
	}


	
	public final boolean inherits(IGSource s) {
		
		if (this == s) {
			return true;
		}
		
		if (m_class_extends != null && m_class_extends.m_scope != null) {
			return ((IGSource)m_class_extends.m_scope).inherits(s);
		}
		
		return false;
	}
	
	public final boolean samePackage(IGSource s) {
		return s.m_package.compareTo(m_package) == 0;
	}
	
	public IGMember getMember(String name) {
		// get a straight up named member
		
		for (int i = m_members.size() - 1; i >= 0; i--) 
		{
			IGMember s = m_members.get(i);
			// || s.m_name.m_value.equals(name + (setter_context ? "__set" : "__get"))
			if (s.m_name.m_value.equals(name)) {
				return s;
			}
		}
		
		return null;
	
	}
	

	public boolean isInterface() {
		return getScopeType() == SCOPE_ITEM_INTERFACE;
	}

	public boolean isClass() {
		return getScopeType() == SCOPE_ITEM_CLASS;
	}

	public boolean isEnum() {
		return getScopeType() == SCOPE_ITEM_ENUM;
	}
	
	//////////////////////////////////////
	// START INTERFACE IGScopeItem
	//////////////////////////////////////

	public int       getScopeType()
	{
		return m_type;
	}
	
	public  IGScopePath getScopePath()
	{
		return m_scope_path;
	}
	
	public IGScopeItem getScopeChild(String name, boolean setter_context)
	{
		String alt_name = name + (setter_context ? "__set" : "__get");
		
		return getScopeChildWithAltName(name, setter_context, alt_name);
	}
	
	
	private IGScopeItem getScopeChildWithAltName(String name, boolean setter_context, String alt_name)
	{
		final int member_count = m_members.size();
		for (int i = 0; i < member_count; i++) 
		{
			IGMember  s = m_members.get(i);
			if (s.m_name.m_value.equals(name) || s.m_name.m_value.equals(alt_name)) {
				return s;
			}
		}
		
		if (m_class_extends != null && m_class_extends.m_scope != null) {
			return ((IGSource)m_class_extends.m_scope).getScopeChildWithAltName(name, setter_context, alt_name);
		}
		
		return null;
	}
	
	
	
	public Type               getDataType(Type relative) 
	{

		if (null == m_source_data_type) {
			throw new RuntimeException("Source was not initialized: " + getScopePath());
		} 
		return m_source_data_type;
	}
	
	public String toString() {
		return getScopePath().toString();
	}
	


	
	/*
	 * Return the type that would result from creating a new instance 
	 * of the object contained in the source file.
	 */
	
	public Type    getInstanceDataType()
	{
		if (null == m_instance_data_type) {
			throw new RuntimeException("Source was not initialized: " + getScopePath());
		}
		return m_instance_data_type;
	}
	
	////////////////////////////////////////////////
	// END INTERFACE IGScopeItem
	////////////////////////////////////////////////
	
	public IGScopeItem lookup(String key, boolean setter_context)
	{
		// uhh.. this needs to be iterated forward for some reason?
		int len = m_members.size();
		for (int i = 0; i < len; i++)
		{
			IGMember m = m_members.get(i);
			if (m.m_name.m_value.equals(key) || m.m_name.m_value.equals(key + (setter_context ? "__set" : "__get"))) {
				return m;
			}
		}
		
		
		if (m_class_extends != null && m_class_extends.m_scope != null) {
			return ((IGSource)m_class_extends.m_scope).lookup(key, setter_context);
		}
	
		return null;
	}
	
	public void compilationFailed(Token stok, String text)
	{
		System.out.println(stok.m_value);
		System.out.println(stok.getFile() + ":" + stok.getLine() + "> " + text);
		m_failed_pass0 = true;
	}
	
	public void compilationFailed() {
		m_failed_pass0 = true;
	}
	
	public void compilationFailed(String text) 
	{
		util.Log.logErrorEcho(m_src_path, text);
	
		//System.out.println(text);
		m_failed_pass0 = true;
	}
	
	
	// public boolean shouldOutput(String compilation_target) {
	
	// 	if (!compilation_target.equals("as3") && getName().equals("IGAS3")) {
	// 		return false;
	// 	}
	
	// 	if (m_token_list[0].m_whitespace.startsWith("/*NO-OUTPUT*/")) {
	// 		return false;
	// 	}
	// 	else
	// 	{
	// 		return true;
	// 	}
	// }

	// public boolean isStub()
	// {
	// 	if (m_token_list[0].m_whitespace.startsWith("/*NO-OUTPUT*/")) {
	// 		return true;
	// 	}
	// 	else
	// 	{
	// 		return false;
	// 	}
	// }
	
	public boolean canOptimizeMemberFunctionOut(String name)
	{
		//if (isStub()) {
		//	return false;
		//}
		
		return !hasUserMemberFunction(name);
	}

	private IGSource() {

	}
	
	/*
	 * Standard constructor
	 */
	
	public IGSource(
			List<String> file_based_package, 
			String src_path, 
			String dst_root,
			String dst_path, 
			String expected_component) 
	{
		m_src_path = src_path;
		m_dst_root = dst_root;
		m_dst_path = dst_path;
		m_package    = new IGScopePath(file_based_package);
		m_name       = new Token(IGConsts.TOK_ID, expected_component);
		m_scope_path = new IGScopePath(m_package, m_name);
	}
	
	public String getName() {
		return m_name.m_value;
	}
	/*
	 * Add a template parameter to this class.
	 * each token will be of the format T1,T2....
	 */
	
	public void addTemplateParameter(Token t, IGScopePath pkg, Type ty, int action, Type extends_ty) {
		m_template_parameters.add(new IGTemplateParameter(t, pkg, ty, action, extends_ty));
	}
	
	public boolean isTemplatedClass() {
		return m_template_parameters.size() > 0;
	}
	
	public int getTemplatedParameterCount() {
		return m_template_parameters.size();
	}

	public boolean isTemplatedAny(int idx) 
	{
		if (idx >= m_template_parameters.size()) {
			return false;
		}

		// todo update this
		return m_template_parameters.get(idx).extends_type == Type.ANY;
	}
	
	
	/*
	 * Set a modifier on the class, interface or enum specified by this source file
	 */
	
	public void addModifier(int m) {
		m_modifiers |= m;
	}
	
	/*
	 * Set the list of tokens that represent this file
	 */
	
	public void setTokenList(Token [] t) {
		m_token_list = t;
	}
	
	/*
	 * Can the object contained within this source file be assigned
	 * to the other specified type.
	 */
	
	public boolean assignable(Type this_type, Type other)
	{
		int st = getScopeType();
		if (other == Type.NULL && st != SCOPE_ITEM_ENUM) {
			return true;
		}	
	
		// check the class we extend if that matches the type in question
		if (m_class_extends != null && m_class_extends.assignable(other))  {
			return true;
		}
		
		// look through our implements list and do the same
		for (Type type : m_class_implements) 
		{
			if (type.assignable(other)) 
			{
				return true;
			}
		}
		
		return false;
	}
	
	public void setNameAndType(Token name, int type)
	{
		m_name = name;
		m_type = type;
		
		// set the default extends for the class
		if (type == SCOPE_ITEM_CLASS) { 
			m_class_extends = Type.OBJECT;
		}
		
		m_scope_path = new IGScopePath(m_package, name);
		
		if (m_scope_path.toString().equals("iglang.Object")) {
			m_class_extends = null;
		}
	}

	/**
	 * Called in FileReader after all fileeds have been et
	 */
	public void generateInternalTypes() 
	{
		{
			// generate a type no matter what missing templated parameters
			Type  tt        = Type.get(m_scope_path.toString());
			tt.m_scope = this;

			// if there's templating parameters generate new type
			if (m_template_parameters.size() > 0) {
				Type t0 =  m_template_parameters.get(0).type;
				Type t1 = null;
				if (m_template_parameters.size() > 1) {
					t1 = m_template_parameters.get(1).type;
				}
				tt = Type.get(m_scope_path.toString(), t0, t1);
			}

			tt.m_scope     = this;
			tt.m_qualified = true;

			m_instance_data_type = tt;
		}

		{
			Type tt        = null;

			if (m_template_parameters.size() == 0) {
				 tt        = Type.get("source:" + m_scope_path.toString());
			}
			else {
				Type t0 =  m_template_parameters.get(0).type;
				Type t1 = null;
				if (m_template_parameters.size() > 1) {
					t1 = m_template_parameters.get(1).type;
				}
				tt = Type.get("source:" + m_scope_path.toString(), t0, t1);
			}

			tt.m_scope     = this;
			tt.m_qualified = true;

			m_source_data_type = tt;
		}

		//System.out.println("Generated internal type: " + m_package + " " + m_instance_data_type + " " + m_source_data_type);
	}
	
	public void addImplements(Type t) {
		m_class_implements.add(t);
	}
	
	public void setExtends(Type t) {
		m_class_extends = t;
	}
	
	public void setPackage(IGScopePath p) {
		m_package = p;
	}
	
	public void addScope(IGScopePath p) {
		m_scopes.add(p);
	}
	
	public boolean hasMemberWithName2(String n0, int n0_type)
	{	
		for (int i = m_members.size() - 1; i >= 0; i --) {
		
			IGMember  member      = m_members.get(i);
			String    n1          = member.m_clean_name;
			int       n1_type     = member.m_clean_type;
				
			if (n1.equals(n0)) {
				if ((n0_type == MEMBER_GET && n1_type == MEMBER_SET) ||
					(n0_type == MEMBER_SET && n1_type == MEMBER_GET)) {
				
					// this is fine	
				}
				else 
				{
					return true;
				}
			}
		}
		
		return false;
	}
	
	public boolean hasMemberWithName(String name)
	{
		for (int i = m_members.size() - 1; i >= 0; i --) {
			if (m_members.get(i).m_name.m_value.equals(name)) {
				return true;
			}
		}
		
		return false;
	}
	
	
	public void addMember(IGMember m) {
		if (m.m_name == null) {
			throw new RuntimeException("Name must be specified before adding to IGSource");
		}
		m_members.add(m);
		m_member_map.put(m.m_name.m_value, m);
		if (m.m_parent_source != this) {
			throw new RuntimeException("Redundant code should have already madae this happen");
		}
		m.m_parent_source = this;
		
		{
			String member_name = m.m_name.m_value;
			boolean member_is_setter = member_name.endsWith("__set");
			boolean member_is_getter = member_name.endsWith("__get");
			
			int     n1_type = MEMBER_DEFAULT;
			if      (member_is_getter) { n1_type = MEMBER_GET; }
			else if (member_is_setter) { n1_type = MEMBER_SET; }		
			String  n1 = (member_is_setter || member_is_getter) ? member_name.substring(0, member_name.length() - 5) : member_name;

			m.m_clean_name = n1;
			m.m_clean_type = n1_type;
		
		}
	}
	
	
	public boolean hasUserMemberFunction(String name) 
	{
		for (int i = m_members.size() - 1; i >= 0; i--) {
		
			IGMember m = m_members.get(i);
			if (m.hasModifier(IGMember.STUB)) {
				continue;
			}
		
			if (m.m_name.m_value.equals(name) && !m.hasModifier(IGMember.STATIC)) {
				return true;
			}
		}
		
		return false;		
	}
	
	/* Returns whether this class contains a constructor.
	 * The name of the constructor internally was mangled to <constructor>
	 */
	
	public boolean hasConstructor() 
	{
		return null != getConstructor();
	}

	/* Returns the member corresponding to the constructor
	 * The name of the constructor internally was mangled to <constructor>
	 */
	
	public IGMember getConstructor()
	{
		for (int i = m_members.size() - 1; i >= 0; i--) {
			IGMember m = m_members.get(i);
			if (m.m_name.m_value.equals("<constructor>")) {
				return m;
			}
		}
		return null;
	}
	
	/*
	 * Get the first constructor found.
	 * In the case that this class doens't define one
	 */
	 
	public IGMember getFirstConstructor()
	{
		IGMember c = getConstructor();
		if (null != c) {
			return c;
		}

		IGSource base = (IGSource)m_class_extends.m_scope;	
		if (base == null) {
			return null;
		}
		return base.getFirstConstructor();
	}
	
	/**
	 * Get the first to string found.
	 * This is used by AS3
	 */
	
	public IGMember getFirstToString()
	{
		for (int i = m_members.size() - 1; i >= 0; i--) {
			IGMember m = m_members.get(i);
			if (   m.m_type == IGMember.TYPE_FUNCTION
				&& m.m_name.m_value.equals("toString") && 
				  !m.hasModifier(IGMember.STATIC)) {
				return m;
			}
		}
		
		IGSource base = (IGSource)m_class_extends.m_scope;
		
		if (base == null) {
			return null;
		}
		return base.getFirstToString();
	}
	
	public IGSource getExtendsSource() 
	{
		if (m_class_extends == null) {
			return null;
		}
		
		IGSource base = (IGSource)m_class_extends.m_scope;
		return base;
	}
	
	
	public void debug()
	{
		System.out.println("source");
		System.out.println("\t" + m_src_path);
		System.out.println("\t" + m_name.m_value);
		System.out.println("package");
		m_package.debug();
		System.out.println("scopes-included");
		for (IGScopePath sp : m_scopes) {
			sp.debug();
		}
		System.out.println("members");
		for (IGMember m : m_members) {
			m.debug();
		}
	}
	
}
