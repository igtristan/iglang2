/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

/*
 * This is a Trie data structure that contains types for nodes specified within it
 * the first 44 ascii characters are discared from the key since IDs cannot be constructed
 * with those characters. Note  '(' and ')' are not used in internal type descriptions
 * a function pointer Function<(int,int):void> would be represented by  Function<@2<int,int>,void>
 * The @2 stipulates the number of required parameters.
 *
 * The theoretical minimum trie table size would be dictated by 2 * 26 + 10 + '<' + ','+ '>' + '.' + '@'
 *      67 entries
 *  We can get close by using the 79 characters in the ASCII range of 44 to 123
 */



public final class IGTypeDatabaseEntry
{
	private IGTypeDatabaseEntry [] m_trie;
	public Type                    m_value = null;

	public IGTypeDatabaseEntry() {
		m_trie = new IGTypeDatabaseEntry[123 - 44];	// 26+26+10  = 62 min
	}

	public IGTypeDatabaseEntry poke(String key, int offset, int length) 
	{
		if (offset == length) {
			return this;
		}

		int c = key.charAt(offset) - 44;
		if (m_trie[c]  == null) {
			m_trie[c] = new IGTypeDatabaseEntry();
		}
		return m_trie[c].poke(key, offset + 1, length);
	}

	public IGTypeDatabaseEntry poke(Type type) {
		String key = type.toString();
		return poke(key, 0, key.length());
	}

	public IGTypeDatabaseEntry poke(String key) {
		return poke(key, 0, key.length());
	}
	
	public IGTypeDatabaseEntry poke(char d) {
		int c = d - 44;
		if (m_trie[c]  == null) {
			m_trie[c] = new IGTypeDatabaseEntry();
		}
		return m_trie[c];
	}
}
