/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

public final class Token
{
	public int m_type;						// the type of the token
	
	// terminal escape codes
	// http://misc.flogisoft.com/bash/tip_colors_and_formatting
	
	public String m_value;					// the value of this token
	public String m_whitespace = "";		// the whitespace that preceeded this token

	public short   m_line        = 0;		// the line in the source file the token appear at
	public boolean m_output      = true;
	public boolean m_discarded   = false; 

	// the index within the token stream
	// this is assigned when the token is officially read/expected by IGFileReder										
	public int     m_token_index = -1;		
	
	// might be able to get rid of this?
	public IGNode m_node = null;			// used by some compilation targets
	
	// the stream that the token was found in
	public IGTokenSource m_token_source = null;
	
	// position within the data stream
	public int     m_position = 0;			// used for error messaging

	
	public void copy(Token other)
	{
		m_type       = other.m_type; 
		m_value      = other.m_value;
		m_whitespace = other.m_whitespace;
		
		m_line = other.m_line;
		// m_output  -- ignored (used in very last output steps)
		// m_discard -- ignored (used in very last output steps)
		
		// m_token_index -- assigned at the very end of IGFileReader
		// m_node        -- assigned in IGValidator

		m_token_source = other.m_token_source;
		m_position = other.m_position;
	}
	
	
	
	// this is used in two places in the cpp outputter
	public void discard() {
		m_discarded = true;
	}
	
	
	public Token() {
	}
	
	public Token(int type, String value)
	{
		m_type = type;
		m_value = value;
	}
	
	public Token(int type, String value, String ws)
	{
		m_type = type;
		m_value = value;
		m_whitespace = ws;
	}
	
	
	public static int getIntValue(Token t)
	{
		if (t.m_type == IGLexer.LEXER_INT) {
			return (int)Long.parseLong(t.m_value);
		}	
		else if (t.m_type == IGLexer.LEXER_HEX) {
			return (int)Long.parseLong(t.m_value.substring(2), 16);
		}	
		return 0;
	}
	
	public static Token createInteger(int value) {
		Token t = new Token();
		//t.m_file = "<unknown>";
		t.m_value = Integer.toString(value);
		t.m_type = IGLexer.LEXER_INT; 
		t.m_line = -1;
		//t.m_column = -1;
		t.m_whitespace = "";
		
		return t;
	}
	
	public static Token createId(String value) {
		Token t = new Token();
		//t.m_file = "<unknown>";
		t.m_value = value;
		t.m_type = IGLexer.LEXER_ID; 
		t.m_line = -1;
		//t.m_column = -1;
		t.m_whitespace = "";
		
		return t;
	}
	
	public String getUniqueVariableId()
	{
		return "__ig_var_" + m_token_index;
	}
	
	// create a specific variable for use as an iterator within an asm output
	public static Token createUniqueVariableId(Token other) {
		Token t = new Token();
		t.copy(other);
		t.m_token_index = other.m_token_index;
		t.m_value = other.getUniqueVariableId();
		t.m_type = IGLexer.LEXER_ID; 

		// checks out
		//System.err.println("unique variable id: " + t.m_value);

		return t;
	}
	

	
	public void init(int type, String type_string, IGTokenSource source, short line)
	{
		m_type = type;
		m_value = type_string;
		m_line = line;
		//m_column = column;
		//m_file = file;
		m_token_source = source;
	}
	
	public String all()
	{
		return m_whitespace + m_value;
	}
	
	public char [] getData() {
		return m_token_source.m_data;
	}
	
	public String getFile() {
		return m_token_source.m_file;
	}
	
	public int getLine() {
		return m_line;
	}	
	
	
	/**
	 *
	 * Print to the error stream with the text.
	 */
	
	
	public void printError(String text)
	{
		//if (text != null) {
		//throw new Error("e");
		//}
		String file = m_token_source.m_file;
		int   line = m_line;
		int column = m_token_source.getColumnFromPosition(m_position);
	
	
		//int column = 0;
		Token stok = this;
		
		System.err.println(file + ":" + line + ": " + text);
		
		
		String buffer = "";
		String whitespace = "";
		
		char [] cdata = getData();
		
		if (cdata == null) {
			System.err.println(stok.m_value);
		}
		else
		{
			// backtrace to the beginning of the line
			int position = m_position;
			while (position >= 0 && cdata[position] != '\n') {
				position --;
			}
			position++;
			
			
			while (position < cdata.length && cdata[position] != '\n') {
				buffer += "" + getData()[position];
				if (position < m_position) {
					whitespace += (cdata[position] == '\t') ? "\t" : " ";
				}
				position ++;
				column ++;
			}

			if (m_type == IGConsts.TOK_EOF) {
				whitespace += " ";
			}

			System.err.println(buffer);			
			System.err.println(whitespace + "\u001B[31m" + "^" + "\u001B[0m");		// neat unicode coloring
			whitespace += "^";
		}
		
		util.Log.logError(file, text, line, column + ((m_type == IGConsts.TOK_EOF) ? 1 : 0), buffer, whitespace);
	}
	
	
	/**
	 *
	 * Print to the debug stream
	 */
	
	public void printDebug(String text)
	{
		String file = m_token_source.m_file;
		int   line = m_line;
		int column = m_token_source.getColumnFromPosition(m_position);
		
		Token stok = this;
		
		System.err.println(file + ":" + line + ": " + text);
		
		char [] cdata = getData();
		
		if (cdata == null) {
			System.err.println(stok.m_value);
		}
		else
		{
			// backtrace to the beginning of the line
			int position = m_position;
			while (position >= 0 && cdata[position] != '\n') {
				position --;
			}
			position++;
			
			String whitespace = "";
			while (position < cdata.length && cdata[position] != '\n') {
				System.err.print(cdata[position]);
				if (position < m_position) {
					whitespace += (cdata[position] == '\t') ? "\t" : " ";
				}
				position ++;
				column ++;
			}
			
			if (m_type == IGConsts.TOK_EOF) {
				whitespace += " ";
			}
			
			
			System.err.println();
			System.err.println(whitespace + "\u001B[31m" + "^" + "\u001B[0m");		// neat unicode coloring
		}
	
	}
	
	
		
	public @Override String toString() {
		String file = m_token_source.m_file;
		int   line = m_token_source.getLineFromPosition(m_position);
		int column = m_token_source.getColumnFromPosition(m_position) +  ((m_type == IGConsts.TOK_EOF) ? 1 : 0);
		
		return "[Token: " + line + ":" + column  + " " + m_value + "]";
	}
}