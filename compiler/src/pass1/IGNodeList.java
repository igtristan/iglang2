/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

import java.util.ArrayList;

public final class IGNodeList
{
	IGNode [] m_entries = null;
	short     m_capacity = 0;
	short     m_size     = 0;
	
	public int size() {
		return m_size;
	}
	
	public IGNode get(int idx) {
		if (idx >= m_size) {
			throw new RuntimeException("Invalid index");
		}
		return m_entries[idx];
	}
	
	public void add(IGNode n) 
	{
		if (null == n) {
			throw new RuntimeException("Cannot add null nodes to a node list");
		}
		
		short sz = m_size;
		if (sz + 1 > m_capacity) {
			if (m_entries == null) {
				m_entries = new IGNode[4];
				m_capacity = 4;
			}
			else {
				IGNode [] old = m_entries;
				int       cap = m_capacity;
				
				m_entries = new IGNode[2 * cap];
				for (int i = cap - 1; i >= 0; i--) {
					m_entries[i] = old[i];
				}	

				
				m_capacity = (short)(2 * cap);
			}
		}
	
		m_entries[sz] = n;
		m_size ++;
	}
	
	
	/**
	 * We for the most part always remove elements from the end of the list
	 */
	
	public void remove(IGNode n) {
		//int idx = test.indexOf(n);
		//test.remove(idx);
		
		int found = -1;
		
		for (int i = m_size - 1; i >= 0; i--) {
			if (m_entries[i] == n) {
				found = i;
				break;
			}
		}
		
		if (found == -1) {
			throw new RuntimeException("Tried to remove node that wasn't part of list");
		}
		
		m_size --;
		for (int i = found; i < m_size; i++) {
			m_entries[i] = m_entries[i + 1];
		}
		
	}

}