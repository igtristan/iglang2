package pass1;

import java.util.*;
import java.io.*;

public class IGIRModule
{

	public static final int CLASS = 0;
	public static final int INTERFACE = 1;
	public static final int ENUM = 2;


	public int    m_storage;
	public Type   m_type;					// ig.  iglang.Object
	public List<IGIRModule>     m_extends = new ArrayList<IGIRModule>();
	public List<IGIRModule>     m_implements = new ArrayList<IGIRModule>();

	// should this be split into categories?
	public List<Named>          m_named            = new ArrayList<Named>();
	public List<Function>       m_member_functions = new ArrayList<Function>();
	public List<Variable>       m_member_variables = new ArrayList<Variable>();
	public List<Function>       m_static_functions = new ArrayList<Function>();
	public List<Variable>       m_static_variables = new ArrayList<Variable>();



	public Collection<IGIRModule.Function> getVTable() {
		HashMap<String, Function> prep = new HashMap<String,Function>();
		getVTable(prep);


		return prep.values(); 
	}


	private void getVTable(Map<String,Function> prep)
	{
		IGIRModule ext = getExtends();
		if (ext != null) {
			ext.getVTable(prep);
		}


		int cnt = m_member_functions.size();
		for (int i = 0; i < cnt; i++) {
			Function fn = m_member_functions.get(i);
			prep.put(fn.m_name, fn);
		}
	}

	public boolean isClass() {
		return m_storage == CLASS;
	}
	public Named resolve(String name) 
	{
		int cnt = m_named.size();
		for (int i = 0; i < cnt; i++) {
			if (m_named.get(i).m_name.equals(name)) {
				return m_named.get(i);
			}
		}

		if (m_extends.size() > 0) {
			return m_extends.get(0).resolve(name);
		}

		return null;
	}

	public IGIRModule getExtends() 
	{
		if (m_extends.size() > 0) {
			return m_extends.get(0);
		}
		return null;
	}

	public void output(PrintWriter pw) throws IOException
	{
		if (m_storage == CLASS)     { pw.println("class     " + m_type);  }
		if (m_storage == INTERFACE) { pw.println("interface " + m_type);  }
		if (m_storage == ENUM)      { pw.println("enum      " + m_type);  }
		for (IGIRModule e : m_extends) {
			pw.println("extends " + e.m_type);
		}
		for (IGIRModule e : m_implements) {
			pw.println("implements " + e.m_type);
		}

		pw.println();
		pw.println("STATIC");
		pw.println("######");
		pw.println();


		for (Variable v : m_static_variables) {
		//	if (v.m_final) {
			v.output(pw);
		//	}
		}

		for (Function f : m_static_functions) {
		//	if (f.m_final) {
			f.output(pw);
		//	}
		}


		pw.println();
		pw.println("MEMBER");
		pw.println("######");
		pw.println();

		for (Variable v : m_member_variables) {
		//	if (!v.m_final) {
				v.output(pw);
		//	}
		}

		for (Function f : m_member_functions) {
		//	if (!f.m_final) {
				f.output(pw);
		//	}
		}
	}

	public static class Named {
		public IGIRModule m_module;
		public boolean    m_static = false;
		public String     m_name   = null;		
		public boolean    m_final  =false;
		

		public boolean isFinal() {
			return m_final;
		}

		public boolean isStatic() {
			return m_static;
		}

		public String getName() {
			return m_name;
		}


		public void setStatic(boolean b) {
			m_static = b;
		}

		public void setName(String n) {
			m_name = n;
		}

		public void setFinal(boolean b) {
			m_final =b;
		}

	}


	public static class Function extends Named
	{
	
		//public boolean m_final  = false;
		public int     m_min_parameter_count;
		public int     m_max_parameter_count;
		public Type    m_return_type = Type.VOID;


		public void output(PrintWriter pw) throws IOException {
			pw.println("function  " + m_name);
			pw.println("final     " + m_final);
			pw.println("static    " + m_static);
			pw.println("type      " + m_return_type);
			//pw.println("value     " + m_initial_value);
			pw.println("params:   " + m_min_parameter_count + " => " + m_max_parameter_count);
			pw.println();

			for (int i = 0; i < m_parameters.size(); i++) {
				m_parameters.get(i).output(pw);
			}

			pw.println("CODE");
			for (int i = 0; i < m_ops.size(); i++) {
				pw.println(IGIR.convertToString(m_ops.get(i)));
			}
			pw.println();

		}

		public List<Variable> m_parameters = new ArrayList<Variable>();
		public List<Variable> m_variables  = new ArrayList<Variable>();
		public List<IGIROp>   m_ops        = new ArrayList<IGIROp>();


		public void op(IGIROp instruction )
		{ 
			op(instruction, 0);
		}
		
		public void op(IGIROp instruction, int depth) {
			if (depth == 8) {
				throw new RuntimeException("Overflow");
			}
			if (instruction.m_next != null) {
				op(instruction.m_next, depth + 1);
			}

			// this is acceptable for the time being
			//if (m_ops.contains(instruction)) {
			//	throw new RuntimeException("Duplicate Instruction in stream");
			//}

			if (instruction.op == IGIR.NOP && instruction.label == null) {
				return;
			}

			m_ops.add(instruction);
		}

/*
		public void op_dup(IGIROp instruction )
		{ 
			op(instruction, 0);
		}
		
		public void op_dup(IGIROp instruction, int depth) {
			if (depth == 8) {
				throw new RuntimeException("Overflow");
			}
			if (instruction.m_next != null) {
				op_dup(instruction.m_next, depth + 1);
			}


			m_ops.add((IGIROp)instruction.clone());
		}
*/
		public void setParameterCount(int min, int max) {
			m_min_parameter_count = min;
			m_max_parameter_count = max;
		}

		public void setReturnType(Type rt) {
			m_return_type = rt;
		}

		public void label(String label) {
			IGIROp nop = IGIR.nop();
			nop.label = label;
			op(nop);
		}


		public void setPublic() {
			
		}


		public void addParameter(String name, Type t, IGValueRange vr) {
			Variable v = new Variable();
			v.setName(name);
			v.setType(t);
			v.setInitialValue(vr);
			m_variables.add(v);
			m_parameters.add(v);
		}



		public void addMemberDependency(Type module, String name) {

		}

		public void addFunctionDependency(Type module, String name) {

		}

		public void addModuleDependency(Type module) {

		}
	}

	public static class Variable extends Named
	{
		//public IGIRModule m_module;
		//public boolean    m_static = false;
		//public String     m_name = null;
		public Type       m_type = Type.VOID;
		public Type       m_type_full = Type.VOID;
		public IGValueRange m_initial_value = null;


		public void output(PrintWriter pw) throws IOException {
			pw.println("variable  " + m_name);
			pw.println("final     " + m_final);
			pw.println("static    " + m_static);
			pw.println("type      " + m_type);
			pw.println("type-full " + m_type_full);
			pw.println("value     " + m_initial_value);
			pw.println();
		}


		

		public void setPublic() {

		}

		public void setType(Type t) {
			m_type = t;
		}

		public void setFullType(Type t) {
			m_type_full = t;
		}

		public void setInitialValue(IGValueRange vr) {
			m_initial_value = vr;
		}
	}

	public void init(int storage, Type path) {
		m_storage = storage;
		m_type = path;
	}


	private void addNamed(Named n) {
		n.m_module = this;
		m_named.add(n);
	}

	public Function createMemberFunction() {
		Function f = new Function();
		m_member_functions.add(f);
		addNamed(f);
		return f;
	}

	public Variable createMemberVariable() {
		Variable v = new Variable();
		m_member_variables.add(v);
		addNamed(v);
		return v;
	}


	public Function createStaticFunction() 
	{
		Function f = new Function();
		f.setStatic(true);
		m_static_functions.add(f);
		addNamed(f);
		return f;
	}

	public Variable createStaticVariable() 
	{
		Variable v = new Variable();
		v.setStatic(true);
		m_static_variables.add(v);
		addNamed(v);
		return v;
	}




	public void addExtends(IGIRModule t) {
		m_extends.add(t);
	}

	public void addImplements(IGIRModule t) {
		m_implements.add(t);
	}


}