/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

package pass1;

import java.util.*;


/*
 * This class is used for resolving the types used by a specific class
 * taking into account all of the imports used
 */

class IGResolver
{
	public IGResolver() {}
	
	public IGPackage              m_root;
	public ArrayList<IGScopeItem> m_packages = new ArrayList<IGScopeItem>();
	private IGSource              m_source = null;
	private HashMap<Type,Type> m_replacements = new HashMap<Type,Type>();
	
	public boolean init(IGPackage root, IGSource s)
	{
		m_source = s;
		m_root = root;
		
		IGScopePath glob_path = new IGScopePath(s.m_package, new Token(IGConsts.TOK_MUL, "*"));
	
		root.resolve(m_packages, glob_path);
		root.resolve(m_packages, IGScopePath.BUILTINS);
		
		// these are proper scopes
		for (IGScopePath sp: s.m_scopes) {
			root.resolve(m_packages, sp);
		}
		
		return true;
	}
	
	public void addReplacement(Type old, Type curr) {
		m_replacements.put(old, curr);
	}


	public IGScopeItem findScopeItem(String name) {
		int idx = name.indexOf('.');
		if (idx == -1) {
			return findScopeItem(name, -1);
		}

		//System.out.println("Checkin split: " + name);

		// see if we can get around making an array
		// TODO remove this regex completely
		String [] split = name.split("\\.");

		IGScopeItem root = m_root;
		for (int i = 0; i < split.length; i++) {
			//System.out.println("OKEN: " + split[i]);
			root = root.getScopeChild(split[i], false);
			if (root == null) {
				System.out.println("Failed to file sub: " +split[i] + " in " + m_root);
				return null;
			}
		}

		return root;
	}

	public IGScopeItem findScopeItem(String name, int scope_item_type) 
	{
		// check if we're refering to a templated parameter
		
		//if (name.indexOf('.') != -1) {
		///	System.err.println("findScopeItem: " + name);
		//	System.exit(1);
		//}
		//System.out.println("IGResolver.findScopeItem " + name + " " + scope_item_type);
		IGScopeItem found = null;
	
		final int packages_count = m_packages.size();
		for (int i = 0; i < packages_count; i++)
		{
			IGScopeItem s = m_packages.get(i);
			if (scope_item_type != -1 && s.getScopeType() != scope_item_type) {
				continue;
			}
			
			//System.out.println("findScopeItem " + name  + " vs " + s.getScopePath());
			
			if (s.getScopePath().endsWith(name)) 
			{
				if (found != null) {
					m_source.compilationFailed("Ambiguous name exists in many classes: " + name);
				}

				found = s;
			}
			else
			{
				//System.err.println("checking " + name + " vs " + s.getScopePath());
			}
		}
		
		return found;
	}
	
	
	/*
	 * Making a type absolute, binds the m_scope field to actual type.
	 * It has fall throughs for T1 and T2 the template parameters.
	 * If a thing doesn't have an explicit scope, its scope is set to PRIMITIVE.  Essentially a sentinel
	 *
	 * @param type - the type to make absolute
	 * @returns an absolute representation, may or may not be the original type object
	 */
	
	
	public Type makeAbsoluteType(Type type) 
	{
		if (type == null) {
			return null;

		}
		Type type_out = null;

		if (type.isPrimitive() || 
			type.isTemplated()) {
			type_out = type;
		}
		else if (type.m_primary.equals("<wrapper>")) 
		{
			Type left  = makeAbsoluteType(type.m_left);
			Type right = makeAbsoluteType(type.m_right);
			type_out = Type.get("<wrapper>", left, right);
		}
		else if (type.isFunctionOrTuple()) 
		{
			Type right = makeAbsoluteType(type.m_right);
			
			String left_primary = type.m_left.m_primary;
			
			int count = type.m_left.getMinimumParameters();	
			ArrayList<Type> list = Type.allocTempVector();	
			
			Type [] types = type.m_left.m_types;
			for (int i = 0; i < types.length; i++) {
				list.add(makeAbsoluteType(types[i]));
			}
			
			type_out = Type.get(type.m_primary, Type.get(list, count), right);
			Type.freeTempVector(list);
		}
		else
		{
			IGScopeItem si = findScopeItem(type.m_primary);
			if (si == null) {
				throw new IGResolverException("Failed to resolve: " + type.m_primary);
			}

			int scope_type = si.getScopeType();
			if (scope_type == IGScopeItem.SCOPE_ITEM_CLASS ||
				scope_type == IGScopeItem.SCOPE_ITEM_INTERFACE ||
				scope_type == IGScopeItem.SCOPE_ITEM_ENUM)
			{
				IGSource scope_source = (IGSource)si;
				int template_count = scope_source.getTemplatedParameterCount();
				int found_count    = 0;
				
				if (type.m_left != null) 
				{
					//System.out.println("Super messy mess " + (s_bad_count++));
					// handle Object, Map, Array, Vector
					Type left  = makeAbsoluteType(type.m_left);
					Type right = makeAbsoluteType(type.m_right);
				
					type_out = Type.get(scope_source.getInstanceDataType().m_primary, left, right);
					
					found_count += (left  != null) ? 1 : 0;
					found_count += (right != null) ? 1 : 0;
				}
				else
				{
					// fully qualify the path to the type
					type_out = Type.get(scope_source.getInstanceDataType().m_primary);
				}
				
				type_out.setScope(si);
				
				if (template_count != found_count) {
					throw new IGResolverException("Invalid number of templated parameters. Found: " + 
						found_count + " Expected: " + template_count);
				}
			}
			else
			{
				// TODO NOTE THAT THE TYPE ITSELF WASN't 
				throw new IGResolverException(type, "Couldn't resolve type: " + type.toString() + " " + scope_type);
			}
		}
		

		Type replacement = m_replacements.get(type_out);
		if (replacement != null) {
			return replacement;
		}



		return type_out;
	}
	
}