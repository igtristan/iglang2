package pass1;


import java.util.*;

public class IGIROptimizer
{

	static class OptimizerList
	{
		int m_read_head = 0;
		int m_write_head = 0;
		int m_length = 0;

		IGIROp [] m_ops;
		public OptimizerList(List<IGIROp> ops) {
			m_ops = new IGIROp[ops.size()];
			for (int i = m_ops.length - 1; i >= 0; i--) {
				m_ops[i] = ops.get(i);
			}
			m_length = m_ops.length;
		}


		public void skip() {
			write(read());
		}

		public IGIROp read() {
			IGIROp tmp = m_ops[m_read_head];
			m_read_head++;

			//System.out.println("read: " + tmp);
			return tmp;
		}

		public void write(IGIROp op) {
			if (m_write_head == m_read_head) {
				throw new RuntimeException("Buffer Failure");
			}
			m_ops[m_write_head] = op;
			m_write_head++;
		}

		public  int getTag(int pos) {
			pos += m_read_head;
			if (pos >= m_length - 1) {
				return -1;
			}
			IGIROp op= m_ops[pos];
			return  op.op + ((op.label != null) ? HAS_LABEL : 0);
		}

		public  int getCount(int pos) {
			pos += m_read_head;
			if (pos >= m_length - 1) {
				return -1;
			}
			return m_ops[pos].count;
		}

		public  int getIndex(int pos) {
			pos += m_read_head;
			if (pos >= m_length - 1) {
				return -1;
			}
			return m_ops[pos].index;
		}

		public void flip() {
			m_length = m_write_head;
			m_read_head = 0;
			m_write_head = 0;
		}



		public void writeBack(List<IGIROp> dst) {
			dst.clear();
			for (int i = 0; i < m_length; i++) 
			{
				dst.add(m_ops[i]);
			}
		}
	}



	private static final int HAS_LABEL = 0x1000;
	private static final int OP_MASK     = 0xff;


	public static boolean optionalLabel(int t, int type) {
		return (t & OP_MASK) == type;
	}

	public static boolean noLabel(int t, int type) {
		return t == type;
	}

	public static boolean anyNoLabel(int t) {
		return (t & HAS_LABEL) == 0;
	}

	public static boolean hasLabel(int t, int type) {
		return t == (type | HAS_LABEL);
	}

	public static void optimize(List<IGIROp> src) {
		//int i0 = -1;
		//int i1 = -1;
		//int i2 = -1;

		OptimizerList list = new OptimizerList(src);

		for (int j = 0; j < 2; j++)
		{
			//int src_len  = src.size();
			for (int i = 0; ;i++)
			{
				int t0 = list.getTag(0);
				int t1 = list.getTag(1);
				int t2 = list.getTag(2);

				// this should be the very last op
				if (t0 == -1) {
					//list.skip();
					break;
				}

				//NOT
				//J0_I32, target_label: MI47
				else if (optionalLabel(t0, IGIR.NOT)  && noLabel(t1, IGIR.J0_I32)) {
					IGIROp not = list.read();
					IGIROp jmp = list.read();

					jmp = jmp.dup();
					jmp.op = IGIR.JN0_I32;
					jmp.label = not.label;
					list.write(jmp);
				}
				//PUSH_I32, value: 2.0
				//I32_TO_F64
				else if (optionalLabel(t0, IGIR.PUSH_I32)  && noLabel(t1, IGIR.I32_TO_F64)) {
					IGIROp push = list.read();
					IGIROp conv = list.read();
					
					push = push.dup();
					push.op = IGIR.PUSH_F64;
					list.write(push);
				}
				else if (optionalLabel(t0, IGIR.NOP)  && anyNoLabel(t1)) {
					IGIROp pop = list.read();
					IGIROp any = list.read();
					
					any = any.dup();
					any.label = pop.label;
					list.write(any);
				}
				else if (optionalLabel(t0, IGIR.MUL_F64)  && noLabel(t1, IGIR.ADD_F64)) {
					IGIROp pop = list.read();
					IGIROp any = list.read();
					
					any = any.dup();
					any.op = IGIR.MLA_F64;
					any.label = pop.label;
					list.write(any);
				}


				else if (
					(optionalLabel(t0, IGIR.DUP_VEC))  && 
					(
						noLabel(t1, IGIR.LOCAL_STORE_VEC)
					) &&
					noLabel(t2, IGIR.POP) && list.getCount(2) == list.getCount(0))
				{
					IGIROp dup   = list.read();
					IGIROp store = list.read();
					IGIROp pop   = list.read();
					
					store = store.dup();
					store.label = dup.label;
					list.write(store);
				}

				else if (
					(optionalLabel(t0, IGIR.DUP) && list.getCount(0) == 1)  && 
					(
						noLabel(t1, IGIR.LOCAL_STORE)  || 
						noLabel(t1, IGIR.THIS_STORE)   || 
						//noLabel(t1, IGIR.MEMBER_STORE) || 
						noLabel(t1, IGIR.STATIC_STORE)
					) &&
					noLabel(t2, IGIR.POP) && list.getCount(2) == 1)
				{
					IGIROp dup   = list.read();
					IGIROp store = list.read();
					IGIROp pop   = list.read();

					store = store.dup();
					store.label = dup.label;
					list.write(store);
				}
				else if (
					(optionalLabel(t0, IGIR.DUP1X))  && 
					(
						noLabel(t1, IGIR.MEMBER_STORE) 
					) &&
					noLabel(t2, IGIR.POP) && list.getCount(2) == 1)
				{
					IGIROp dup   = list.read();
					IGIROp store = list.read();
					IGIROp pop   = list.read();

					store = store.dup();
					store.label = dup.label;
					list.write(store);
				}
				else if (
					(optionalLabel(t0, IGIR.DUP2X))  && 
					(
						noLabel(t1, IGIR.ARRAY_STORE)
					) &&
					noLabel(t2, IGIR.POP) && list.getCount(2) == 1)
				{
					IGIROp dup   = list.read();
					IGIROp store = list.read();
					IGIROp pop   = list.read();


					store = store.dup();
					store.label = dup.label;
					list.write(store);
				}

				else if (
					optionalLabel(t0, IGIR.PUSH_I32)  && 
					noLabel(t1, IGIR.RET1))
				{
					IGIROp push   = list.read();
					IGIROp ret    = list.read();

					push = push.dup();
					push.type = ret.type;
					push.op = IGIR.RET1_I32;
					list.write(push);
				}
				
				else if (
					optionalLabel(t0, IGIR.LOCAL_LOAD)  && 
					noLabel      (t1, IGIR.STACK_INC_I32) &&
					noLabel      (t2, IGIR.LOCAL_STORE) && 
					list.getIndex(0) == list.getIndex(2))
				{
					IGIROp load   = list.read();
					IGIROp inc    = list.read();
					IGIROp store    = list.read();

					//System.out.println("optimizing: " + load + " " + inc + " " + store);

					load = load.dup();
					load.op = IGIR.LOCAL_INC_I32;
					load.value = inc.value;
					list.write(load);
				}
				
				/*
				LOCAL_LOAD, type: org.iglang.layer0.math.L0Vec3, index: 1
		MEMBER_LOAD, module: org.iglang.layer0.math.L0Vec3, name: x
		*/
			
				else if (
					optionalLabel(t0, IGIR.LOCAL_LOAD)  && 
					noLabel(t1, IGIR.MEMBER_LOAD))
				{
					IGIROp load   = list.read();
					IGIROp member_load    = list.read();

					load = load.dup();
					load.op     = IGIR.LOCAL_MEMBER_LOAD;
					load.module = member_load.module;
					load.name   = member_load.name;
					load.type   = null;	// todo establish this it is  missing
					list.write(load);
				}
				else if (
					optionalLabel(t0, IGIR.PUSH_I32)  && 
					noLabel(t1, IGIR.POW_I32))
				{
					IGIROp push   = list.read();
					IGIROp value    = list.read();

					push = push.dup();
					push.op     = IGIR.POW_I32_CONST;
					list.write(push);
				}
				else if (
					optionalLabel(t0, IGIR.PUSH_F64)  && 
					noLabel(t1, IGIR.POW_F64))
				{
					IGIROp push   = list.read();
					IGIROp value    = list.read();

					push = push.dup();
					push.op     = IGIR.POW_F64_CONST;
					list.write(push);
				}
				else 
				{
					list.skip();
				}
			}

			list.skip();
			list.flip();
		}
		list.writeBack(src);
	}
}

/*




*/