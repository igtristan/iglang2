package pass1;

import java.util.*;

public class IGQualifier
{
	public static void run(IGPackage root, Vector<IGSource> sources)
	{
		///////////////////////////////////////////////
		// Map the scopes of primitives to a concrete enum
		// this is to facilitate the generation of generics
		// as it gives us a way to know what "functions" can be
		// called on a primitive value
		////////////////////////////////////////////////////
		
		for (IGSource s : sources)
		{	
			try
			{
				String instance_data_type = s.getInstanceDataType().toString();
				
				if (instance_data_type.equals("iglang.util.igShort")) {
					Type.SHORT.setScope(s);
				}
				if (instance_data_type.equals("iglang.util.igByte")) {
					Type.BYTE.setScope(s);
				}
				if (instance_data_type.equals("iglang.util.igInteger")) {
					Type.INT.setScope(s);
				}
				else if (instance_data_type.equals("iglang.util.igDouble")) {
					Type.DOUBLE.setScope(s);
				}
				else if (instance_data_type.equals("iglang.util.igBoolean")) {
					Type.BOOL.setScope(s);
				}
				else if (instance_data_type.equals("iglang.util.igFloat")) {
					Type.FLOAT.setScope(s);
				}
			}
			catch (RuntimeException ex) {
				util.Log.logError(s.getName(), "", 0, 0);
				throw ex;
			}
		}	


		/////////////////////////////////////////////////
		// Validate 
		// - extends    (does the class being extended exists)
		// - interfaces (does the class honor the interfaces mentioned)
		// - function types (do the types of each of the functions match something that exists)
		//
		// THESE all produce soft errors 		
		
		for (IGSource s : sources)
		{
			IGResolver r = new IGResolver();
			
			// failed to resolve the packages included by this source file
			if (!r.init(root, s)) {
				continue;
			}
		
			if (s.isClass() || s.isEnum() || s.isInterface()) 
			{
			
				//////////////////////////////////////////////////////
				// Fully qualify all of the types used... yes
				// and tag the appropriate scope items for each of them
				{	
					if (s.isClass() || s.isInterface()) 
					{
						s.m_class_extends = fullyQualify(r, s.m_class_extends, s.m_name, "Failed to find class extended: ");
					}
					
					if (s.isEnum()) 
					{
						if (s.m_enum_extends == null) {
							s.m_enum_extends = Type.INT;
						}
						s.m_enum_extends = fullyQualify(r, s.m_enum_extends, s.m_name, "Failed to find primitive to extend: ");
					}
					
					if (s.isClass()) 
					{
						List<Type> implements_list = s.m_class_implements;
						for (int i = 0; i < implements_list.size(); i++) {
							implements_list.set(i, 
								fullyQualify(r, implements_list.get(i), s.m_name,
								"Failed to find interface implemented: "));
						}
						

						////////////////////////////
						// Don't think we actually have to do this
						// but it may become important down the road

						// boil down the template parameters to concrete exensions
						ArrayList<IGTemplateParameter> tpls = s.m_template_parameters;
						for (int i = 0; i < tpls.size() && i < 2; i++) {
							IGTemplateParameter tpl = tpls.get(i);
							tpl.extends_type = fullyQualify(r, tpl.extends_type, tpl.token);
							//if (tpl.extends_type.m_scope == null) {
							//	softError(tpl.token, "Category not found: " + tpl.extends_type);
							//}
						}
					}
				
					
				}
			}
		}

		// flush out the stubs for the templated classes
		for (IGSource s : sources)
		{
			
			ArrayList<IGTemplateParameter> tpls = s.m_template_parameters;
			for (int i = 0; i < tpls.size() && i < 2; i++) 
			{
				IGTemplateParameter tpl = tpls.get(i);
		
				IGSource src = (IGSource)tpl.extends_type.m_scope;
				IGSource dst = (IGSource)tpl.type.m_scope;

				if (tpl.template_action == 0 && src.isEnum()) {
					dst.stubExtendEnum(root, src);
				}
				else if (tpl.template_action == 1 && src.isClass()) {
					dst.stubExtendClass(src);
				}
				else if (tpl.template_action == 2 && src.isInterface()) {
					dst.stubImplementsClass(root, src);
				}
				
				else {
					error(tpl.token, "Invalid template setup");
				}
			}
		}

		for (IGSource s : sources)
		{
			IGResolver r = new IGResolver();
			// failed to resolve the packages included by this source file
			if (!r.init(root, s)) {
				continue;
			}
			//////////////
			// Resolve the types of all variables/functions
			for (IGMember m : s.m_members) 
			{
				if (m.m_type == IGMember.TYPE_FUNCTION) 
				{
					// What exactly does this do?
					m.m_return_type   = fullyQualify(r, m.m_return_type, m.m_name, "Failed to resolve return type: ");		
					//System.out.println("" + m.m_return_type);
					m.m_function_type = fullyQualifyFunctionSignature(r,  m);	
				}
				else
				{
					m.m_return_type   = fullyQualify(r, m.m_return_type, m.m_name, "Failed to resolve variable type: ");
				}
				
				if (m.m_type == IGMember.TYPE_FUNCTION) 
				{
					//System.err.println("Function: " + m.m_name.m_value);

					List<IGVariable> vlist = m.getVariables();
					// this check is most likely redundant as we recheck all types as we go
					for (int i = 0; i < vlist.size(); i++) 
					{
						IGVariable v = vlist.get(i);
						Token blame = (v.m_type_token != null) ? v.m_type_token : v.m_name;
					
						//System.err.println("IGVariable : " + i );
						//System.err.println("" + v.m_name);
						//System.err.println(" " + v.m_type);
						vlist.get(i).m_type = fullyQualify(r, v.m_type, blame, "Failed to resolve variable type: " + v.m_type);
					}
				}	
			}
		}
	}

	public static Type fullyQualify(IGResolver r, Type type, Token viscinity) {
		return fullyQualify(r, type, viscinity, "Couldn't qualify type: " );
	}

	public static Type fullyQualify(IGResolver r, Type type, Token viscinity, String error_message)
	{
		if (null == type) {
			return null;
		}

		Type result  = null;
		try
		{
			 result = r.makeAbsoluteType(type);
		}
		catch (IGResolverException ex) 
		{
			ex.printStackTrace();
			
			if (ex.m_type != null) {
				error(viscinity, "Couldn't resolve type: " + ex.m_type);
			}
			else {
				error(viscinity, error_message + type);
			}
		}

		return result;
	}


	public static Type fullyQualifyFunctionSignature(IGResolver r, IGMember m)
	{
		// validate each token separately
		// then move on to the whole sig
		for (IGVariable v : m.m_parameters) 
		{
			try
			{
				r.makeAbsoluteType(v.m_type);
			}
			catch (IGResolverException ex) 
			{
			//	ex.printStackTrace();
				error(v.m_type_token, "Couldn't resolve parameter type: " + v.m_type);
			}
		}
		
		// validate the return type
		try
		{
			r.makeAbsoluteType(m.m_return_type);
		}
		catch (IGResolverException ex) 
		{
			ex.printStackTrace();
			error(m.m_return_type_token, "Couldn't resolve return type: " + m.m_return_type);
		}
	
		// validate the whole shebang (also includes minimum parameters)
		Type type = m.getDataType(null);
		
		if (null == type) return null;
		
		Type result  = null;
		try
		{
			 result = r.makeAbsoluteType(type);
		}
		catch (IGResolverException ex) 
		{
			ex.printStackTrace();
			
			if (ex.m_type != null) {
				error(m.m_name, "Couldn't resolve type: " + ex.m_type);
			}
			else {
				error(m.m_name, "Failed to resolve function signature 2: " + type);
			}
		}

		return result;
	}

	public static final void error(Token t, String text)
	{
		if (t == null) {
			System.err.println("No token error: " + text);
		}
		else {
			t.printError(text);
		}
		throw new RuntimeException("Failure: " + text);
	}
	

}