#!/bin/sh

compile_jar()
{
	javac  -source 1.8 -target 1.8 -g -classpath ./src -d ./bin ./src/$1.java || exit 1
	rsync -av --exclude=".*" data/ bin/data

	cd bin
	jar cvfe ../jar/$1.jar $1 .
	#cd ..
	    #cp ./jar/$1.jar ../$1.jar
}


main()
{
	[ -d bin ] || mkdir bin
	[ -d jar ] || mkdir jar
	echo "Generating Data Driven Code"
	php build_datadriven.php
	echo "Compiling JAR"
	compile_jar 		igc2 || exit 1
}


main;
