"use strict";




var igvm = require("./dst_js/index.js");
igvm.init();

function processArguments(method, fn, start_index, args_in) {
	let args = [method, fn];
	for (let i = start_index; i < args_in.length; i++) {
		const a = args_in[i];
		if (typeof a === 'string') {
			args.push(igvm.nativeToString(a));
		}
		else {
			args.push(a);
		}
	}

	return args;
}

function runExactTest(message, expected_result, method, fn)
{
	let args = processArguments(method, fn, 4, arguments);
	let result = igvm.call.apply(igvm, args);

	if (result === expected_result) {
		console.log("SUCCESS " + message);
	}
	else {
		console.log("ERROR " + message + ". expected: " + expected_result + " found: " + result);
	}
}

function runStringTest(message, expected_result, method, fn) 
{
	let args = processArguments(method, fn, 4, arguments);
	

	let result = igvm.call.apply(igvm, args);
	if (igvm.isNull(result)) {
		result = null;
	}
	else {
		result = igvm.stringToNative(result);
	}

	if (result == expected_result) {
		console.log("SUCCESS " + message);
	}
	else {
		console.log("ERROR " + message + ". expected: " + expected_result + " found: " + result);
	}
}


console.log('############################################');
console.log('NodeJS Test Suite for iglang JS target');
console.log('############################################');

/*
	name
	expected result

	module code exist in
	function to call
	param0 .. paramN
*/

const TRUE  = 1;
const FALSE = 0;


runExactTest("PrimitiveCasting bool(1)",  TRUE, "PrimitiveCasting", "intToBool", 1);
runExactTest("PrimitiveCasting bool(110)",  TRUE, "PrimitiveCasting", "intToBool", 110);
runExactTest("PrimitiveCasting bool(0)",  FALSE, "PrimitiveCasting", "intToBool", 0);

runExactTest("Math imla",         7,   "MathTest", "imla", 1, 2, 3);
runExactTest("Math doubleToInt",  3,   "MathTest", "doubleToInt", 3.75);
runExactTest("Math dot2",         19,  "MathTest", "dot2", 1,3,4,5);

runExactTest("Math pow",  (3*3*3*3) * (3*3*3*3),   "MathTest", "pow",  3.0, 4.0);

runExactTest("Math sin(0)",  0.0,   "iglang.Math", "sin",  0.0);
runExactTest("Math cos(0)",  1.0,   "iglang.Math", "cos",  0.0);
runStringTest("String concat null", "nullnull", "StringTest", "concat", null, null);
runStringTest("String concat",      "abc123", "StringTest", "concat", "abc", "123");
runStringTest("String charAt", "d", "iglang.String",   "charAt", "abcdef", 3);
runExactTest("Vector idx",       4, "VectorTest", "getIndex", 2,   0,2,4,8);
runExactTest("Switch pow of 3", 27, "SwitchTest", "powerOf3", 3);
runExactTest("Array Copy", 1+2+3+4+5+6+7+8, "ArrayTest", "copy");


runExactTest("TupleTest vec3 storage", 1, "TupleTest", "storage", 5, -1, 22);
runExactTest("crossDot",              -3, "TupleTest","crossDot",  0,0,1,  0,1,0,  3,4,5);

// degenerate points cases (2 poinsts, a point, b point)
runExactTest("Physics ssd", 5.0*5.0, "PhysicsTest", "test_segmentSegmentSquaredDistance",
		0,0,0,  0,0,0,  
		5,0,0,  5,0,0);

runExactTest("Physics ssd", 1, "PhysicsTest", "test_segmentSegmentSquaredDistance",
		0,0,0,  0,0,0,  
		1,0,0,  1,0,1);

runExactTest("Physics ssd", 1, "PhysicsTest", "test_segmentSegmentSquaredDistance",
		0,0,0,  0,0,1,  
		1,0,0,  1,0,0);

// simple case
runExactTest("Physics ssd", 1, "PhysicsTest", "test_segmentSegmentSquaredDistance",
		0,0,0,  0,0,1,  
		1,0,0,  1,0,1);


// simple case
runExactTest("Physics ray aabb", 1, "PhysicsTest", "test_rayAABBIntersection",
		0,0,-10,   0,0,1,  
		-2.-2,-2,  2,2,2);


///////////////////////////////////////////////
// Testing grazing the side of the box

console.log();
console.log("Swept Sphere AABB");

// simple case
runExactTest("Sweep sphere", 0, "PhysicsTest", "test_intersectMovingSphereAABB",
		2,0,0,  0.5,  4,0,0,  
		-1.-1,-1,  1,1,1);

// simple case
runExactTest("Sweep sphere", 1, "PhysicsTest", "test_intersectMovingSphereAABB",
		2,0,0,  1,  4,0,0,  
		-1.-1,-1,  1,1,1);

// simple case
runExactTest("Sweep sphere", 1, "PhysicsTest", "test_intersectMovingSphereAABB",
		2,0,0,  1.5,  4,0,0,  
		-1.-1,-1,  1,1,1);

runExactTest("Templates 2.0", 55, "templates.AllocatorTest", "test");

runExactTest("Templates - Interfaace", 1, "templates.InterfaceTest", "test");

runExactTest("Inheritane Variables", 1, "inheritance.variables.Child", "test");

runExactTest("OperatorsLength", 1, "math.OperatorsLength", "test");

runExactTest("TupleDouble", 1, "math.TupleDouble", "test");

runExactTest("Nil", 1, "types.Nil", "test");

// Test whether the vtable is found
(function(){
	var file = igvm.newObject('iglang.io.File');
	//console.log(file);

	if (file === null || typeof file.$vtable === 'undefined') {
		console.log("ERROR Corrupt $vtable on newObject creation");
		
	}
	else {
		console.log("SUCCESS vtable found"); 
	}
})();