package math;

public class OperatorsLength
{
	public static function test(): bool {

		var a :double = 1;
		var a2 : 2 double = (4.0,  0.0);
		var a3 : 2 double = (0.0, -4.0);


		return (#length a) == 1 &&
			   (#length a2) == 4 &&
			   (#length a3) == 4;
	}
}