package math;

public class TupleDouble
{
	public static function testDiv() :bool {
		var a : 3 double = (2.0, 4.0, 51.0);
		var b : 3 double = a / a;

		return b_0 == 1 && b_1 == 1 && b_2 == 1;
	}

	public static function test() :bool {
		return testDiv();
	}
}