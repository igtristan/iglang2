package;

public class ArrayTest
{
	public static function copy() :int {
		var a0 :Array<int> = new Array<int>(8) [
			1,2,3,4,5,6,7,8
		];

		var a1 :Array<int> = new Array<int>(8);
		a1.copy(a0);

		var sum :int = 0;
		for (var i :int in 0 => a1.length) {
			sum += a1[i];
		}

		return sum;
	}
}