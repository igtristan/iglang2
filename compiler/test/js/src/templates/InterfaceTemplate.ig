package templates;

public class InterfaceTemplate<X interface Interface>
{
	public function run(obj : X) :void {
		obj.doSomething();
		X.doSomethingStatic();
	}
}