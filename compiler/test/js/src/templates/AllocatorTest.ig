package templates;

public class AllocatorTest
{
	public function test() : int {
		var a : Allocator<AllocatorObj> = new Allocator<AllocatorObj>();

		// yeah this generates weird code
		// technically we should't be creating generic versions if there is a new involved in the code period
		return a.alloc().query();
	}
}