package templates;

public class Allocator<T1 extends Object>
{
	public function alloc() :T1 {
		return new T1();
	}

	public function dealloc(o : T1) :void {

	}
}