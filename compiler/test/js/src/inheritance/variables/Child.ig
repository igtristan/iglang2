package inheritance.variables;

public class Child extends Parent
{
	public function test() :bool {
		var c :Child  = new Child();
		return c.i32 == 32 && c.f64 == 64.0 && c.b == true &&
			   c.i32_undefined == 0 && c.f64_undefined == 0.0 && c.b_undefined == false && c.obj_undefined == null;
	}
}