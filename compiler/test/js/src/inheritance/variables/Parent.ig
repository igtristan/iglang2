package inheritance.variables;

public class Parent
{
	public var i32 :int = 32;
	public var f64: double = 64.0;
	public var  b :bool = true;

	public var i32_undefined: int;
	public var f64_undefined: double;
	public var b_undefined:   bool;
	public var obj_undefined :Object;
}