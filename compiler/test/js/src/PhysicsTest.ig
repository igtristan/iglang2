package;

/**
 * The following is a modified excerpt from Real Time Collision Detection
TODO:
	 FORGETTING TO CLOSEE A COMMENT CAN FREAK OUT THE COMPILER
 	ADD IN SUPPORT for 1.0f
 	ALSO looks liek you can straight up set tupple variables without any punishment
 */

public class PhysicsTest
{
	
	public static function test_segmentSegmentSquaredDistance(
		a1 : 3 double, a2 : 3 double,
						 b1 : 3 double, b2 : 3 double) : double {

		var times : Pair<double,double> = new Pair<double,double>(0,0);
		return CollisionDetection.segmentSquaredDistance(a1,a2,b1,b2,times);
	}

	public static function test_rayAABBIntersection(
		r0 : 3 double, rd : 3 double,
		mins : 3 double, maxs : 3 double) :bool {


		var times : Pair<double,double> = new Pair<double,double>(0,0);
		return CollisionDetection.rayAABBIntersection(r0,rd,mins,maxs,times);
	}

	public static function test_intersectMovingSphereAABB(
		r0 : 3 double, r :double, rd : 3 double,
		min : 3 double, max : 3 double):bool {

		var times : Pair<double,double> = new Pair<double,double>(0,0);
		return CollisionDetection.intersectMovingSphereAABB(r0, r, rd, min, max,times);
	}

/*
Point Corner(AABB b, int n)
{
Point p;
p.x = (n & 1) ? b.max.x : b.min.x;
p.y = (n & 2) ? b.max.y : b.min.y;
p.z = (n & 4) ? b.max.z : b.min.z;
return p;
}
*/
}