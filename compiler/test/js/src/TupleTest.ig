package;

public class TupleTest
{
	var a : double;
	var b : double;
	var c : double;

	static var x : double;
	static var y : double;
	static var z : double;


	public static function crossDot(a : 3 double, b : 3 double, c : 3 double) :double {
		return (a #cross b) #dot c;
	}

	public static function abs(a : 3 double) : double {
		var b : 3 double = #abs a;
		return b_0 + b_1 + b_2;
	}
	public static function storage(v : 3 double) : bool {
		return (new TupleTest())._storage(v);
	}
	public  function _storage(v : 3 double) : bool {
		(a,b,c) = v;
		(x,y,z) = v;

		if (a != v_0) { return false; }
		if (b != v_1) { return false; }
		if (c != v_2) { return false; }
	

		if (x != v_0) { return false; }
		if (y != v_1) { return false; }
		if (z != v_2) { return false; }

		return true;
	}
}