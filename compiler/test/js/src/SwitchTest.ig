package;

public class SwitchTest
{
	
	public static function powerOf3(idx :int) : int
	{
		switch (idx) {
			case (0){ return 1; }
			case (1){ return 3; }
			case (2){ return 9; }
			case (3){ return 27; }
			case (4){ return 81; }
		}
		return -1;

	}
}