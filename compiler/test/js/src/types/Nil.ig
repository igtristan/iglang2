package types;

public class Nil
{
	public function test():bool {
		var a :int = nil;
		var b :double = nil;
		var c :bool = nil;
		var d :Object = nil;
		var e :Function<(int):void> = nil;

		// TODO test enums and anything I didn't miss

		return a == 0 && b == 0.0 && c == false && d == null && e == null;
	}
}