package;

public class JumpTargetFailure
{
	private var m_board_width :int = 16;
	private var m_board_height :int = 16;
	private var m_cell_width :int = 4;
	private var m_cell_height :int = 4;

	//The following can cause an undeclared jump target
	public function screenPositionToCellId(x :double, y :double) :int 
	{
		//x -= m_board_x;
		//y -= m_board_y;

		var cx :int = Math.floor(x / m_cell_width);
		var cy :int = Math.floor(y / m_cell_height);
		if (cx < 0 || cx >= m_board_width ||
			cy < 0 || cy >= m_board_height) {
			return 0; // CellId.UNDEFINED;
		}


		return 1;

		//var cell_id : CellId =  CellId.create(cx,cy);

		//System.log("End");
		//return cell_id;
	}
}