package;

public class CollisionDetection
{
	public static const EPSILON :double = 0.0000001;

//	public var c1 : 3 double;
	



	// Clamp n to lie within the range [min, max]
	public static function clamp(n :double, min :double, max :double):double {
   	 	if (n < min) { return min; }
    	if (n > max) { return max; }
    	return n;
	}


	public static  function dot(left : 3 double, right : 3 double) : double {
		return  left_0 * right_0 + 
				left_1 * right_1 + 
				left_2 * right_2;
	}	

	public static function vtos(v : 3 double) :String {
		return "vec3[" + v_0 + "," + v_1 + "," + v_2 + "]";
	}


	public static function intersectMovingSphereAABB(
		p : 3 double, sr: double, 
		d : 3 double, 
		min : 3 double,
		max : 3 double,
		times : Pair<double,double>) :bool
	{


		// Coarse test
		{
		    // Compute the AABB resulting from expanding b by sphere radius r
		    var emins : 3 double = min - (sr,sr,sr);
		    var emaxs : 3 double = max + (sr,sr,sr);

		    // Intersect ray against expanded AABB e. Exit with no intersection if ray
		    // misses e, else get intersection point p and time t as result
		    if (!rayAABBIntersection(p, d, emins, emaxs, times) || times.left > 1.0)
		    {
		        return false;
		    }
		}

		var scoarse : double = times.left;

	    // Compute which min and max faces of b the intersection point p lies
	    // outside of. Note, u and v cannot have the same bits set and
	    // they must have at least one bit set amongst them
	    var u :int = 0;
	    var v :int = 0;
	    if (p_0 < min_0) {
	        u |= 1;
	    }
	    if (p_0 > max_0) {
	        v |= 1;
	    }
	    if (p_1 < min_1) {
	        u |= 2;
	    }
	    if (p_1 > max_1) {
	        v |= 2;
	    }
	    if (p_2 < min_2) {
	        u |= 4;
	    }
	    if (p_2 > max_2) {
	        v |= 4;
	    }

	    // ‘Or’ all set bits together into a bit mask (note: here u + v == u | v)
	    var  m :int = u | v;

	    // Define line segment [c, c+d] specified by the sphere movement
	   
	    var r2 :double = sr*sr;

	    var vx :bool = 0 != (v&1);
	    var vy :bool = 0 != (v&2);
	    var vz :bool = 0 != (v&4);

	    var corner0 : 3 double = iif( ( vx, vy, vz), min, max);
		var ca      : 3 double = iif( (!vx, vy, vz), min, max);
	    var cb      : 3 double = iif( ( vx,!vy, vz), min, max);
	    var cc      : 3 double = iif( ( vx, vy,!vz), min, max);
		var cd      : 3 double = iif( (0 == (u&1), 0 == (u&2), 0 == (u&4)), min, max);



	    // If all 3 bits set (m == 7) then p is in a vertex region
	    if (m == 7) 
	    {
	        // Must now intersect segment [c, c+d] against the capsules of the three
	        // edges meeting at the vertex and return the best time, if one or more hit
	        var smin :double = double.MAX_VALUE;
	        if (r2 >= segmentSquaredDistance(p, p + d, corner0, ca, times))
	        {
	            smin = double.min(times.left, smin);
	        }

	        if (r2 >= segmentSquaredDistance(p, p + d, corner0, cb, times))
	        {
	            smin = double.min(times.left, smin);
	        }
	        
	        if (r2 >= segmentSquaredDistance(p, p + d, corner0, cc, times))
	        {
	            smin = double.min(times.left, smin);
	        }

	        if (smin == double.MAX_VALUE) {
	            return false; // No intersection
	        }

	        scoarse = smin;
	        times.left = scoarse;
	        return true; // Intersection at time t == tmin
	    }
	    // If only one bit set in m, then p is in a face region
	    if ((m & (m - 1)) == 0) {
	        // Do nothing. Time t from intersection with
	        // expanded box is correct intersection time
	        times.left = scoarse;
	        return true;
	    }
	    // p is in an edge region. Intersect against the capsule at the edge
	    if (r2 >= segmentSquaredDistance(p, p + d, cd, corner0, times)) {
	    	return true;
	    }
	    else {
	    	return false;
	    }
	}


	// Intersect ray R(t) = p + t*d against AABB a. When intersecting,
	// return intersection distance tmin and point q of intersection
	public static function rayAABBIntersection( 
		p     :3 double, d    : 3 double, 
		amin  :3 double, amax : 3 double, 
		times :Pair<double,double>) : bool
	{
	    var tmin :double = 0.0;          // set to -FLT_MAX to get first hit on line
	    var tmax :double = double.MAX_VALUE; // set to max distance ray can travel (for segment)

	    // X Slab
	    //////////////////////////////////
        if (Math.abs(d_0) < EPSILON) {
            // Ray is parallel to slab. No hit if origin not within slab
            if (p_0 < amin_0 || p_0 > amax_0) {
            	return false;
            }
        } 
        else 
        {
            // Compute intersection t value of ray with near and far plane of slab
            var ood :double = 1.0 / d_0;
            var t1  :double = (amin_0 - p_0) * ood;
            var t2  :double = (amax_0 - p_0) * ood;
            // Make t1 be intersection with near plane, t2 with far plane
            if (t1 > t2) //Swap(t1, t2);
            {
            	// Compute the intersection of slab intersections intervals
            	tmin = Math.max(tmin, t2);
            	tmax = Math.min(tmax, t1);
            }
            else 
            {
            	// Compute the intersection of slab intersections intervals
            	tmin = Math.max(tmin, t1);
            	tmax = Math.min(tmax, t2);
            }
            // Exit with no collision as soon as slab intersection becomes empty
            if (tmin > tmax) {
            	return false;
        	}
        }

        // Y Slab
	    //////////////////////////////////
        if (Math.abs(d_1) < EPSILON) {
            // Ray is parallel to slab. No hit if origin not within slab
            if (p_1 < amin_1 || p_1 > amax_1) {
            	return false;
            }
        } 
        else 
        {
            // Compute intersection t value of ray with near and far plane of slab
            var ood :double = 1.0 / d_1;
            var t1  :double = (amin_1 - p_1) * ood;
            var t2  :double = (amax_1 - p_1) * ood;
            // Make t1 be intersection with near plane, t2 with far plane
            if (t1 > t2) //Swap(t1, t2);
            {
            	// Compute the intersection of slab intersections intervals
            	tmin = Math.max(tmin, t2);
            	tmax = Math.min(tmax, t1);
            }
            else 
            {
            	// Compute the intersection of slab intersections intervals
            	tmin = Math.max(tmin, t1);
            	tmax = Math.min(tmax, t2);
            }
            // Exit with no collision as soon as slab intersection becomes empty
            if (tmin > tmax) {
            	return false;
        	}
        }

        // Z Slab
	    //////////////////////////////////
        if (Math.abs(d_1) < EPSILON) {
            // Ray is parallel to slab. No hit if origin not within slab
            if (p_1 < amin_1 || p_1 > amax_1) {
            	return false;
            }
        } 
        else 
        {
            // Compute intersection t value of ray with near and far plane of slab
            var ood :double = 1.0 / d_1;
            var t1  :double = (amin_1 - p_1) * ood;
            var t2  :double = (amax_1 - p_1) * ood;
            // Make t1 be intersection with near plane, t2 with far plane
            if (t1 > t2) //Swap(t1, t2);
            {
            	// Compute the intersection of slab intersections intervals
            	tmin = Math.max(tmin, t2);
            	tmax = Math.min(tmax, t1);
            }
            else 
            {
            	// Compute the intersection of slab intersections intervals
            	tmin = Math.max(tmin, t1);
            	tmax = Math.min(tmax, t2);
            }
            // Exit with no collision as soon as slab intersection becomes empty
            if (tmin > tmax) {
            	return false;
        	}
        }
	    
	    // Ray intersects all 3 slabs. Return point (q) and intersection t value (tmin) 
	    //setC1(p + d * tmin);
	    times.left  =  tmin;
	    times.right = tmax;
	    return true;
	}



	// Computes closest points C1 and C2 of S1(s)=P1+s*(Q1-P1) and
	// S2(t)=P2+t*(Q2-P2), returning s and t. Function result is squared
	// distance between between S1(s) and S2(t)
	public static function segmentSquaredDistance(   
			p1 : 3 double,  q1 : 3 double, 
	        p2: 3 double,   q2 : 3 double,
	        times  :Pair<double,double>) :double
	{
	    var d1 :3 double = q1 - p1; // Direction vector of segment S1
	    var d2 :3 double = q2 - p2; // Direction vector of segment S2
	    var r  :3 double = p1 - p2;
	    var a : double = dot(d1, d1); // Squared length of segment S1, always nonnegative
	    var e : double = dot(d2, d2); // Squared length of segment S2, always nonnegative
	    var f : double = dot(d2, r);
	    var t :double = 0;
	    var s:double = 0;

	   // System.log("Running test " + vtos(p1) + " " + vtos(q1) + " " + vtos(p2) + " " + vtos(q2));
	    // this is okay
	   // (s) = (1.0);

	    // this should be caught by the validator but it is not
	   // var tmp : 2 double = (1.0,2.0);
	    //tmp += tmp;

	    // Check if either or both segments degenerate into points
	    if (a <= EPSILON && e <= EPSILON) {
	        // Both segments degenerate into points
	        
	        times.left = 0;
	        times.right = 0;

	        var c1 : 3 double = p1;
	        var c2 : 3 double = p2;
	       // setC1(c1); 
	   	 	//setC2(c2);

	   	// 	System.log("Early out");
	        return dot(c1 - c2, c1 - c2);
	    }
	    if (a <= EPSILON) {
	        // First segment degenerates into a point
	        s = 0.0;
	        t = f / e; // s = 0 => t = (b*s + f) / e = f / e
	        t = clamp(t, 0.0, 1);
	    } 
	    else 
	    {
	        var c : double = dot(d1, r);
	        if (e <= EPSILON) {
	            // Second segment degenerates into a point
	            t = 0.0;
	            s = clamp(-c / a, 0, 1); // t = 0 => s = (b*t - c) / a = -c / a
	        } 
	        else 
	        {
	            // The general nondegenerate case starts here
	            var b : double = dot(d1, d2);
	            var denom : double = a*e-b*b; // Always nonnegative

	            // If segments not parallel, compute closest point on L1 to L2, and
	            // clamp to segment S1. Else pick arbitrary s (here 0)
	            if (denom != 0.0) {
	                s = clamp((b*f - c*e) / denom, 0, 1);
	            } 
	            else {
	            	s = 0.0;
	            }
	            // Compute point on L2 closest to S1(s) using
	            // t = dot((P1+D1*s)-P2,D2) / dot(D2,D2) = (b*s + f) / e
	            t = (b*s + f) / e;

	            // If t in [0,1] done. Else clamp t, recompute s for the new value
	            // of t using s = dot((P2+D2*t)-P1,D1) / dot(D1,D1)= (t*b - c) / a
	            // and clamp s to [0, 1]
	            if (t < 0.0) {
	                t = 0.0;
	                s = clamp(-c / a, 0.0, 1.0);
	            } 
	            else if (t > 1.0) {
	                t = 1.0;
	                s = clamp((b - c) / a, 0.0, 1.0);
	            }
	        }
	    }

	    var c1 : 3 double = p1 + d1 * s;
	    var c2 : 3 double = p2 + d2 * t;
	
		times.left  = s;
		times.right = t;
	    
	    return dot(c1 - c2, c1 - c2);
	}

}