package iglang.util;

public enum igByte extends int
{
	;
	public  function toString(): String
	{
		return String(int(this));
	}
	
	public  function hashCode(): int
	{
		return this;
	}
	
	public  function equals(other : igByte): bool
	{
		return other == this;
	}
	
}