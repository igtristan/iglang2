package iglang.util;

public class ArrayIterator<T1>
{
	private var m_index : int = 0;
	private var m_array : Array<T1>;
	
	public function constructor(a : Array<T1>) {
		m_array = a;
		has_next = a.length > 0;
		m_index = 0;
	}
	
	public var has_next : bool = false;
	
	public final function next(): T1
	{
		var idx : int = m_index + 1;
		has_next = (idx < m_array.length);
		m_index = idx;
		return m_array[idx - 1];
	}

}