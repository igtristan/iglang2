package iglang;

public class Box<T1>
{
	public var value :T1;

	public function constructor(v :T1) {
		value = v;
	}

	public override function hashCode() :int {
		return value.hashCode();
	}

	public override function toString() :String {
		return value.toString();
	}

	// this signature is weird
	public override function equals(other :Object) : bool {
		//
		//return false;		// fix this
		throw new Exception("Fix this");
		/*
		if (other instanceof Box) {
			var other_box : Box<T1> = Box<T1>(other);
			return value.equals(other_box.value);
		}
		else {
			return false;
		}
		*/
	}


}