package iglang;

import iglang.util.Extern;

public class System
{
	public static function log(msg :String) :void {
		Extern.log(msg);
	}
}