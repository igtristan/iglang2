package iglang;

public class Pair<T1, T2>
{
	public var left : T1;
	public var right :T2;

	public function constructor(l :T1, r :T2) {
		left  = l;
		right = r;
	}
}