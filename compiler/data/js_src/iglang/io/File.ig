package iglang.io;

import iglang.util.Extern;

public class File
{
	public static var SEPARATOR      :String = "/";
	private static var FILE_BIT      :int = 1;
	private static var DIRECTORY_BIT :int = 2;


	private var m_path :String;
	private var m_name :String;
	private var m_size :int;
	private var m_time_modified: double;

	// Should we disallow relative paths???

	public static function createFromString(path :String) :File {
		var file :File = new File();
		file.m_path = path;
		file.extractNameFromPath();
		return file;
	}

	private function extractNameFromPath() :void 
	{
		var working_path :String = m_path;
		var name_start :int = working_path.lastIndexOf(SEPARATOR);
		if (name_start == -1) {
			name_start = 0;
		}
		this.m_name = working_path.substring(name_start);
	}



	public function get parent() :File 
	{
		var term :int = m_path.lastIndexOf(SEPARATOR);
		var parent_file :File = new File();

		var parent_path :String = m_path.substring(0, term);

		parent_file.m_path = parent_path;
		parent_file.extractNameFromPath();

		return parent_file;
	}

	

	public function getChild(name :String) : File {
		var child :File  = new File();
		child.m_path = m_path + SEPARATOR + name;
		child.m_name = name;
		return child;
	}


	public function listChildren() :Array<File> {
		var children :Array<String> = Extern.fileImplList(m_path);
		var children_out : Array<File> = new Array<File>(children.length);
		for (var i :int in 0 => children.length) {
			var child :File = new File();
			child.m_name = children[i];
			child.m_path = m_path + SEPARATOR + children[i];
			children_out[i] = child;
		}
		return children_out;
	}

	public function readByteArray(ba :ByteArray, offset :int = 0, max_length :int = -1) : int {
	
		var count :int = Extern.fileImplReadByteArray(m_path, ba, offset, max_length);
		ba.length = Math.max(ba.length, offset + count);
		return count;
	}

	public function writeByteArray(ba :ByteArray, offset :int = 0, length :int = -1) : bool {
		return Extern.fileImplWriteByteArray(m_path, ba.backingArray, offset, length);
	}

	public function get isFile() :bool {
		return bool(Extern.fileImplStat(m_path, this) & FILE_BIT); 
	}

	public function get isDirectory() :bool {
		return bool(Extern.fileImplStat(m_path, this) & DIRECTORY_BIT); 
	}

	public function get exists() : bool {		
		return bool(Extern.fileImplStat(m_path, this) & (FILE_BIT | DIRECTORY_BIT)); 
	}

	public function get size() : int {		
		Extern.fileImplStat(m_path, this);
		return m_size;
	}


	public function mkdir() : bool {
		return Extern.fileImplMkDir(m_path);
	}

	public function get name() :String {
		return m_name;
	}

	public function delete() : bool {
		return Extern.fileImplDelete(m_path);
	}

	public static function copy(src :File, dst :File) :void {
		Extern.fileImplCopy(src.m_path, dst.m_path);
	}

}