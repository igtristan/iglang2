package iglang.util;


enum igInteger extends int
{
	;


	public static const MAX_VALUE : int =  2147483647;
	public static const MIN_VALUE : int = -2147483648;		// TODO verify that this value gets set correctly

	public  function toString(): String
	{
		return String(int(this));
	}
	
	public  function hashCode(): int
	{
		return this;
	}
	
	public  function equals(other : igInteger): bool
	{
		return other == this;
	}
	
	

	public function inRange0(range_end :int) : bool {
		return 0 <= int(this) && int(this) < range_end;
	}

	public function inRange(range_start :int, range_end :int) : bool {
		return range_start <= int(this) && int(this) < range_end;
	}

	public function clamp(min_value :int, max_value :int) : int {
		if (int(this) < min_value) {
			return min_value;
		}
		if (int(this) > max_value) {
			return max_value;
		}
		return int(this);
	}

	public function min(other :int) :int {
		return (int(this) < other) ? int(this) : other;
	}


	public function max(other :int) :int {
		return (int(this) > other) ? int(this) : other;
	}

	////////////////////////////////////////////////
	// Bit Shifting Functions
	///////////////////////////////////////////////

	public function extract(shift_right : int, mask : int) : int {
		return (int(this) #>> shift_right) & mask;
	}

	public function rotateRight(cnt :int) :int {
	 	return (this #<< (32-cnt)) | (this #>>> cnt);
	}

	public function shiftLeft(cnt :int) :int {
		return this #<< cnt;
	}
 
	public function shiftRight(cnt :int) :int {
		return this #>>> cnt;
	}

	public function arithmeticShiftRight(cnt :int) :int {
		return this #>> cnt;
	}
}