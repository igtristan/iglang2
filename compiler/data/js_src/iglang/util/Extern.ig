package iglang.util;

import iglang.io.File;

public class Extern
{
	public static function log(v :String) :void {
		throw new Error("Unimplemented");
	}

	public static function doubleToIntBits(value : double) : int {
		throw new Error("Unimplemented");
	}
	
	public static function doubleToIntBitsLo(value : double) : int {
		throw new Error("Unimplemented");
	}

	public static function doubleToIntBitsHi(value : double) : int {
		throw new Error("Unimplemented");
	}
	
	public static function intBitsToDouble(value : int) : double {
		throw new Error("Unimplemented");
	}
	
	public static function intPairToDouble(value0 : int, value1 : int) : double {
		throw new Error("intPairToDouble Missing Native");
	}

	public static native function fileImplList(path :String) : Array<String> {
		return new Array<String>(0);
	}

	public static native function fileImplMkDir(path :String) : bool {
		return false;
	}

	public static native function fileImplStat(path :String, file :File) : int {
		return 0;
	}

	public static native function fileImplDelete(path :String) : bool {
		return false;
	}

	public static native function fileImplReadByteArray(m_path :String, ba :ByteArray, offset :int, max_length :int):int
	{
		return -1;
	}

	public static native function fileImplWriteByteArray(m_path :String, ba :Array<byte>, offset :int, length : int): bool {
		return false;
	}

	public static native function fileImplCopy(src : String ,dst :String ) :void;
}