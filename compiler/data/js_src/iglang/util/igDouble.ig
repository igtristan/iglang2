package iglang.util;

enum igDouble extends double
{		
	;
	
	public static const MAX_VALUE :double = 0d7fefffffffffffff;
	public static const MIN_VALUE :double = 0d1;

	public function toString() : String {
		return String(double(this));
	}
	
	public function hashCode() : int {
		var d : double = this;
		return int(d);
	}
	
	public function equals(other : igDouble): bool
	{
		return this == other;
	}


	public static function clamp(a :double, min_value :double, max_value :double) : double {
		if ((a) < min_value) {
			return min_value;
		}
		if ((a) > max_value) {
			return max_value;
		}
		return (a);
	}

	public static function min(a : double, other :double) :double {
		return iif(a < other, a, other);
	}

	public function max(a : double, other :double) :double {
		return iif(a > other, a, other);
	}
}