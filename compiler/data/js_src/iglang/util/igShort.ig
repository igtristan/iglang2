package iglang.util;

public enum igShort extends int
{
	;



	public static const MIN_VALUE :int = -0x1000;
	public static const MAX_VALUE :int = 0x7fff;
	
	public  function toString(): String
	{
		return String(int(this));
	}
	
	public  function hashCode(): int
	{
		return int(this);
	}
	
	public  function equals(other : igShort): bool
	{
		return int(other) == int(this);
	}
	
}