package iglang.util;

public interface BufferedInputStream
{
	public function read(): int;
	public function peek(offset :int = 0): int;
	public function available(): int;
}