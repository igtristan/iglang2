package iglang.util;

public class MapIterator<T1,T2>
{
	private var m_index : int = 0;
	private var m_size : int = 0;
	private var m_hash : Array<MapEntry<T1,T2>>;
	private var m_entry : MapEntry<T1,T2> = null;
	
	public var has_next : bool = false;
	
	public function constructor(a : Array<MapEntry<T1,T2>>, sz : int) {
		m_hash = a;
		m_size = sz;
		m_entry = null;
		m_index = 0;
		
		var i : int = 0;
		while (i < sz) {
			if (a[i] != null) {
				m_entry = a[i];
				break;
			}
			i++;
		}
		
		m_index = i;
		if (i < sz) {
			has_next = true;
		}
		else {
			has_next = false;
		}
	}
	
	
	public final function hasNext() : bool {
		return has_next;
	}
	
	
	public final function next(): T1
	{
		var tmp : MapEntry<T1,T2> = m_entry;
		m_entry = m_entry.m_next;
		
		if (m_entry == null) 
		{
			var a : Array<MapEntry<T1,T2>> = m_hash;
			var sz : int = m_size;
			var i : int = m_index + 1;
			while (i < sz) {
				if (a[i] != null) {
					m_entry = a[i];
					break;
				}
				i++;
			}
			
			m_index = i;
			if (i < sz) {
				has_next = true;
			}
			else {
				has_next = false;
			}
		}
		
		return tmp.m_key;
	}

}