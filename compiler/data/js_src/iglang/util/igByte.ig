package iglang.util;

public enum igByte extends int
{
	;

	public static const MIN_VALUE :int = -128;
	public static const MAX_VALUE :int =  127;

	public  function toString(): String
	{
		return String(int(this));
	}
	
	public  function hashCode(): int
	{
		return this;
	}
	
	public  function equals(other : igByte): bool
	{
		return other == this;
	}
	
}