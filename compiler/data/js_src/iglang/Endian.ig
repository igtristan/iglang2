package iglang;

public enum Endian extends int
{
	BIG_ENDIAN 		= 0,
	LITTLE_ENDIAN 	= 1;
}