package iglang;

public class Math
{	
	public static const PI         :double = 3.14159265358979323846;
	public static const TAU        :double = 6.28318530717958647692528676655900576839433879875021;
	public static const DEG_TO_RAD :double = 3.14159265358979323846/ 180.0;
	public static const RAD_TO_DEG :double = 180.0 / 3.14159265358979323846;
	
	public static native function sign  (v   :double            ) : double ;
 	public static native function sin   (v   :double            ) : double ;
	public static native function cos   (v   :double            ) : double ;
	public static native function tan   (v   :double            ) : double ;
	public static native function asin  (v   :double            ) : double ;
	public static native function acos  (v   :double            ) : double ;
	public static native function atan2 (y   :double, x : double) : double ;
	public static native function atan  (v   :double            ) : double ;
	public static native function pow   (base:double, pow:double) : double ;
	public static native function sqrt  (v   :double            ) : double ;
	public static native function random()                        : double ;
	public static native function abs   (v   :double)             : double ;
	public static native function floor (v   :double)             : double ;
	public static native function ceil  (v   :double)             : double ;
	public static native function round (v   :double)             : double ;
	public static native function min   (a   :double, b:double)   : double ;
	public static native function max   (a   :double, b:double)   : double ;
	
	public static function isqrt(v : double) :double {
		return 1.0 / sqrt(v);
	}



	//////////////////////////////////////////////////////////////////
	// Experimental section
	//////////////////////////////////////////////////////////////////

	//public static function square(a :double): double {
	//	return a*a;
	//}

	public static function length(a :double, b :double, c:double) : double {
		return Math.sqrt(a**2+b**2+c**2);
	}

	public static function length3(a :double, b :double, c:double) : double {
		return Math.sqrt(a **2 + b **2 + c **2);
	}

	public static function clamp(value :double, min_value :double, max_value :double): double {
		if (value < min_value) { return min_value; }
		if (value > max_value) { return max_value; }
		return value;
	}
}
