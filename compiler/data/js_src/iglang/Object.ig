package iglang;

import iglang.util.Extern;

public class Object
{
	public function constructor() {
	}	
	
	public static function trace(v : String): void { Extern.log(v);  }
	public static function stackTrace(): String    { throw new Error("Native Missing");  }
	
	public function hashCode(): int                {  
		throw new Error("Native Missing"); 
	}
	
	public function equals(v2 : Object): bool      {  
		return v2 == this;                 
	}
	
	public function toString() : String            {  
		return "";                         
	}
}
