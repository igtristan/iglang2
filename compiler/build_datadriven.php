<?php

$filename = "./data/ir_ops.txt";

error_log('loading: ' . $filename);

$fp = fopen('./src/pass1/IGIR.java', 'w');
if (null === $fp) {
}


function F($fp, string $string) {
	fprintf($fp, $string . PHP_EOL);
}




F($fp, 'package pass1;' );

F($fp, 'import java.util.*;' );

F($fp,  PHP_EOL);
F($fp, 'public class IGIR {' );


$found_ids = [];

$handle = fopen($filename, "r");
if ($handle)
{
	while (($line = fgets($handle)) !== false) 
	{

		$line = trim($line);

		// process the line read.
	 	if (strpos($line, '#') === 0 || strlen($line) === 0) {
       	        	continue;
        	}

		if ($line === 'EOF') {
			break;
		}

		$toks = explode(',', $line);
		foreach($toks as $k => $v) {
			$toks[$k] = trim($v);
		}

		$id 		= strtoupper($toks[0]);
		$stack_movement = $toks[1];
		$stack_change   = $toks[2];
	
		
			


		if (isset($found_ids[$id])) {
			error_log('Duplicate id: ' . $id);
			exit(1);	
		}


		$arg_info = [];
		$id_arg_count = count($toks) - 3;
		for ($i = 3; $i < count($toks); $i++) {
			$id_pair = explode(':', $toks[$i]);
			$arg_info[] = $id_pair;
		}

		$found_ids[$id] = [
			'id' => $id,
			'stack_movement' => (int)$stack_movement,
			'arg_info' => $arg_info
		];

		error_log($line);
	}
	fclose($handle);
}


$sequence_no = 0;
foreach ($found_ids as $k => $v) {
	$id = $v['id'];
	F($fp, "\tpublic static final int "  . $id . ' = ' . $sequence_no . ';' );	
	$sequence_no++;
}

F($fp, "\tpublic static final Map<Integer,String> OP_TO_NAME = new HashMap<Integer,String>();" );
F($fp, "\tstatic {");	
foreach ($found_ids as $k => $v) {
	$id = $v['id'];
	F($fp, "\t\tOP_TO_NAME.put(${id},\"${id}\");" );	
}
F($fp, "\t}" );	

foreach ($found_ids as $k => $v) {
	$id = $v['id'];
	$id_lower 	= strtolower($v['id']);
	$arg_info = $v['arg_info'];	

	F($fp, "\tpublic static IGIROp ${id_lower}(");
	foreach ($arg_info as $idx => $ai) {
		if ($idx != 0) {
			F($fp, ',');
		}
		F($fp, $ai[1] . ' ' . $ai[0] );
	}
	F($fp, ") { " );	
	F($fp, "\t\tIGIROp tmp = new IGIROp(${id});    " );

	foreach ($arg_info as $idx => $ai) {
		F($fp, "\t\ttmp." . $ai[0] . ' = ' . $ai[0] . ';' );
	}

	F($fp, "\t\treturn tmp; " );
	F($fp, "\t}" );	
}


F($fp, 'public static String convertToString(IGIROp op) {');
F($fp, "\tswitch(op.op) {");
foreach ($found_ids as $k => $v) {
	$id       = $v['id'];
	$arg_info = $v['arg_info'];	

	F($fp, "\t\tcase $id: {");


	$fields = ["\"$id\""];
	foreach ($arg_info as $idx => $ai) {
		$aname = $ai[0];
		$fields[] = "\", $aname: \"";
		$fields[] = 'op.' . $aname;
	}


	F($fp, "\t\t\tString lbl = (op.label != null) ? (\"\t\" + op.label + \":\t\") : (\"\t\t\");");

	F($fp, "\t\t\treturn op.stack + \"\\t\" + lbl + " .  join(' + ', $fields) . ';');
	F($fp, "\t\t}");
}
F($fp, "\t\tdefault:{");
F($fp, "\t\t\tthrow new RuntimeException(\"Invalid\");");
F($fp, "\t\t}");
F($fp, "\t}");
F($fp, '}');

F($fp, '}' );
