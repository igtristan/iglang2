/*
Copyright (c) 2014-2017 Incubator Games Ltd.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

#include "IGVM.h"

#include <stdlib.h>
#include <stdio.h>

namespace igvm
{	

	void  Config::defaultGetCompiledSourceDirectory(VMInstance * vm, char * out, size_t sz)
	{
		strncpy(out, "dst", sz);
	}

	void * Config::defaultOpen(VMInstance * vm, const char * path)
	{
		Config * conf = vm->getConfig();

		char compiled_src_dir[1024];
		(*conf->_get_dir)(vm, compiled_src_dir, 1024);
		
		char * tmp = (char *) malloc(strlen(path) + 1);
		memcpy(tmp, path, strlen(path) + 1);
	
		// replace all '.' s with backslashes
		int mode = 0;
		char * ptr = tmp;
		while (*ptr != '\0') 
		{
			if (mode == 0)
			{
				if (*ptr == '.') {
					*ptr = '/';
				}
				else if (*ptr == '<') {
					mode = 1;
				}
			}
			
			if (mode == 1)
			{
				if (*ptr == '<') { *ptr = '{'; }
				if (*ptr == '>') { *ptr = '}'; }
			}
			ptr++;
		}
	
		char full_path[1024];
		snprintf(full_path, 1024, "%s/%s.igclass", compiled_src_dir, tmp);
	
		free(tmp);
		
		return fopen(full_path, "rb");
	}
	
	void   Config::defaultRead(VMInstance * vm, void * ptr, void * dst, size_t sz)
	{	
		FILE * fp = (FILE *)ptr;
		fread(dst, 1, sz, fp);
	}
	void   Config::defaultClose(VMInstance * vm, void * ptr)
	{
		fclose((FILE *)ptr);
	}
	void *  Config::defaultMalloc(VMInstance * vm, size_t sz) {
		return malloc(sz);
	}
	void    Config::defaultFree(VMInstance * m, void * ptr) {
		return free(ptr);
	}
	void    Config::defaultLog(VMInstance * vm, const char * message) {
		printf("igvm(%p): %s\n", vm, message);
	}
	void    Config::defaultSwapRef32(VMInstance * vm, ig_i32 old_ref, ig_i32 new_ref, bool has_new)
	{
	}
};